﻿
## Caine’s Story

Caine Eckstein is a professional multi-sport athlete from the Gold Coast, Australia. Throughout his career he has excelled in various areas of sport with a number of impressive accolades to his name. His major achievements include being the Guinness World 24 hour pull ups record holder (7,620 pull ups), a five-times Coolangatta Gold Champion and a Kellogg’s Nutri-Grain Ironman and World Board Race Champion. Caine’s success has been built on his determination and extremely active lifestyle which involves an intensive 3.5 hour daily training routine. He has succeeded in surfing, running, swimming, triathlon, boxing and ironman.

Initially when Caine began training for the professional ironman series he started focusing on 15 minute races and progressed into four hour endurance races. Throughout his training regimen for the ironman races, Caine saw a dramatic difference in his muscle amount and body weight whilst switching between these two events.

In 2014, Caine attempted a new goal and travelled to New York City to attempt to break the Guinness World Record for pull ups over 12 and 24 hour periods live on the Today Show. Caine was successful in breaking the 12 and 24 hour record, completing 4,210 pull ups in 12 hours, unfortunately he tore his bicep during the event after seven hours. Two years later, the 24 hour record had been broken multiple times and sat at 7,310 pull ups. Caine was determined to attempt to break the record again but this time he was aiming to take a different approach with a plan to implement intermittent fasting into his daily regimen. He took on the 16:8 intermittent fasting approach which involved abstaining from eating for a 16 hour period and choosing an 8 hour period to consume food.

This theory for Caine was all about volume and his power to weight ratio. If he was  able to maintain strength and be lighter, it would increase his performance and chances of breaking the record without injuring himself this time.  To achieve his record breaking 7,620 pull ups at his body weight of 77kg, he would have to pull the accumulative weight of 586,740kg through the air in 24 hours, an incredibly challenging task. Through the 16:8 fasting routine, his body weight dropped to 72kg which meant he would have to pull 548,640kg in the same 24 hour period. The difference of 38,100kg was a crucial factor in enhancing Caine's performance and putting less stress on his muscles, therefore reducing the chance of injury. By adapting intermittent fasting into his lifestyle, Caine completed 7,620 pull ups over the 24 hour period breaking the Guinness World Record for a second time.

Caine noticed multiple key points whilst implementing this fasting schedule:
* Body fat percentage decreased
* Maintained power and strength
* Performance dramatically increased
* More energy
* Ability to workout longer at a higher intensity
* No injuries
* Delay in fatigue during exercise
* Substantial drop in lactic acid during high intensity workouts
* Felt an addictive feeling of euphoria during morning exercise fasting period
* Meal portions became smaller during the eating window.
* Ability to find ideal body weight
![Image of Yaktocat](https://octodex.github.com/images/yaktocat.png )
