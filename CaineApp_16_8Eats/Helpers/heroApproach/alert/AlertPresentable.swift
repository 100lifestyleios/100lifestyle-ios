//
//  AlertPresentable.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 08/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
protocol AlertPresentable {
    func presentAlert(title:String, message: String, okHandler: @escaping (Bool) -> ())
}
