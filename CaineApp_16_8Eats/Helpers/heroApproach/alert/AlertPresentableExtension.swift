//
//  AlertPresentableExtension.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 08/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
extension AlertPresentable where Self : UIViewController {
    
    func presentAlert(title : String, message: String, okHandler: @escaping (Bool) -> () ) {
        let alert = UIAlertController(title: title, message: message,  preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            okHandler(true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func presentAlert(title : String, message: String, yesHandler: @escaping (Bool) -> () ,  noHandler: @escaping (Bool) -> ()) {
        let alert = UIAlertController(title: title, message: message,  preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
            yesHandler(true)
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action) in
            noHandler(true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
