//
//  ArticleDemoDetailViewController.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 03/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Hero

class ArticleDemoDetailViewController: UIViewController {
    
    @IBOutlet weak var ivDetail : UIImageView!
    
    var imageName : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.hero.isEnabled = true
        self.navigationController?.hero.isEnabled = true
        ivDetail.hero.id = "image"
        
        if(imageName != nil){
            ivDetail.image = UIImage(named: imageName!)
        }
    }

}
