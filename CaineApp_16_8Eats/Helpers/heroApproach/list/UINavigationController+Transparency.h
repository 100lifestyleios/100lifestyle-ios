#import <UIKit/UIKit.h>

@interface UINavigationController (Transparency)

- (void)gsk_setNavigationBarTransparent:(BOOL)transparent
                               animated:(BOOL)animated;
- (void)presentTransparentNavigationBar;
- (void)hideTransparentNavigationBar;

@end
