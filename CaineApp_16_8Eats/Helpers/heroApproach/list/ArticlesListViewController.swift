//
//  ArticlesListViewController.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 03/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Hero
import SVProgressHUD
import Mixpanel
import ParseUI


class ArticlesListViewController: UITableViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    static let TAG = "ArticlesListViewController"
    
    var articles : [Article] = []
    
    internal var currentCell: ArticleTableViewCell?
    
    fileprivate var duration: Double = 0.25
    fileprivate var currentTextLabel: MovingLabel?
    
    var isNotificationClicked = false
    var notificationImageName : String?
    var notificationTitleText : String?
    
    var presenter : SplashPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: "Article List", properties: ["Screen name" : "Article List"])
        FEUserDefaults.sharedInstance.screenName = "Articles List View Controller"
        FEUserDefaults.sharedInstance.startTime = Date()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.bounces = false
        
        presenter = SplashPresenter(view: self)
        
        SVProgressHUD.setForegroundColor(Globals.colorAccent)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        
        
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }

        //Remove Left Right Buttons
        setupLeftBarButton()
//        setupRightBarButton()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(isNotificationClicked){
            presenter = SplashPresenter(view: self)
            SVProgressHUD.show()
            presenter?.requestArticles()
        }
        
        let appDelegate = UIApplication.shared.delegate as! CaineAppDelegate
        appDelegate.appVisibleController = self as ArticlesListViewController
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onArticleUnlocked(notification:)), name: Notification.Name(Globals.notificationArticleUnlockedList), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onArticleOpened(notification:)), name: Notification.Name(Globals.notificationArticleOpenedList), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        articles = ArticleInstance.sharedInstance.articles
        //        moveCellsBackIfNeed(0.0) { [weak self] in
        //            self?.tableView.reloadData()
        //            self?.currentCell?.lblTimeRemaining.isHidden = false
        //            self?.currentCell?.lblCaineName.isHidden = false
        //            self?.currentCell?.imageViewLock.isHidden = false
        //            self?.currentCell?.imageViewCaine.isHidden = false
        //        }
        //        closeCurrentCellIfNeed(duration)
        //        moveDownCurrentLabelIfNeed()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        //removed to fixed update issue
        //        let appDelegate = UIApplication.shared.delegate as! CaineAppDelegate
        //        appDelegate.appVisibleController = nil
        
        //        presenter = nil
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Globals.notificationArticleUnlockedList), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Globals.notificationArticleOpenedList), object: nil)
        
        super.viewDidDisappear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Tabbar")
    }
    
    deinit {
        print("\(self) deinit called")
        presenter = nil
    }
    
}

//MARK:- Table View Settings....
extension ArticlesListViewController {
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCell", for: indexPath) as! ArticleTableViewCell
        
        cell.article = articles[indexPath.row]
        
        cell.parallaxHeightConstraint.constant = parallaxImageHeight
        cell.parallaxTopConstraint.constant = parallaxOffsetFor(newOffsetY: tableView.contentOffset.y, cell: cell)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        guard let currentCell = tableView.cellForRow(at: indexPath) as? ArticleTableViewCell else {
            return indexPath
        }
        
        
        //        cell.isOpaque = true
        
        //        if( !articles[indexPath.row].isLocked ){
        
        //        currentCell.lblTimeRemaining.isHidden = true
        //        currentCell.lblCaineName.isHidden = true
        //        currentCell.imageViewLock.isHidden = true
        //        currentCell.imageViewCaine.isHidden = true
        //        self.currentCell = currentCell
        
        //        }
        
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if( !articles[indexPath.row].isLocked ){
            
            let cell = tableView.cellForRow(at: indexPath) as! ArticleTableViewCell
            
            cell.hero.id = "image_\(indexPath.row)"
            
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DemoDetailViewController") as! DemoDetailViewController
            
            controller.bgImage = UIImage(named: articles[indexPath.row].imageName)
            controller.titleText = articles[indexPath.row].title
            controller.tableRow = indexPath.row
            controller.hero.isEnabled = true
            controller.delegate = self
            
            self.navigationController?.hero.isEnabled = true
            
            let navController = UINavigationController(rootViewController: controller)
            navController.hero.isEnabled = true
            
            self.present(navController, animated: true)
            
        } else {
            
            let title = articles[indexPath.row].title
            self.presentAlert(title: title , message: "The article is locked!", okHandler: { _ in
                
            })
        }
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = tableView.contentOffset.y
        for cell in tableView.visibleCells as! [ArticleTableViewCell] {
            cell.parallaxTopConstraint.constant = parallaxOffsetFor(newOffsetY: offsetY, cell: cell)
        }
        //        parallaxOffsetDidChange(scrollView.contentOffset.y)
    }
}

//MARK:- Parallax Helpers
extension ArticlesListViewController {
    
    // Change the ratio or enter a fixed value, whatever you need
    var cellHeight: CGFloat {
        return 0.42 * tableView.frame.size.height
    }
    
    // Just an alias to make the code easier to read
    var imageVisibleHeight: CGFloat {
        return cellHeight
    }
    
    // Change this value to whatever you like (it sets how "fast" the image moves when you scroll)
    var parallaxOffsetSpeed: CGFloat {
        return 25;
    }
    
    // This just makes sure that whatever the design is, there's enough image to be displayed, I let it up to you to figure out the details, but it's not a magic formula don't worry :)
    var parallaxImageHeight: CGFloat {
        let maxOffset = (sqrt(pow(cellHeight, 2) + 4 * parallaxOffsetSpeed * tableView.frame.height) - cellHeight) / 2
        return imageVisibleHeight + maxOffset
    }
    
    // Used when the table dequeues a cell, or when it scrolls
    func parallaxOffsetFor(newOffsetY: CGFloat, cell: UITableViewCell) -> CGFloat {
        return ((newOffsetY - cell.frame.origin.y) / parallaxImageHeight) * parallaxOffsetSpeed
    }
    
}


extension ArticlesListViewController {
    
    fileprivate func createTitleLable(_ cell: ArticleTableViewCell) -> MovingLabel {
        
        let yPosition = cell.frame.origin.y + cell.frame.size.height / 2.0 - 22 - tableView.contentOffset.y
        let label = MovingLabel(frame: CGRect(x: 0, y: yPosition, width: UIScreen.main.bounds.size.width, height: 44))
        label.textAlignment = .center
        label.backgroundColor = .clear
        if let font = cell.parallaxTitle?.font,
            let text = cell.parallaxTitle?.text,
            let textColor = cell.parallaxTitle?.textColor {
            label.font = font
            label.text = text
            label.textColor = textColor
        }
        
        navigationController?.view.addSubview(label)
        return label
    }
    
    fileprivate func createSeparator(_ color: UIColor?, height: CGFloat, cell: UITableViewCell) -> MovingView {
        
        let yPosition = cell.frame.origin.y + cell.frame.size.height - tableView.contentOffset.y
        let separator = MovingView(frame: CGRect(x:0.0, y: yPosition, width: tableView.bounds.size.width, height: height))
        if let color = color {
            separator.backgroundColor = color
        }
        separator.translatesAutoresizingMaskIntoConstraints = false
        navigationController?.view.addSubview(separator)
        return separator
    }
}


// MARK: helpers

extension ArticlesListViewController {
    
    fileprivate func parallaxOffsetDidChange(_ offset: CGFloat) {
        
        let _ = tableView.visibleCells
            .filter{$0 != currentCell }
            .forEach { if case let cell as ArticleTableViewCell = $0 { cell.parallaxOffset(tableView) } }
    }
    
    fileprivate func moveCellsBackIfNeed(_ duration: Double, completion: @escaping () -> Void) {
        
        guard let currentCell = self.currentCell,
            let currentIndex = tableView.indexPath(for: currentCell) else {
                return
        }
        
        for case let cell as ArticleTableViewCell in tableView.visibleCells where cell != currentCell {
            
            if cell.isMovedHidden == false {continue}
            
            if let index = tableView.indexPath(for: cell) {
                let direction = (index as NSIndexPath).row < (currentIndex as NSIndexPath).row ? ArticleTableViewCell.Direction .up : ArticleTableViewCell.Direction.down
                cell.animationMoveCell(direction, duration: duration, tableView: tableView, selectedIndexPaht: currentIndex, close: true)
                cell.isMovedHidden = false
            }
        }
        delay(duration, closure: completion)
    }
    
    fileprivate func closeCurrentCellIfNeed(_ duration: Double) {
        
        guard let currentCell = self.currentCell else {
            return
        }
        
        currentCell.closeCell(duration, tableView: tableView) { () -> Void in
            self.currentCell = nil
        }
    }
    
    fileprivate func moveDownCurrentLabelIfNeed() {
        
        guard let currentTextLabel = self.currentTextLabel else {
            return
        }
        currentTextLabel.move(0.0, direction: .down) { (finished) in
            currentTextLabel.removeFromSuperview()
            self.currentTextLabel = nil
        }
    }
    
    //  animtaions
    fileprivate func moveCells(_ tableView: UITableView, currentCell: ArticleTableViewCell, duration: Double) {
        guard let currentIndex = tableView.indexPath(for: currentCell) else {
            return
        }
        
        for case let cell as ArticleTableViewCell in tableView.visibleCells where cell != currentCell {
            cell.isMovedHidden = true
            let row = (tableView.indexPath(for: cell) as NSIndexPath?)?.row
            let direction = row! < (currentIndex as NSIndexPath).row ? ArticleTableViewCell.Direction .down : ArticleTableViewCell.Direction.up
            cell.animationMoveCell(direction, duration: duration, tableView: tableView, selectedIndexPaht: currentIndex, close: false)
        }
    }
}

//MARK :- Article Unlock Notification Recieved
extension ArticlesListViewController {
    @objc func onArticleUnlocked(notification: Notification){
        print("ArticlesListViewController : onArticleUnlocked()")
        
        SVProgressHUD.show()
        
        //show progress hud
        presenter?.requestArticles()
        
    }
    
    @objc func onArticleOpened(notification: Notification){
        print("ArticlesListViewController : onArticleOpened()")
        
        SVProgressHUD.show()
        
        isNotificationClicked = true
        notificationTitleText = notification.userInfo?["title"] as? String
        notificationImageName = notification.userInfo?["imageName"] as? String
        
        //show progress hud
        presenter?.requestArticles()
    }
}

//MARK :- Splash Presenter Functions
extension ArticlesListViewController : SplashView {
    
    func onArticlesListError() {
        print("\(ArticlesListViewController.TAG) : onArticlesListNext()")
        SVProgressHUD.dismiss()
    }
    
    func onArticlesListNext(articles: [Article]) {
        print("\(ArticlesListViewController.TAG) : onArticlesListNext()")
        
        SVProgressHUD.dismiss()
        self.articles = articles
        self.tableView.reloadData()
        
        if(isNotificationClicked && notificationImageName != nil && notificationTitleText != nil ){
            let detailController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DemoDetailViewController") as! DemoDetailViewController
            
            detailController.bgImage = UIImage(named: (self.notificationImageName)!)
            detailController.titleText = (self.notificationTitleText)!
            
            let navController = UINavigationController(rootViewController: detailController)
            navController.hero.isEnabled = true
            detailController.delegate = self
            
            self.present(navController, animated: true, completion: nil)
            self.isNotificationClicked = false
        }
        
    }
}

//MARK :- Article List VC Delegate
extension ArticlesListViewController : ArticleDetailVCDelegate {
    func onArticleUnlockedAtDetail() {
        SVProgressHUD.show()
        presenter?.requestArticles()
    }
    
    func onArticleOpenAtDetail(title : String, imageName: String) {
        print("ArticlesListViewController : onArticleOpenAtDetail()")
        print("imageName : \(imageName)")
        print("title : \(title)")
        isNotificationClicked = true
        notificationImageName = imageName
        notificationTitleText = title
        SVProgressHUD.show()
        presenter?.requestArticles()
        
    }
}

extension ArticlesListViewController {
    func setupLeftBarButton(){
        let image = UIImage(named: "menu")
        let leftButton = UIBarButtonItem(image: image  , style: .plain, target: self , action: #selector(self.didTabNavigationBarLeftButton(_:)))
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = nil
    }
    
    func setupRightBarButton(){
        let image = #imageLiteral(resourceName: "ticket")
        let rightButton = UIBarButtonItem(image: image, style: .plain, target: self , action: #selector(self.didTabNavigationBarRightButton(_:)))
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
    }
    
    @objc func didTabNavigationBarLeftButton(_ sender : UIButton){
        print("\(ArticlesListViewController.TAG) : didTabNavigationBarLeftButton()")
        showActionSheet()
    }
    
    @objc func didTabNavigationBarRightButton(_ sender : UIButton){
        print("\(ArticlesListViewController.TAG) : didTabNavigationBarRightButton()")
        showEntryTicket()
    }
    
    func showEntryTicket() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EntryPassViewController") as! EntryPassViewController
    
        self.navigationController?.hero.isEnabled = false
        
        let navController = UINavigationController(rootViewController: controller)
        navController.hero.isEnabled = false
        
        self.present(navController, animated: true)
    }
    
    
    private func showActionSheet() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Privacy Policy", style: .default , handler:{ [weak self] action in
            self?.openURL(withURL: URL(string: "https://100clubapp.com/privacy-policy/")!)
        }))
        alert.addAction(UIAlertAction(title: "Terms and Conditions", style: .default , handler:{ [weak self] action in
            self?.openURL(withURL: URL(string: "https://100clubapp.com/terms/")!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
        }))
        present(alert, animated: true, completion: nil)
    }
    
    override func openURL(withURL url : URL ){
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
    }
    
}

