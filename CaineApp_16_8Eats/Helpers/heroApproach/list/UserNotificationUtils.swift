//
//  UserNotificationUtils.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 07/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UserNotifications
import Firebase
class UserNotificationUtils  {
    
    static let TAG = "UserNotificationUtils"
    
    static func scheduleNotification(date : Date, title : String, subTitle : String,  body : String, imageName : String,  delegate : UNUserNotificationCenterDelegate){
        
        var userInfo : [String : String] = [:]
        
        userInfo["imageName"] = imageName
        
        // schedule user notification
        let content = UNMutableNotificationContent()
        content.sound = UNNotificationSound.default()
        content.title = title
        content.subtitle = subTitle
        content.body = body
        content.userInfo = userInfo
        
        let center = UNUserNotificationCenter.current()
        center.delegate = delegate
        
        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate,
                                                    repeats: false)
        
        
        let identifier = UUID().uuidString
        
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                // Something went wrong
                print( "\(UserNotificationUtils.TAG) : error: \(error.localizedDescription)" )
            }
        })
        
    }
    
    static func cancelAllNotifications(){
        
        Analytics.logEvent("cancelAllNotifications", parameters: [:])
        
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
    }
    
}

