#import "UINavigationController+Transparency.h"
#import "UIImage+BetterAdditions.h"
@implementation UINavigationController (Transparency)

- (void)gsk_setNavigationBarTransparent:(BOOL)transparent
                               animated:(BOOL)animated {
    [UIView animateWithDuration:animated ? 0.5 : 0 animations:^{
        if (transparent) {
            [self.navigationBar setBackgroundImage:[UIImage new]
                                     forBarMetrics:UIBarMetricsDefault];
            self.navigationBar.shadowImage = [UIImage new];
            self.navigationBar.translucent = YES;
            self.view.backgroundColor = [UIColor clearColor];
            self.navigationBar.backgroundColor = [UIColor clearColor];
        } else {
            [self.navigationBar setBackgroundImage:nil
                                     forBarMetrics:UIBarMetricsDefault];
        }
        self.navigationBarHidden = transparent;
    }];
}

- (void)presentTransparentNavigationBar
{
    self.navigationBar.barTintColor = [UIColor clearColor];
    self.navigationBar.backgroundColor = [UIColor clearColor];
    UIImage * transparent= [UIImage imageWithColor:[UIColor clearColor]];
    [self.navigationBar setBackgroundImage:transparent forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[UIImage new]];
    [self.navigationBar setTranslucent:YES];
}

- (void)hideTransparentNavigationBar
{
//    [self.navigationBar setBackgroundImage:[UIImage imageWithColor:[AppConfig sharedInstance].navigationBarBgColor] forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setTranslucent:[[UINavigationBar appearance] isTranslucent]];
    [self.navigationBar setShadowImage:[[UINavigationBar appearance] shadowImage]];
}

@end

//@implementation UINavigationController (ChildStatusBarStyle)
//- (UIViewController *)childViewControllerForStatusBarStyle
//{
//    return self.topViewController;
//}
//@end

