//
//  ArticleTableViewCell.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 03/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Hero

fileprivate struct C {
    
    struct Constraints {
        static let bottom = "Bottom"
        static let height = "Height"
    }
}

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var articleImageView : UIImageView!
    
    @IBOutlet weak var parallaxHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var parallaxTopConstraint: NSLayoutConstraint!
    
    let screenHeight = UIScreen.main.bounds.height
    
    @IBOutlet weak var imageViewGrayOverlay: UIImageView!
    @IBOutlet weak var imageViewCaine: UIImageView!
    @IBOutlet weak var lblCaineName: UILabel!
    @IBOutlet weak var imageViewLock: UIImageView!
    @IBOutlet weak var lblTimeRemaining: UILabel!
    
    open var separatorView: UIView?
    
    var parallaxTitle: UILabel?
    
    var topSeparator: UIView? // only for animation
    
    var bgImageY: NSLayoutConstraint?
    var parallaxTitleY: NSLayoutConstraint?
    
    /// parallax offset
    @IBInspectable open var difference: CGFloat = 100 // image parallax
    
    enum Direction {
        case up
        case down
    }
    
    var bgImage: UIImageView?
    
    /// The foreground view’s background color.
    @IBInspectable open var foregroundColor = UIColor.white
    
    /// The foreground view’s alpha.
    @IBInspectable open var foregroundAlpha: CGFloat = 0.0
    
    var foregroundView: UIView?
    var isMovedHidden: Bool = false
    
    fileprivate var closedBgImageYConstant: CGFloat = 0
    fileprivate var closedYPosition: CGFloat = 0
    
    fileprivate var damping: CGFloat = 0.78
    
    var article : Article? {
        didSet {
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clipsToBounds = true
        articleImageView.contentMode = .scaleAspectFill
        articleImageView.clipsToBounds = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        parallaxTitle?.font = UIFont(name: "Freshman", size: screenHeight / 26.285)  //28
        parallaxTitle?.minimumScaleFactor = 0.5;
        parallaxTitle?.textColor = UIColor.white
        parallaxTitle?.numberOfLines = 2
        
        lblCaineName.font = UIFont(name: "Kalinga", size: screenHeight / 52.5714) //14
        lblCaineName.textColor = UIColor.white
        
        lblTimeRemaining?.font = UIFont(name: "Kalinga", size: screenHeight / 61.333) //12
        lblTimeRemaining?.textColor = UIColor.white
        
        // To show nib views, bring outlets to front
        for subview:UIView in [imageViewGrayOverlay, parallaxTitle!, lblCaineName, lblTimeRemaining, imageViewCaine, imageViewLock, separatorView!] {
            self.contentView.bringSubview(toFront: subview)
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func updateUI(){
        articleImageView.contentMode = .scaleAspectFill
        parallaxTitle?.text = article?.title
        if(article?.image != nil) {
            articleImageView.image = article?.image
        }
        
        if(article?.isLocked)!{
            imageViewLock.image = UIImage(named: "lock")
        } else {
            imageViewLock.image = UIImage(named: "unlock")
        }
        
        if(article != nil){
            if(article!.dateDaysRemaining > 0){
                lblTimeRemaining.text = "\(String(describing: article!.dateDaysRemaining)) days to go"
                lblTimeRemaining.isHidden = false
            }
            else {
                lblTimeRemaining.isHidden = true
            }
        }
    }
    
}

// MARK: life cicle
extension ArticleTableViewCell {
    
    func commonInit() {
        
        layer.masksToBounds = false
        selectionStyle = .none
        
        // create background image view
        let backgroundImageView = createBckgroundImage()
        
        // add constraints
        if let bgSuperView = backgroundImageView.superview {
            for attribute: NSLayoutAttribute in [.leading, .trailing] {
                (bgSuperView, backgroundImageView) >>>- {
                    $0.attribute = attribute
                    return
                }
            }
            bgImageY = (bgSuperView, backgroundImageView) >>>- {
                $0.attribute = .centerY
                return
            }
            backgroundImageView >>>- {
                $0.attribute = .height
                $0.constant = bounds.height + difference
                return
            }
        }
        bgImage = backgroundImageView
        bgImage?.contentMode = .top
        
        foregroundView = createForegroundView(foregroundColor)
        contentView.backgroundColor = UIColor.white
        
        // create title label
        let titleLabel = createTitleLable()
        //    for attribute: NSLayoutAttribute in [.left, .right] {
        //        (contentView, titleLabel) >>>- {
        //          $0.attribute = attribute
        //          return
        //      }
        //      }
        (contentView, titleLabel) >>>- {
            $0.attribute = .left
            $0.constant = 10
            return
        }
        (contentView, titleLabel) >>>- {
            $0.attribute = .right
            $0.constant = -10
            return
        }
        parallaxTitleY = (contentView, titleLabel) >>>- {
            $0.attribute = .centerY
            return
        }
        titleLabel >>>- {
            $0.attribute = .height
            $0.constant = bounds.height + difference
            return
        }
        parallaxTitle = titleLabel
        
        separatorView = createSeparator(.white, height: 2, verticalAttribure: .bottom, verticalConstant: 0.0)
    }
    
    open override func prepareForReuse() {
        if topSeparator?.superview != nil {
            topSeparator?.removeFromSuperview()
            topSeparator = nil
        }
    }
}


// MARK: create
extension ArticleTableViewCell {
    
    fileprivate func createBckgroundImage() -> UIImageView {
        
        let container = createImageContainer()
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        container.addSubview(imageView)
        return imageView
    }
    
    fileprivate func createImageContainer() -> UIView {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        contentView.addSubview(view)
        
        // added constraints
        for attribute: NSLayoutAttribute in [.left, .right, .top, .bottom] {
            (contentView, view) >>>- {
                $0.attribute = attribute
                return
            }
        }
        
        return view
    }
    
    
    fileprivate func createTitleLable() -> UILabel {
        
        let label = UILabel()
        label.backgroundColor = .clear
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        if case let font as UIFont = UINavigationBar.appearance().titleTextAttributes?[NSFontAttributeName] {
            label.font = font
        }
        
        if case let textColor as UIColor = UINavigationBar.appearance().titleTextAttributes?[NSForegroundColorAttributeName] {
            label.textColor = textColor
        }
        contentView.addSubview(label)
        return label
    }
}

// MARK: cofigure
extension ArticleTableViewCell {
    
    fileprivate func createForegroundView(_ color: UIColor) -> UIView {
        guard let bgImage = self.bgImage else {
            fatalError("set bgImage")
        }
        
        let foregroundView = UIView()
        foregroundView.alpha = foregroundAlpha
        foregroundView.backgroundColor = color
        foregroundView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.insertSubview(foregroundView, aboveSubview: bgImage)
        
        // add constraints
        for attribute: NSLayoutAttribute in [.left, .right, .top] {
            (contentView, foregroundView) >>>- {
                $0.attribute = attribute
                return
            }
        }
        (contentView, foregroundView) >>>- {
            $0.attribute = .bottom
            $0.identifier = C.Constraints.bottom
            return
        }
        
        return foregroundView
    }
    
    // return bottom constraint
    fileprivate func createSeparator(_ color: UIColor, height: CGFloat, verticalAttribure: NSLayoutAttribute, verticalConstant: CGFloat) -> UIView {
        let separator = UIView(frame: CGRect.zero)
        separator.backgroundColor = color
        separator.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(separator)
        
        for attribute: NSLayoutAttribute in [.leading, .trailing] {
            (contentView, separator) >>>- {
                $0.attribute = attribute
                return
            }
        }
        
        (contentView, separator) >>>- {
            $0.attribute = verticalAttribure
            $0.constant = verticalConstant
            return
        }
        // height constraint
        separator >>>- {
            $0.attribute = .height
            $0.constant = height
            return
        }
        return separator
    }
}

// MARK: internal
extension ArticleTableViewCell {
    
    func parallaxOffset(_ tableView: UITableView) {
        
        guard let bgImageY = self.bgImageY , isMovedHidden == false else {
            return
        }
        
        var deltaY = (frame.origin.y + frame.height/2) - tableView.contentOffset.y
        deltaY = min(tableView.bounds.height, max(deltaY, 0)) // range
        
        var move : CGFloat = (deltaY / tableView.bounds.height)  * difference
        move = move / 2.0  - move
        
        bgImageY.constant = move
    }
    
    func openCell(_ tableView: UITableView, duration: Double) {
        guard let superview = self.superview,
            let bgImageY = self.bgImageY else {
                return
        }
        
        closedBgImageYConstant = bgImageY.constant
        closedYPosition = center.y
        
        let offsetY = tableView.contentOffset.y
        let cellY = frame.origin.y - offsetY + frame.size.height / 2.0 + offsetY - tableView.frame.size.height / 2.0
        let cellFrame = CGRect(x: 0, y: cellY, width: tableView.frame.size.width, height: tableView.frame.size.height)
        frame = cellFrame
        superview.sendSubview(toBack: self)
        
        // animation
        moveToCenter(duration , offset: offsetY)
        parallaxTitle?.isHidden = true
        foregroundHidden(true, duration: duration)
    }
    
    func closeCell(_ duration: Double, tableView: UITableView, completion: @escaping () -> Void) {
        bgImageY?.constant = closedBgImageYConstant
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: damping, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: { [weak self] () in
            guard let `self` = self else { return }
            self.bgImage?.superview?.layoutIfNeeded()
            self.center = CGPoint(x: self.center.x, y: self.closedYPosition)
            }, completion: {[weak self] finished in
                
                self?.parallaxTitle?.isHidden = false
                completion()
        })
        
        foregroundHidden(false, duration: duration / 2.0)
    }
    
    func animationMoveCell(_ direction: Direction, duration: Double, tableView: UITableView, selectedIndexPaht: IndexPath, close: Bool) {
        
        let selfYPosition = close == false ? frame.origin.y : closedYPosition
        let selectedCellFrame = tableView.rectForRow(at: selectedIndexPaht)
        var dy: CGFloat = 0
        if selfYPosition < selectedCellFrame.origin.y {
            dy = selectedCellFrame.origin.y - tableView.contentOffset.y
        } else {
            dy = tableView.frame.size.height - (selectedCellFrame.origin.y - tableView.contentOffset.y + selectedCellFrame.size.height)
        }
        dy = direction == .down ? dy * -1 : dy
        if close == false {
            closedYPosition = center.y
        } else {
            center.y = closedYPosition - dy
        }
        
        superview?.bringSubview(toFront: self)
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: damping, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.center.y = self.center.y + dy
        }, completion: nil)
    }
    
    func showTopSeparator() {
        topSeparator = createSeparator(.white, height: 2, verticalAttribure: .top, verticalConstant: -2)
    }
}

// MARK: animation
extension ArticleTableViewCell {
    
    fileprivate func moveToCenter(_ duration: Double, offset: CGFloat) {
        bgImageY?.constant = 0
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.78, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.frame.origin.y = offset
            self.bgImage?.superview?.layoutIfNeeded()
        }, completion: nil)
    }
    
    fileprivate func foregroundHidden(_ hidden: Bool, duration: Double) {
        guard let foregroundView = self.foregroundView else {
            return
        }
        
        if hidden == true {
            let currentConstrant = contentView.constraints.filter{return $0.identifier == C.Constraints.bottom ? true : false}
            contentView.removeConstraints(currentConstrant)
            
            foregroundView >>>- {
                $0.attribute = .height
                $0.constant = 64
                $0.identifier = C.Constraints.height
                return
            }
        } else {
            let currentConstrant = foregroundView.constraints.filter{return $0.identifier == C.Constraints.height ? true : false}
            foregroundView.removeConstraints(currentConstrant)
            
            (contentView, foregroundView) >>>- {
                $0.attribute = .bottom
                $0.identifier = C.Constraints.bottom
                return
            }
        }
        
        UIView.animate(withDuration: duration,
                       delay: 0,
                       usingSpringWithDamping: damping * 2.0,
                       initialSpringVelocity: 0,
                       options: UIViewAnimationOptions(),
                       animations: { () -> Void in
                        foregroundView.superview?.layoutIfNeeded()
        }, completion: nil)
    }
}

