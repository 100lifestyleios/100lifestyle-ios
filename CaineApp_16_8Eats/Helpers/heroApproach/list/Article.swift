//
//  Article.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 04/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
class Article {
    
    var title : String
    var imageName : String
    var isGrey : Bool
    
    var image : UIImage?
    var isLocked : Bool = false
    
    var unlockDays : Int = 0
    var dateDaysRemaining : Int = 0
    
    var unlockingNotificationDate = Date()
    
    init(_ imageName : String, _ title : String, index : Int, unlockDays : Int) {
        self.title = title
        self.imageName = imageName
        self.unlockDays = unlockDays
        self.isGrey = false
    }
}

