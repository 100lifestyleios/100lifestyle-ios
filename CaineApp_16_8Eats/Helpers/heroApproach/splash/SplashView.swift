//
//  SplashView.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 07/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
protocol SplashView {
    func onArticlesListNext(articles : [Article])
    func onArticlesListError()
}

