//
//  Globals.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 07/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
class Globals {
    
    //Days for Unlocking Articlies (For Release)
    static let unit = Calendar.Component.day
    static let conversionDevisor = (1000.0 *  60 * 60 * 24)

    //Minutes fo Unclocking Articles (For Debugging)
    
//    static let unit = Calendar.Component.minute
//    static let conversionDevisor = (1000.0 *  60)
    
    static let notificationArticleUnlockedList = "NotificationArticleUnlockedList"
    static let notificationArticleUnlockedDetail = "NotificationArticleUnlockedDetail"
    
    static let notificationArticleOpenedList = "NotificationArticleOpenedList"
    static let notificationArticleOpenedDetail = "NotificationArticleOpenedDetail"
    
    static let colorAccent = UIColor(red: 58/255.0, green: 182/255.0, blue: 227/255.0, alpha: 1.0)
    
}

