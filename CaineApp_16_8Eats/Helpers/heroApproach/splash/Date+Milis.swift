//
//  Date+Milis.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 07/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

