//
//  SplashPresenter.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 07/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import RxSwift

class SplashPresenter {
    
    static let TAG = "SplashPresenter"
    
    var view : SplashView
    var articles : [Article] = []
    
    let bgSchedular : ConcurrentDispatchQueueScheduler
    
    init(view : SplashView) {
        self.view = view
        bgSchedular = ConcurrentDispatchQueueScheduler(qos: .background)
        
        self.initArticles()
    }
    
    func requestArticles(){
        _ = self.getArticlesObservable(articlesList: self.articles)
            .observeOn(MainScheduler())
            .subscribeOn(self.bgSchedular)
            .subscribe(onNext: { [weak self] (listArticles) in
                print("\(SplashPresenter.TAG) : onNext()")
                self?.view.onArticlesListNext(articles: listArticles)
                }, onError: { [weak self] (err) in
                    print("\(SplashPresenter.TAG) : error: \(err)")
                    self?.view.onArticlesListError()
                }, onCompleted: {
                    print("\(SplashPresenter.TAG) : onCompleted()")
            } )
    }
}

//MARK :- Observable Functions
extension SplashPresenter {
    
    func getArticlesObservable(articlesList : [Article]) -> Observable<[Article]> {
        return Observable.create { observer in
            
            ArticleInstance.sharedInstance.articles = self.articles
            
            let currentDate : Date = Date()
            var installDate : Date = Date()
            
            let userDefaults = UserDefaults.standard
            
            if(userDefaults.double(forKey: "installDate") > 0){
                installDate = Date(milliseconds: Int64(userDefaults.double(forKey: "installDate")))
            }
            else {
                installDate = currentDate
                userDefaults.set(installDate.millisecondsSince1970, forKey: "installDate")
            }
            
            var resultArticles : [Article] = []
            
            for article in articlesList {
                let image = UIImage(named: article.imageName)
                
                //unlock the relevant articles
                //currentDate - Days
                let dateUnlocking = Calendar.current.date(byAdding: Globals.unit, value: -1 * article.unlockDays, to: currentDate)
                
                let isUnlocked = ((dateUnlocking?.millisecondsSince1970)! >= installDate.millisecondsSince1970)
                
                article.isLocked = !isUnlocked
                
                if(article.isLocked){
                    let grayImage = image?.toGrayscale()
                    article.image = grayImage
                }
                else {
                    article.image = image
                }
                
                let milisRemaining = Double( installDate.millisecondsSince1970 - (dateUnlocking?.millisecondsSince1970)!  )
                
                let daysRemaining =  milisRemaining * 1.0 / Globals.conversionDevisor
                
                article.dateDaysRemaining = Int(daysRemaining)
                
                let unlockingNotificationDate = Calendar.current.date(byAdding: Globals.unit, value: article.unlockDays, to: currentDate)
                
                article.unlockingNotificationDate = unlockingNotificationDate!
                
                resultArticles.append(article)
            }
            
            observer.onNext(resultArticles)
            observer.onCompleted()
            
            return Disposables.create()
        }
    }
    
}

extension SplashPresenter {
    func initArticles(){
        
        articles.append(Article("1. Caine's Story (Cover)", "Caine’s Story", index: 0, unlockDays: 1)) // 1
        articles.append(Article("2. 16:8 Intermittent Fasting (Cover)", "16:8 Intermittent Fasting - What is it?", index: 1, unlockDays: 1)) // 1
        articles.append(Article("3. How it Works (Cover)", "How it Works", index: 2, unlockDays: 1)) //  1
        articles.append(Article("4. Finding your Ideal Bodyweight (Cover)", "Finding Your Ideal Bodyweight", index: 3, unlockDays: 10)) // 15
        articles.append(Article("5. Fasted training. The benfits. (Cover)", "The Benefits of Fasted Training", index: 4, unlockDays: 12)) //17
        articles.append(Article("6. We Love Coffee! (Cover)", "We Love Coffee", index: 5, unlockDays: 15)) //20
        articles.append(Article("7. Eating Plan (Cover)", "Eating Plan", index: 6, unlockDays: 17)) // 22
        articles.append(Article("8. Clean Eating Grocery List (Cover)", "Clean Eating Grocery List", index: 7, unlockDays: 17))
        articles.append(Article("9. Your on the right Track! (Cover)", "You are on the right Track", index: 8, unlockDays: 24))
        articles.append(Article("10. Alcohol Awereness (Cover)", "Alcohol Awareness", index: 9, unlockDays: 25))
        articles.append(Article("11. Eat Me! (Cover)", "Eat Me", index: 10, unlockDays: 31))
        articles.append(Article("12. Eating Plan 2.0 (Cover)", "Eating Plan 2.0", index: 11, unlockDays: 31))
        articles.append(Article("13. Step out of your Comfort Zone (Cover)", "Step out of your Comfort Zone", index: 12, unlockDays: 37))
        articles.append(Article("14. This will change your Life! (Cover)", "This will change your Life!", index: 13, unlockDays: 38))
        articles.append(Article("15. Our Journey (Cover)", "Our Journey", index: 14, unlockDays: 40))
        articles.append(Article("16. Fueling your body with Fats (Cover 1)", "Fueling your body with Fats", index: 15, unlockDays: 42))
        articles.append(Article("17. Keytones (Cover)", "Keytones", index: 16, unlockDays: 43))
        articles.append(Article("18. Healthy Fats (Cover)", "Healthy Fats", index: 17, unlockDays: 43))
        articles.append(Article("19. Eating Plan 3.0 (Cover)", "Eating Plan 3.0", index: 18, unlockDays: 45))
        articles.append(Article("20. Caine's Secrets to Cardio (Cover)", "Caine's Secrets to Cardio", index: 19, unlockDays: 52))
        articles.append(Article("21. Lunchtime Workouts (Cover)", "Lunchtime Workouts", index: 20, unlockDays: 52))
        articles.append(Article("22. Dan's Secret Lifting (Cover)", "Dan's Secret to Heavy Lifting", index: 21, unlockDays: 52))
        
        // subract 1 from all days
        
        for article in articles {
            article.unlockDays = article.unlockDays - 1
        }
        
    }
}

