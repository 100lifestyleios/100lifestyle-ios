//
//  SplashMainViewController.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 03/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Hero
import Firebase
import Mixpanel
import ParseUI
import SDWebImage


class SplashMainViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    var presenter : SplashPresenter?
    var isNotificationClicked = false
    var notificationArticleTitle : String?
    var notificationArticleImageName : String?
    var emptyImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: "Splash Screen", properties: ["Screen name" : "Splash Screen"])
        presenter = SplashPresenter(view: self)
        InAppFactory.sharedInstance().requestProducts { (string ) in
            
        }
        InAppFactory.sharedInstance().checkExpirationDateIfUserHasPurchased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.requestArticles()
        let appDelegate = UIApplication.shared.delegate as! CaineAppDelegate
        appDelegate.appVisibleController = self as SplashMainViewController
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter = nil
    }
    
    deinit {
        print("\(self) deinit called")
    }
}

extension SplashMainViewController : SplashView {
    func onArticlesListError() {
        print("SplashMainViewController: onArticlesListError()")
    }
    
    func onArticlesListNext(articles: [Article]) {
        let appDelegate = UIApplication.shared.delegate as? CaineAppDelegate
        let userDefaults = UserDefaults.standard
        if(userDefaults.bool(forKey: "areNotificationsDone2")) {
            print("onArticlesListNext")

        } else {
            Analytics.logEvent("rescheduleNotifications", parameters: [
                "areNotificationsDone2": userDefaults.bool(forKey: "areNotificationsDone2")
                ])
            //cancel notifications and reschedule notifications
            UserNotificationUtils.cancelAllNotifications() 
            for article in articles {
                if(article.dateDaysRemaining > 0){
                    print("article: \(article.title) , notificationDate: \(article.unlockingNotificationDate)")
                    appDelegate?.scheduleNotification(date: article.unlockingNotificationDate, subTitle: article.title, body: "\(article.title) is unlocked now", imageName: article.imageName)
                }
            }
            userDefaults.set(true, forKey: "areNotificationsDone2")
        }
        ArticleInstance.sharedInstance.articles = articles
        fetchUserData()
    }
    
    func fetchUserData() {
        print("In Completion User")
        if ParseUser.current() == nil {
            print("In Welcome Screen")
            let controller = UIViewController.WelcomeViewController() as! WelcomeViewController
            UIApplication.shared.keyWindow?.rootViewController = controller
            return
        }
        
        if ParseUser.current() != nil {
            print(ParseUser.current())
            print("In Fetch User")
            ParseUser.current()?.fetchInBackground(block: { [weak self] (user, error) in
                if error == nil && user != nil {
                    print("User Fetched")
                    let newUser = user as? ParseUser
                    DispatchQueue.main.async(execute: {
                        LSUserDefaults.sharedInstance.fullName = newUser?.fullName
                        LSUserDefaults.sharedInstance.isFacebookLogin = newUser?.isFacebookLogin
                        LSUserDefaults.sharedInstance.purchaseStatus = newUser?.purchaseStatus
                        LSUserDefaults.sharedInstance.workOutDay = newUser?.workOutDay?.intValue
                        LSUserDefaults.sharedInstance.workoutReminderTime = newUser?.workoutTime
                        LSUserDefaults.sharedInstance.lastWorkOutDate = newUser?.lastWorkOutDate
                        print("Checking Photo")
                        if newUser?.profilePhoto != nil {
                            print("Photo Available")
                            self?.emptyImage.sd_setImage(with: URL(string: (newUser?.profilePhoto?.url)!), placeholderImage: nil, options: SDWebImageOptions.refreshCached, completed: { (image, error, type , url) in
                                print("Photo Downloaded")
                                self?.setWorkoutDay(user: newUser!)
                            })
                        }
                        else {
                            self?.setWorkoutDay(user: newUser!)
                        }
                    })
                } else {
                    print("user fetching failed")
                }
            })
        }
    }
    
    private func setWorkoutDay(user: ParseUser) {
        
        API.sharedInstance.getNumberOfWorkouts().continueWith { [weak self](task) -> Any? in
            if task.error != nil {
                print((task.error?.localizedDescription)!)
            }
            else {
                let totalWorkouts = task.result as! Int
                if getDate(Date()) > getDate(LSUserDefaults.sharedInstance.lastWorkOutDate!) {
                    var newWorkoutDay = LSUserDefaults.sharedInstance.workOutDay! + 1
                    if newWorkoutDay > totalWorkouts {
                        newWorkoutDay = newWorkoutDay % totalWorkouts
                    }
                    LSUserDefaults.sharedInstance.workOutDay = newWorkoutDay
                    user.workOutDay =  NSNumber(integerLiteral: LSUserDefaults.sharedInstance.workOutDay!)
                    user.lastWorkOutDate = getDate(Date())
                    
                }
                else {
                    var newWorkoutDay = LSUserDefaults.sharedInstance.workOutDay!
                    if newWorkoutDay > totalWorkouts {
                        newWorkoutDay = newWorkoutDay % totalWorkouts
                    }
                    LSUserDefaults.sharedInstance.workOutDay = newWorkoutDay
                    user.workOutDay =  NSNumber(integerLiteral: LSUserDefaults.sharedInstance.workOutDay!)
                }
                user.saveInBackground()
                self?.fetchNewWorkouts()
            }
            return nil
        }
    }
    
    func fetchNewWorkouts() {
        API.sharedInstance.saveNewWorkoutsInRealm().continueWith { [weak self] (task) -> Any? in
            if task.error != nil {
                print((task.error?.localizedDescription)!)
            }
            else {
                print("In Change Controller In Fetch New User Workouts")
                self?.changeController()
            }
            return nil
        }
    }
    
    
    private func changeController() {
        print("In Change Controller")
        let user = ParseUser.current()

        if  user?.purchaseStatus == nil || (user?.purchaseStatus?.int32Value)! <= NO_ACCOUNT {
            let controller = UIViewController.InAppPurchaseController()
            UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: controller)
            return
        }
        
        let workout = API.sharedInstance.getCurrentWorkout(isSynced: false)
        if workout == nil {
            API.sharedInstance.syncDataInDisk()
            let controller = UIViewController.tabbarController()!
            controller.initialSelectedIndex = 0
            UIApplication.shared.keyWindow?.rootViewController = controller
        }
        else {
            let controller = UIViewController.LoadingDataViewController() as! LoadingDataViewController
            controller.workout = workout
            UIApplication.shared.keyWindow?.rootViewController = controller
        }
    }
}
