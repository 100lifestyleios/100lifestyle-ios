//
//  UIViewController+URLOpener.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 22/06/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
extension UIViewController {
    func openURL(withURL url : URL ){
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}
