//
//  UpdateAppViewController.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 22/06/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

class UpdateAppViewController: UIViewController {
    
    var url : String?

    @IBAction func onBtnUpdateClick(_ sender: UIButton) {
        openURL(withURL: URL(string: url!)!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! CaineAppDelegate
        appDelegate.appVisibleController = self as UpdateAppViewController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
