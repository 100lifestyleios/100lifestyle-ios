//
//  InAppNotify+Extension.swift
//  ChallengeApp
//
//  Created by Mursaleen Moosa on 01/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit


func showToast(withMessage message: String) {
    DispatchQueue.main.async {
     InAppNotify.Show(Announcement(title:message  ,duration : 2.0), to: UIApplication.topViewController()! )
    }
}



