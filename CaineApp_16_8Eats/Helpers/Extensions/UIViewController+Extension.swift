//
//  UIViewController+Extension.swift
//  SnapLearn
//
//  Created by Mursaleen Moosa on 26/03/2018.
//  Copyright © 2018 isystematic. All rights reserved.
//

import UIKit


extension UIViewController {
    
    // MARK: - Main Storyboard
    
    internal class func SplashMainViewController() -> UIViewController {
        let story = UIStoryboard.mainStoryboard()
        return  (story.instantiateViewController(withIdentifier: "SplashMainViewController"))
    }
    
    
    
    internal class func InAppPurchaseController() -> UIViewController {
        let story = UIStoryboard.mainStoryboard()
        return  (story.instantiateViewController(withIdentifier: "InAppPurchaseController"))
    }
    
    
    
    // MARK: - Bootcamp Storyboard
    
    internal class func MainBootCampViewController() -> UIViewController {
        let story = UIStoryboard.bootcampStoryboard()
        return  (story.instantiateViewController(withIdentifier: "MainBootCampViewController"))
    }
    
    
    internal class func EntryPassViewController() -> UIViewController {
        let story = UIStoryboard.bootcampStoryboard()
        return  (story.instantiateViewController(withIdentifier: "EntryPassViewController"))
    }
    
    internal class func InviteFriendsViewController() -> UIViewController {
        let story = UIStoryboard.bootcampStoryboard()
        return  (story.instantiateViewController(withIdentifier: "InviteFriendsViewController"))
    }
    
    internal class func NearestBootCampViewController() -> UIViewController {
        let story = UIStoryboard.bootcampStoryboard()
        return  (story.instantiateViewController(withIdentifier: "NearestBootCampViewController"))
    }
    internal class func DailyWorkOutViewController() -> UIViewController {
        let story = UIStoryboard.bootcampStoryboard()
        return  (story.instantiateViewController(withIdentifier: "DailyWorkOutViewController"))
    }
    
    
    // MARK: - Registration Storyboard
    internal class func AddPhotoViewController() -> UIViewController {
        let story = UIStoryboard.registrationStoryboard()
        return  (story.instantiateViewController(withIdentifier: "AddPhotoViewController"))
    }
    
    internal class func WelcomeViewController() -> UIViewController {
        let story = UIStoryboard.registrationStoryboard()
        return  (story.instantiateViewController(withIdentifier: "WelcomeViewController"))
    }
    
    internal class func LoginViewController() -> UIViewController {
        let story = UIStoryboard.registrationStoryboard()
        return  (story.instantiateViewController(withIdentifier: "LoginViewController"))
    }
    
    internal class func SignupViewController() -> UIViewController {
        let story = UIStoryboard.registrationStoryboard()
        return  (story.instantiateViewController(withIdentifier: "SignupViewController"))
    }
    
    internal class func AddWorkoutTimerViewController() -> UIViewController {
        let story = UIStoryboard.registrationStoryboard()
        return  (story.instantiateViewController(withIdentifier: "AddWorkoutTimerViewController"))
    }
    
    internal class func LoadingDataViewController() -> UIViewController {
        let story = UIStoryboard.registrationStoryboard()
        return  (story.instantiateViewController(withIdentifier: "LoadingDataViewController"))
    }

    // MARK: - Profile Storyboard
    internal class func ProfileViewController() -> UIViewController {
        let story = UIStoryboard.profileStoryboard()
        return  (story.instantiateViewController(withIdentifier: "ProfileViewController"))
    }
    
    
}
