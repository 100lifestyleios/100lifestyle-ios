//
//  UILabelExtension.swift
//  Reel
//
//  Created by Mursaleen Moosa on 18/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit


extension UILabel {
    
    /**
     This funcation is automatic adjust font by
     screen size from given size
     **/
    func setFontByScreenSize(fontName name:String, fontSize givenSize:CGFloat) {
        if IS_IPHONE_6P {
            self.font = UIFont(name: name, size: givenSize)
        } else if IS_IPHONE_6 || IS_IPHONE_X {
            let newSize:CGFloat = givenSize-1
            self.font = UIFont(name: name, size: newSize)
        } else {
            let newSize:CGFloat = givenSize-2
            self.font = UIFont(name: name, size: newSize)
        }
    }
}
