//
//  UIView+Extension.swift
//  Reel
//
//  Created by Afnan on 04/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit
import JTMaterialSpinner
import Foundation


let spinnerTag = 111111110
let viewTag =    1111110


extension UIView {
    
    func  makeCornerRadiusView(withRaduius radius: Int)
    {
        layer.cornerRadius = (CGFloat )( radius)
        layer.masksToBounds = true
        layer.borderWidth = 0
    }
    func startScaleAnimation() {
        UIView.animate(withDuration: 0.8, animations: {
            self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }) { (finished) in
            UIView.animate(withDuration: 0.8, animations: {
                self.transform = CGAffineTransform.identity
            })
        }
    }
    
    func showSpinner() {
        
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        myView.backgroundColor = .black
        myView.alpha = 0.5
        self.addSubview(myView)
        myView.tag = viewTag
        
        let frameSize = CGRect(x: ((self.frame.size.width * 0.5) - 20), y: self.frame.size.height * 0.5, width: 40, height: 40)
        
        let spinnerView = JTMaterialSpinner(frame: frameSize)
        spinnerView.tag = spinnerTag
        // Customize the line width
        spinnerView.circleLayer.lineWidth = 3.0
        
        // Change the color of the line
        spinnerView.circleLayer.strokeColor = UIColor.spinnerColor.cgColor
        
        // Change the duration of the animation
        spinnerView.animationDuration = 2.5
        myView.addSubview(spinnerView)
        spinnerView.beginRefreshing()
        bringSubview(toFront: myView)
        
    }
    
    
    func showSmallSpinner() {
        
        let frameSize = CGRect(x: ((self.frame.size.width * 0.5) - (self.frame.size.height * 0.5)), y: self.frame.size.height * 0.5, width: self.frame.size.height, height: self.frame.size.height)
        
        let spinnerView = JTMaterialSpinner(frame: frameSize)
        spinnerView.tag = spinnerTag
        // Customize the line width
        spinnerView.circleLayer.lineWidth = 3.0
        
        // Change the color of the line
        spinnerView.circleLayer.strokeColor = UIColor.spinnerColor.cgColor
        
        // Change the duration of the animation
        spinnerView.animationDuration = 2.5
        addSubview(spinnerView)
        spinnerView.beginRefreshing()
        
    }
    
    func hideSmallSpinner() {
        guard let spinnerView = viewWithTag(spinnerTag) as? JTMaterialSpinner else { return }
        spinnerView.endRefreshing()
        spinnerView.removeFromSuperview()
     }
    
    
    func hideSpinner() {
        
        guard let view = viewWithTag(viewTag) else { return }
        guard let spinnerView = view.viewWithTag(spinnerTag) as? JTMaterialSpinner else { return }
        spinnerView.endRefreshing()
        spinnerView.removeFromSuperview()
        view.removeFromSuperview()
        
    }
    func applyDropShadow() {
        self.layer.shadowOffset = CGSize(width: 0, height: 8)
        self.layer.shadowColor = UIColor().UIColorFromRGB(0x969aa6).cgColor
        self.layer.shadowRadius = 3.0; //default is 3.0
        self.layer.shadowOpacity = 0.23; //default is 0.0
    }
    
    func reelCardDropShadow() {
        let shadowSize : CGFloat = 5.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: self.frame.size.height + shadowSize))
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor().UIColorFromRGB(0x969aa6).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 3.0
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    /**
     For reel card shadow
     **/
    func applyCardDropShodow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor().UIColorFromRGB(0x969aa6).cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        self.layer.shadowRadius = 3.0
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = 1
    }
    
    
     
    
    var snapshot : UIImage? {
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            self.layer.render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
    
    // Get Controller from View
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func makeRounded(cornerWithRadius radius : CGFloat) {
        layer.cornerRadius = (radius * UIScreen.main.bounds.width / 414) / 2
        layer.masksToBounds = true
        layer.borderWidth = 0
    }
}
