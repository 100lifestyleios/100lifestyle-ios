//
//  CloudExtension.swift
//  Reel
//
//  Created by Afnan on 11/01/2018.
//  Copyright © 2018 isystematic. All rights reserved.
//

import Foundation
import UIKit
import Parse


class CloudCode {
    
    class var sharedInstance :CloudCode {
        struct Singleton {
            static let instance = CloudCode()
        }
        return Singleton.instance
    }
    
    
    //.. ---------          PFCloud for Duplicate one signal id        --------------------
    func callOneSignalCloudFunction(onSignalID oneSignalD : String) {
        let oneSignalData : NSMutableDictionary  =   NSMutableDictionary.init()
        oneSignalData["oneSignalId"] = oneSignalD
        oneSignalData["userId"] = ParseUser.current()?.objectId
        
        /*
         PFCloud.callFunction(inBackground: "checkDuplicateOneSignalId", withParameters: oneSignalData as? [AnyHashable : Any]) { (success , error) in
         }
         */
    }
    
    /// Order Reel for Download
    func VerifyMultipleLoginForUser( )   {
        
        guard let knUser =  ParseUser().currentUser as? ParseUser else { return }
        
        let orderEmailData : NSMutableDictionary  =   NSMutableDictionary.init()
        
        orderEmailData["userId"] = knUser.objectId
 
        /*
        PFCloud.callFunction(inBackground: "VerifyMultipleLoginForUser", withParameters: orderEmailData as? [AnyHashable : Any]) { (success , error) in
            print("error VerifyMultipleLoginForUser 1 \(error?.localizedDescription)")
            
        }
 */
        
        
    }
    
    
    
   
    
    
}
