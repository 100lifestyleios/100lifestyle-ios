//
//  StringExtension.swift
//  Reel
//
//  Created by Afnan on 06/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import Foundation

internal extension String {
    var hasOnlyNewlineSymbols: Bool {
        return trimmingCharacters(in: CharacterSet.newlines).isEmpty
    }
}

extension String {
    
    func getFirstName () -> String
    {
        var components = self.components(separatedBy: " ")
        return (components.count > 0) ? components.removeFirst() : ""
        
    }
    func getLastName () -> String
    {
        let components = self.components(separatedBy: " ")
        let lastName: String? = components.count > 1 ? components[components.count - 1] : ""
        return lastName!
    }
    
    func getFirstCharacter () -> String
    {
        return  String(self.first!)
        
    }
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    // check email is valid
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    //MARK: Date format
    /** Date format 120-01-2017 **/
    func getStringFromDate(_ givenDate : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        let dateString = formatter.string(from: givenDate)
        return dateString
    }
    
    /// Date format Nov 2, 2017
    func getStringFromDateInMonthFormat(_ givenDate : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        let dateString = formatter.string(from: givenDate)
        return dateString
    }
 
    
    var isStringEmpty : Bool {
        let string = self
        let characterSet = CharacterSet.whitespacesAndNewlines.inverted   //whitespacesAndNewlines.invertedSet
        let range =  string.rangeOfCharacter(from: characterSet)  //string.rangeOfCharacterFrom(characterSet)
        let ok = string.isEmpty || range == nil
        return ok
    }
    
//     func getTimeDifferenceString(_ time : Date) -> String {
//        let minutes = time.minutesUntil%60
//        let hours = time.hoursUntil
//        let timeDifference = String(format: "%02d:%02d", hours, minutes)
//        return timeDifference
//    }

    //MARK: Fix Extension URL
    
   func facebookProfileImageThumbnailURL () -> String{
    return "https://graph.facebook.com/\(self)/picture?width=200&height=200"
     }
    
    func getGoogleSearchURL () -> String{
        return "http://www.google.com/search?q=\(self)"
    }
    
    
    func getImageName () -> String{
        let timeInterval : Int = Int((Date().timeIntervalSince1970).rounded())
        let imageName = "thumbnail_\(timeInterval))"
        
        return imageName
    }
//
//    func getVideoName () -> String{
//        let timeInterval : Int = Int((Date().timeIntervalSince1970).rounded())
//        let imageName = "video_\(timeInterval)_\(ApplicationDefault.sharedInstance.getVideoNumberToSend())"
//        
//        return imageName
//    }
    
    func cleanPhoneNumber() -> String {
        let phoneNubmerCharacterSet = NSMutableCharacterSet(charactersIn: "+")
        phoneNubmerCharacterSet.formUnion(with: .decimalDigits)
        let cleanPhoneNumber = self.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")
        return cleanPhoneNumber
    }
    
    func cleanString() -> String {
        let cleanString = self.components(separatedBy: NSCharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz").inverted).joined(separator: "")
        return cleanString.lowercased()
    }
    
    func addThumbnailURL() -> String {
        let thumbnailURL = self.replacingOccurrences(of: "upload/", with: "upload/c_fill,g_face,h_\(THUMBNAIL_SIZE),w_\(THUMBNAIL_SIZE)/")
        return thumbnailURL
    }
    
    func addGIFURL(withWidth width : Int, andHeight height : Int ) -> String {
        let gifURL = self.replacingOccurrences(of: "upload/", with: "upload/c_fill,du_3,w_\(width),h_\(height)/")
        return gifURL
    }
    
    /**
     *  if url  =  http://res.cloudinary.com/dngzlm8j5/image/upload/v1536149660/j7nu6N41uG/image_1536149656_10.move
     *  we will replace mov with gif
     *  and will assign specific height and width for our image
     *  fl_lossy for low quality. but it shows some blur
     **/
    func videoToGIFURL(withWidth width : Int, andHeight height : Int ) -> String {
        var videoURL = self
        videoURL = videoURL.replacingOccurrences(of: ".mov", with: ".gif")
        let gifURL = videoURL.replacingOccurrences(of: "upload/", with: "upload/fl_lossy,c_fill,w_\(width),h_\(height)/")
        return gifURL
    }
    
    /**
     *  if url  =  http://res.cloudinary.com/dngzlm8j5/image/upload/v1536149660/j7nu6N41uG/image_1536149656_10.png.png
     *  we remove /v1536149660/ becuase it just shows version number and we dont need
     *  and it creates different URL before and after upload
     **/
    func cleanImageUrlForSDWebImage() -> String {
        
        let fullNameArr = self.components(separatedBy: "/")
        var count = 0
        var cleanString = ""
        for urlPart in fullNameArr{
            count = count + 1
            if count == 7 { continue}
            cleanString = "\(cleanString)/\(urlPart)"
        }
        cleanString.removeFirst()
        return cleanString
        
    }
    
    
    func getAllHashTags() -> NSArray {
        /*
        let text = self.text
        let tags: [String] = (text?.components(separatedBy: " "))!
        
        for tag in tags {
            if tag.hasPrefix("#") {
                print(tag)
            }
        }
        */
        let range = NSRange(location: 0, length: self.utf16.count)
        let matches = RLRegex.getElements(from: self, with: RLRegex.hashtagPattern, range: range)
        let nsString = self as NSString
        let tagsArray = NSMutableArray()
        for match in matches where match.range.length > 2 {
            let range = NSRange(location: match.range.location + 1, length: match.range.length - 1)
            var word = nsString.substring(with: range)
            if word.hasPrefix("@") {
                word.remove(at: word.startIndex)
            }
            else if word.hasPrefix("#") {
                word.remove(at: word.startIndex)
                tagsArray.add(word.lowercased())
            }
            
        }
        return tagsArray as NSArray
    }
    
    
    
    func  StringArrayToCommaSeparatedString(fromArray  array :  NSMutableArray?) -> String
    {
         var tempArray  : String = ""
        if array == nil || array!.count == 0  { return tempArray }
        for pathName in array!
        {
            tempArray = "\(tempArray)\(pathName),"
         }
         tempArray = tempArray.substring(to: tempArray.count - 1 )
        return tempArray;
    }
    
    func getLocationURL(withLatitude latitude: Double, withLongitude longitue:Double) -> URL {
        
        let string = "https://maps.googleapis.com/maps/api/staticmap?key=\(GOOGLE_MAPS_API_KEY)&center=\(latitude),\(longitue)&zoom=16&format=png&maptype=roadmap&style=element:geometry%7Ccolor:0xf5f5f5&style=element:labels.icon%7Cvisibility:off&style=element:labels.text.fill%7Ccolor:0x616161&style=element:labels.text.stroke%7Ccolor:0xf5f5f5&style=feature:administrative.land_parcel%7Celement:labels.text.fill%7Ccolor:0xbdbdbd&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:poi.park%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:road%7Celement:geometry%7Ccolor:0xffffff&style=feature:road.arterial%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:road.highway%7Celement:geometry%7Ccolor:0xdadada&style=feature:road.highway%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:road.local%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:transit.line%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:transit.station%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:water%7Celement:geometry%7Ccolor:0xc9c9c9&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&size=480x480&markers=icon:https://goo.gl/uQDHWK%7C\(latitude),\(longitue)"
        
        let url = URL(string: string)
        return url!
    }
 
    
    
    
}

//MARK: Substring methods
extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}

struct RLRegex {
    static let hashtagPattern = "(?:^|\\s|$)#[\\p{L}0-9_]*"
    static let mentionPattern = "(?:^|\\s|$|[.])@[\\p{L}0-9_]*"
    static let urlPattern = "(^|[\\s.:;?\\-\\]<\\(])" +
        "((https?://|www\\.|pic\\.)[-\\w;/?:@&=+$\\|\\_.!~*\\|'()\\[\\]%#,☺]+[\\w/#](\\(\\))?)" +
    "(?=$|[\\s',\\|\\(\\).:;?\\-\\[\\]>\\)])"
    
    private static var cachedRegularExpressions: [String : NSRegularExpression] = [:]
    
    static func getElements(from text: String, with pattern: String, range: NSRange) -> [NSTextCheckingResult]{
        guard let elementRegex = regularExpression(for: pattern) else { return [] }
        return elementRegex.matches(in: text, options: [], range: range)
    }
    
    private static func regularExpression(for pattern: String) -> NSRegularExpression? {
        if let regex = cachedRegularExpressions[pattern] {
            return regex
        } else if let createdRegex = try? NSRegularExpression(pattern: pattern, options: [.caseInsensitive]) {
            cachedRegularExpressions[pattern] = createdRegex
            return createdRegex
        } else {
            return nil
        }
    }
}

//extension StringProtocol where Index == String.Index {
//    func index(of string: Self, options: String.CompareOptions = []) -> Index? {
//        return range(of: string, options: options)?.lowerBound
//    }
//    func endIndex(of string: Self, options: String.CompareOptions = []) -> Index? {
//        return range(of: string, options: options)?.upperBound
//    }
//    func indexes(of string: Self, options: String.CompareOptions = []) -> [Index] {
//        var result: [Index] = []
//        var start = startIndex
//        while start < endIndex,
//            let range = self[start..<endIndex].range(of: string, options: options) {
//                result.append(range.lowerBound)
//                start = range.lowerBound < range.upperBound ? range.upperBound :
//                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
//        }
//        return result
//    }
//    func ranges(of string: Self, options: String.CompareOptions = []) -> [Range<Index>] {
//        var result: [Range<Index>] = []
//        var start = startIndex
//        while start < endIndex,
//            let range = self[start..<endIndex].range(of: string, options: options) {
//                result.append(range)
//                start = range.lowerBound < range.upperBound ? range.upperBound :
//                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
//        }
//        return result
//    }
//}

