//
//  UITextFeild+Extension.swift
//  Reel
//
//  Created by Afnan on 07/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit

extension UITextField {

    func setLeftPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setRightPaddingImage(_ imageView : UIImageView) {
//        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = imageView
        self.rightViewMode = .always
    }
    
    func setRightPaddingView(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setFontByScreenSize(fontName name:String, fontSize givenSize:CGFloat) {
        if IS_IPHONE_6P {
            self.font = UIFont(name: name, size: givenSize)
        } else if IS_IPHONE_6 || IS_IPHONE_X {
            let newSize:CGFloat = givenSize-1
            self.font = UIFont(name: name, size: newSize)
        } else {
            let newSize:CGFloat = givenSize-2
            self.font = UIFont(name: name, size: newSize)
        }
//        disableAutoCorrection()
    }
    
//    func disableAutoCorrection() {
//        textContentType = .nickname
//        autocorrectionType = .no
//        spellCheckingType = .no
//        smartDashesType = .no
//        smartQuotesType = .no
//        smartInsertDeleteType = .no
//    }
    
}

extension UITextView {
    
    func setFontByScreenSize(fontName name:String, fontSize givenSize:CGFloat) {
        if IS_IPHONE_6P {
            self.font = UIFont(name: name, size: givenSize)
        } else if IS_IPHONE_6 || IS_IPHONE_X {
            let newSize:CGFloat = givenSize-1
            self.font = UIFont(name: name, size: newSize)
        } else {
            let newSize:CGFloat = givenSize-2
            self.font = UIFont(name: name, size: newSize)
        }
//        disableAutoCorrection()
    }
    
//    func disableAutoCorrection() {
//        textContentType = .nickname
//        autocorrectionType = .no
//        spellCheckingType = .no
//        smartDashesType = .no
//        smartQuotesType = .no
//        smartInsertDeleteType = .no
//    }
    
}


