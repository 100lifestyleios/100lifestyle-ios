//
//  UIApplication+Extension.swift
//  Reel
//
//  Created by Afnan on 09/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit
import Parse


extension UIApplication {
    
//    func initialzeAppData() {
//        self.next
//    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
//    private static let runOnce: Void = {
//        ImageHelper.sharedInstance.createDirectory()
//        KNUser.registerSubclass()
//        PFUser.registerSubclass()
//    }()
    
    // It will initialze all classes spcilly of pfObject
    // becuase we can not use Load and initialze anymore
//    override open var next: UIResponder? {
//        // Called before applicationDidFinishLaunching
//        UIApplication.runOnce
//        return super.next
//    }
    
}
