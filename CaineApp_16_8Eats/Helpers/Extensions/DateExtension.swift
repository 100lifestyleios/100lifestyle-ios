//
//  DateExtension.swift
//  Reel
//
//  Created by Mursaleen Moosa on 22/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit
import DateToolsSwift

extension Date {
    
    var  formatter: DateFormatter { return DateFormatter() }
    
    func getDateFromString(_ givenString: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        let date = formatter.date(from: givenString)!
        return date
    }
    
    public func getStringFromDate() -> String {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from:self)
    }
    
 
 
    
    
    /** Return a user friendly hour */
    func getCurrentDateTime() -> String {
        // Customize a date formatter
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        
        return formatter.string(from: self)
    }
    
    /**  return current date and time in long logn format*/
    func timestamp() -> Int64{
        //----------------------------------------------------------------------------------------------------------------
         return Int64(self.timeIntervalSince1970 * 1000);
    }
    
    
    func getBasicDate() -> Date {
    return  (Date() as NSDate).subtractingYears(10)
    }
 
 
    
      func getDate(fromTimeStamp timeStamp : Int64) -> Date{
         return Date(timeIntervalSince1970: TimeInterval(timeStamp / 1000))
    }
    
    func getCurrentTimeInUTC() -> Int64{
        let date = Date()
        // "Jul 23, 2014, 11:01 AM" <-- looks local without seconds. But:
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let defaultTimeZoneStr = formatter.string(from: date)
        // "2014-07-23 11:01:35 -0700" <-- same date, local, but with seconds
        formatter.timeZone = NSTimeZone(abbreviation: "UTC") as! TimeZone
        let utcTimeZoneStr = formatter.string(from: date)
        let seconds = date.timeIntervalSince1970
        return Int64(seconds) 
    }
    
}
