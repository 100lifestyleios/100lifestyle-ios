//
//  UIButton+Extension.swift
//  Quixly
//
//  Created by isystematic on 8/7/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

extension UIButton {
    
    
    //Default Font for Buttons in Entire Application
    func setDefaultFont() {
        setFont(withName: CLANOT_NEWS, withSize: 18)
    }
    
    func setFont(withName name : String , withSize givenSize : CGFloat) {
        var newSize : CGFloat = 0.0
        if IS_IPHONE_6P {
            self.titleLabel?.font = UIFont(name: name, size: givenSize)
        } else if IS_IPHONE_6 || IS_IPHONE_X {
            newSize = givenSize-1
            self.titleLabel?.font = UIFont(name: name, size: newSize)
        } else {
            newSize = givenSize-3
            self.titleLabel?.font = UIFont(name: name, size: newSize)
        }
        print("The Given Size is : \(givenSize) and New Size is : \(newSize)")
    }
}
