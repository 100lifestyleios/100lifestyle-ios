//
//  UserDefaults+Extension  .swift
//  ChallengeApp
//
//  Created by Mursaleen Moosa on 02/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    subscript<T>(key: String) -> T? {
        get {
            return value(forKey: key) as? T
        }
        set {
            set(newValue, forKey: key)
        }
    }
}
