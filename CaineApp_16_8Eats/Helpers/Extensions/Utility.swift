//
//  Utility.swift
//  Reel
//
//  Created by Afnan on 09/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit


/**
 This method convert font size by screen
 From given size
 **/

func getSize(byScreen size : CGFloat) -> CGFloat {
    if IS_IPHONE_6P {
        return size
    } else if IS_IPHONE_6 || IS_IPHONE_X {
        let newSize:CGFloat = size-1
        return newSize
    } else {
        let newSize:CGFloat = size-3
        return newSize
    }
}


func setWorkoutTime(_ date: Date) -> Date {
    let calander = Calendar.current
    var dateComponents = Calendar.current.dateComponents([.year , .month , .day , .hour , .minute], from: date)
    dateComponents.year = 1970
    dateComponents.month = 1
    dateComponents.day = 1
    let newDate = calander.date(from: dateComponents)
    return newDate!
}

func getDate(_ date: Date) -> Date {
    let calander = Calendar.current
    var dateComponents = Calendar.current.dateComponents([.year , .month , .day , .hour , .minute], from: date)
    dateComponents.hour = 0
    dateComponents.minute = 0
    dateComponents.second = 0
    let newDate = calander.date(from: dateComponents)
    return newDate!
}

func getTime(byDate date: Date) -> String {
    let formatter = DateFormatter()
    formatter.timeStyle = .short
    return formatter.string(from: date)
}
