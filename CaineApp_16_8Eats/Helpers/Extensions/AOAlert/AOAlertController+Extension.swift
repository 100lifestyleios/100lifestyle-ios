//
//  AOAlertController+Extension.swift
//  ChallengeApp
//
//  Created by Mursaleen Moosa on 24/07/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

extension AOAlertController {
    public func configureFontAndColor() {
        self.backgroundColor = .white
        self.titleFont = UIFont(name: CLANOT_MEDIUM, size: getSize(byScreen: 18))
        self.messageFont = UIFont(name: CLANOT_NEWS, size: getSize(byScreen: 14))
        self.titleColor = UIColor().UIColorFromRGB(0x373946)
        self.messageColor = UIColor().UIColorFromRGB(0x797979)
    }
}


extension AOAlertAction {
    public func configureDefaultAction() {
        self.color = UIColor().UIColorFromRGB(0xED244A)
        self.font = UIFont(name: CLANOT_NEWS, size: getSize(byScreen: 18))
    }
    public func configureCancelAction() {
        self.color = UIColor().UIColorFromRGB(0x515154)
        self.font = UIFont(name: CLANOT_NEWS, size: getSize(byScreen: 18))
    }
}
