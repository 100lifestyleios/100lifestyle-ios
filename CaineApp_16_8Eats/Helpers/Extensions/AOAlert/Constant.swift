//
//  Constant.swift
//  SnapLearn
//
//  Created by Mursaleen Moosa on 28/03/2018.
//  Copyright © 2018 isystematic. All rights reserved.
//

import UIKit

let APP_NAME                              =  "Knack"
let DEFAULT_COUNTRY_CODE                  =  "+92" // "+61"
let DEFAULT_COUNTRY_NAME                  =  "Pakistan"//"Australia"
let GOOGLE_MAPS_API_KEY                   =  "AIzaSyDrXnFox80L8pju-5XAvs8KvaKGAov0KIw"
let GOOGLE_MAPS_PLACES_API_KEY            =  "AIzaSyA03mh5b49M-_Df_mGKR2ZvuU44cMLme28"


let INTERNET_CONNECTION_ERROR =     "Please check your internet connection"

let  MINIMUM_AGE = 18

let CONTACT_US = "https://100clubapp.com/contact-us/"
let ABOUT_100LIFESTYLE = "https://100clubapp.com/100-lifestyle/"
let INVITE_MESSAGE = " has invited you to join 100 Lifestyle, to get entry passes to bootcamps, daily workouts and fitness tips for free.\n"


/**
 // For Video uploading
 https://cloudinary.com/documentation/ios_video_manipulation
 https://cloudinary.com/documentation/video_manipulation_and_delivery#creating_animated_gif_and_animated_webp
 
 
 /********************************* Cloudinary App Keys ***************************************************/
 
 For Download image
 https://cloudinary.com/blog/cloudinary_s_ios_sdk_makes_a_swift_move_for_mobile_image_management
 
 For setting preset
 https://cloudinary.com/console/upload_presets/new
 **/

let CLOUDINARY_NAME     =       "isystematic"
let CLOUDINARY_API_KEY  =       "797698537478635"
let CLOUDINARY_PRESET   =       "kprzadt4"
let THUMBNAIL_SIZE     =       200

let ONE_SIGNAL_KEY =                "8a87de0e-f5c9-4e2f-bcee-a4aa6f0e407d"


let MIX_PANEL_TOKEN =        "ba7bc4b93ab7e794c7545f40df7e80a3"



let  KWorkOutTableName   =                       "Workout"
let screenWidthRatio = UIScreen.main.bounds.width / 414

/********************************* Parse App Keys ***************************************************/

/*
    There are two instance of parse database, one for debugging and another for app store
    Please make sure when we upload build for app store Choose Live Parse Instance.
    For Debugging and testing use debugging version of Parse Intance
*/

//
//let GOOGLE_MAP_KEY =                "AIzaSyDUxnGhyFKUTMzWrwmDRnKYjsRlnknQb34"
//let GOOGLE_PLACES_KEY =             "AIzaSyDHWdvzmWr-PCu5coobM8qqhaIasUuNMqg"

/*********************************** UIDevices *********************************************/

let IS_IPAD  =              (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
let IS_IPHONE =             (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)

let SCREEN_WIDTH =          (UIScreen.main.bounds.size.width)
let SCREEN_HEIGHT =         (UIScreen.main.bounds.size.height)
let SCREEN_MAX_LENGTH =     (max(SCREEN_WIDTH, SCREEN_HEIGHT))
let SCREEN_MIN_LENGTH =     (min(SCREEN_WIDTH, SCREEN_HEIGHT))

let IS_IPHONE_4_OR_LESS =   (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
let IS_IPHONE_5 =           (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
let IS_IPHONE_6 =           (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
let IS_IPHONE_6P =          (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
let IS_IPHONE_X =           (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)



let kAttributedTitle = "attributedTitle"
let kAttributedMessage = "attributedMessage"


/********************** Cell Identifiers ********************************/

let PROFILECELL = "profileCell"
let PROFILEWORKOUTCELL = "profileWorkoutCell"
let EDITPROFILECELL = "editProfileCell"
let DEALSLISTSECTIONHEADER = "dealsListSectionHeader"
let DEALSLISTCELL = "dealsListCell"
let DEALSCOLLECTIONCELL = "dealsCollectionCell"
let DEALDETAILPHOTOSCOLLECTIONCELL = "dealDetailPhotosCollectionCell"
let DEALDETAILINFOCELL = "dealInfoCell"
let DEALDETAILABOUTCELL = "dealAboutCell"
let DEALDETAILRELATIVEDEALCELL = "relativeDealsCell"
let DEALDETAILCONTACTCELL = "dealContactCell"
let INTERESTEDDEALCELL = "interestedDealCell"
let LOCATIONSELECTIONCELL = "locationSelectionCell"
let WORKOUTCELL = "workoutCell"


/********************** Segue Identifiers ********************************/

let DEAL_BASICINFO_IDENTIFIER = "dealBasicInfoSegue"



/********************** Registration Constants ********************************/

let FIRSTNAME =         "First name"
let LASTNAME =          "Last name"
let EMAIL =             "Email address"
let PASSWORD =          "Password"
let PURCHASE_STATUS =   "purchaseStatus"


/********************** Fonts Names ********************************/

let CLANOT_BOLD = "ClanOT-Bold"
let CLANOT_BOOK = "ClanOT-Book"
let CLANOT_EXTENDED_BOLD = "ClanOT-ExtdBold"
let CLANOT_EXTENDED_BOOK = "ClanOT-ExtdBook"
let CLANOT_EXTENDED_MEDIUM = "ClanOT-ExtdMedium"
let CLANOT_EXTENDED_NEWS = "ClanOT-ExtdNews"
let CLANOT_MEDIUM = "ClanOT-Medium"
let CLANOT_NEWS = "ClanOT-News"


/********************** Class Identifiers ********************************/

let GUESTCONTROLLER = "guest"
let PROFILECONTROLLER = "profile"


/********************** User Types ********************************/

let CUSTOMER = "customer"
let STORE = "store"


/********************** Parse ********************************/

let CREATED_AT =            "createdAt"
let WORKOUT_NAME =          "workoutName"
let WORKOUT_IMAGE_URL =     "workoutImageUrl"
let WORKOUT_DURATION =      "workoutDuration"
let WORKOUT_VIDEO_URLS =    "workoutVideoUrls"
let WORKOUT_VIDEO_TITLES =  "workoutVideoTitles"
let PUBLISH_DATE =          "publishDate"
let IS_PUBLISHED =          "isPublished"
let IS_DELETED  =           "isDeleted"

/********************** Parse ********************************/


let        FNOTIFICATION_TYPE          =             "notification_type"
let        FNOTIFICATION_WORK_OUT    =              1



