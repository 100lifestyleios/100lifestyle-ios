//
//  UIColor+Extension.swift
//  Reel
//
//  Created by Afnan on 04/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit

extension UIColor {
    
    func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    open class var spinnerColor : UIColor {
        get {
            return UIColor().UIColorFromRGB(0x43B4E7)
        }
    }
    
    open class var toastColor : UIColor {
        get {
            return UIColor().UIColorFromRGB(0xED244A)
        }
    }
}
