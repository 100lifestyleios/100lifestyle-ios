//
//  UILabel+Extension.swift
//  Qixley
//
//  Created by Salman Khan on 18/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit


extension UILabel {
    
    
    func setFontSize(withName name:String, fontSize givenSize:CGFloat) {
        if IS_IPHONE_6P {
            self.font = UIFont(name: name, size: givenSize)
        } else if IS_IPHONE_6 || IS_IPHONE_X {
            let newSize:CGFloat = givenSize-1
            self.font = UIFont(name: name, size: newSize)
        } else {
            let newSize:CGFloat = givenSize-2
            self.font = UIFont(name: name, size: newSize)
        }
    }
    
    
    func setButtonTitleFont() {
        if IS_IPHONE_6P {
            self.font = UIFont(name: CLANOT_MEDIUM, size: 20)
        } else if IS_IPHONE_6 || IS_IPHONE_X {
            self.font = UIFont(name: CLANOT_MEDIUM, size: 19)
        } else {
            self.font = UIFont(name: CLANOT_MEDIUM, size: 18)
        }
    }
    
//    func setFontSize() {
//        if IS_IPHONE_6P {
//            self.font = UIFont(name: SNAP_FONT_BOLD, size: 20)
//        } else if IS_IPHONE_6 || IS_IPHONE_X {
//            self.font = UIFont(name: SNAP_FONT_BOLD, size: 19)
//        } else {
//            self.font = UIFont(name: SNAP_FONT_BOLD, size: 18)
//        }
//    }
    
}
