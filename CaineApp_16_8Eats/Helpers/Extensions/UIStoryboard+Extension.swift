//
//  UIStoryboard+Extension.swift
//  SnapLearn
//
//  Created by Mursaleen Moosa on 26/03/2018.
//  Copyright © 2018 isystematic. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    internal class func mainStoryboard() -> UIStoryboard {
        return UIStoryboard.init(name: "Main", bundle: nil)
    }
    
    internal class func registrationStoryboard() -> UIStoryboard {
        return UIStoryboard.init(name: "Registration", bundle: nil)
    }
    
    internal class func profileStoryboard() -> UIStoryboard {
        return UIStoryboard.init(name: "Profile", bundle: nil)
    }
    
    internal class func bootcampStoryboard() -> UIStoryboard {
        return UIStoryboard.init(name: "Bootcamp", bundle: nil)
    }
}

