//
//  ImageHelper.swift
//  ChallengeApp
//
//  Created by Mursaleen Moosa on 19/06/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//


import UIKit
import Disk


class ImageHelper  {
    
    class var sharedInstance : ImageHelper {
        struct Singleton {
            static let instance = ImageHelper()
        }
        return Singleton.instance
    }
    
    func  createDirectory() {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent(APP_NAME)
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
            try Disk.doNotBackup(APP_NAME, in: .documents)
            
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
    }
    
    
    func saveInDocuments(data imgageData :  Data , name fileName : String) {
        do {
            try Disk.save(imgageData, to: .documents, as: "\(APP_NAME)/\(fileName)")
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
    }
    
    func getFromDocuments(name fileName : String) -> Data? {
        if  ifExistInDocument(name: fileName) ==  false { return nil}
        
        do {
            let retrievedData = try  Disk.retrieve("\(APP_NAME)/\(fileName)", from: .documents, as: Data.self)
            return retrievedData
        }
        catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func  ifExistInDocument (name fileName :  String) -> Bool {
        return  Disk.exists("\(APP_NAME)/\(fileName)", in: .documents)
    }
    
    func  deleteFromDocument (name fileName :  String) {
        if  ifExistInDocument(name: fileName) ==  false { return }
        do {
            try Disk.remove("\(APP_NAME)/\(fileName)", from: .documents)
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func getVideoURL(name fileName :  URL) -> URL?{
        if  ifExistInDocument(name: fileName.lastPathComponent) ==  false { return nil}
        do {
           let url = try Disk.getURL("\(APP_NAME)/\(fileName.lastPathComponent)", from: .documents)
            print("REAL url \(url)")
            return url
        }
        catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }
    
    //MARK: In Temporary file
    func saveInTemporary(data imgageData :  Data , name fileName : String) -> URL {
        var fullURL : URL?
        do {
            let directory = NSTemporaryDirectory()
            fullURL = NSURL.fileURL(withPathComponents: [directory, fileName])
            try imgageData.write(to: fullURL!)
            
            try Disk.save(imgageData, to: .temporary, as: "\(APP_NAME)/\(fileName)")
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return fullURL!
    }
    
    func getTempURL(withName fileName : String) -> URL {
        let directory = NSTemporaryDirectory()
        let fullURL = NSURL.fileURL(withPathComponents: [directory, fileName])
        return fullURL!
    }
    
    
     
    
    // Download next gif image
    func downloadNextGifImage(ofURL urlString : String?){
        if (urlString == nil || (urlString?.isStringEmpty)!) {return }
        let cleanImageURL = urlString?.cleanImageUrlForSDWebImage()
        let imageURL =  URL(string : cleanImageURL!)
        
        // If already exit then leave
        if ImageHelper.sharedInstance.ifExistInDocument(name: (imageURL?.lastPathComponent)!) == true  {  return }
        DispatchQueue.global(qos: .background).async {  () -> Void in

        let data : Data? = try?  Data(contentsOf: imageURL!)
        if (data == nil)  {return}
             ImageHelper.sharedInstance.saveInDocuments(data: data!, name: (imageURL?.lastPathComponent)!)
        }
        
    }
}
