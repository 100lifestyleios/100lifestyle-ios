//
//  LoginWithFacebook.swift
//  ChallengeApp
//
//  Created by isystematic on 7/30/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Parse


let FACEBOOK_AUTH_TYPE = "facebook"
let FACEBOOK_IMAGE_URL = "https://graph.facebook.com/\(LSUserDefaults.sharedInstance.facebookId!)/picture?width=1280&height=1280"

protocol LoginWithFacebookDelegate {
    func facebookLogin(withSuccess success : Bool)
    func facebookLogin(withError error : Error)
}

class LoginWithFacebook: NSObject {
    
    var controller : UIViewController!
    var facebookDelegate : LoginWithFacebookDelegate?
    var kFacebookPermissionsArrayKey = "kFacebookPermissionsArrayKey"
    let kIsAlreadyLoggedInKey = "kIsAlreadyLoggedInKey"
    
    func facebookPermissions() -> [Any]? {
        var DDASLLoggerOnceToken: Int = 0
        if (DDASLLoggerOnceToken == 0) {
            let permissions = ["public_profile","email", "user_location", "user_hometown", "user_website", "user_about_me", "user_photos", "user_friends", "user_birthday"]
            objc_setAssociatedObject(self, &kFacebookPermissionsArrayKey, permissions, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        DDASLLoggerOnceToken = 1
        return objc_getAssociatedObject(self, &kFacebookPermissionsArrayKey) as? [Any]
    }
    
    func facebookLogin() {
        PFUser.logOut()
        PFFacebookUtils.logInInBackground(withReadPermissions: ["public_profile"] as? [String]) {  (user, error) in
            if error == nil && user == nil {
                self.facebookDelegate?.facebookLogin(withSuccess: false)
                return
            }
            if error != nil {
                self.facebookDelegate?.facebookLogin(withError: error!)
                return
            }
            if user != nil {
                let token = FBSDKAccessToken.current()!
                LSUserDefaults.sharedInstance.facebookId = token.userID
                LSUserDefaults.sharedInstance.facebookAuthToken = token.tokenString
                //LSUserDefaults.sharedInstance.isFacebookLinked = true
                LSUserDefaults.sharedInstance.objectId = user?.objectId
                if token.expirationDate.compare(Date()) != .orderedDescending {
                    let login = FBSDKLoginManager()
                    login.logIn(withReadPermissions: self.facebookPermissions(), from: self.controller, handler: { (result, error) in
                        if error != nil || (result?.isCancelled)! {
                            self.facebookDelegate?.facebookLogin(withError: error!)
                        } else {
                            if PFUser.current() != nil {
                                self.signupUserWithFacebook(user as! ParseUser)
                            }
                        }
                    })
                } else {
                    if PFUser.current() != nil {
                        self.signupUserWithFacebook(user as! ParseUser)
                    }
                }
            }
        }
    }
    
    func getAllFacebookFriends() -> BFTask<AnyObject> {
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        let parameters = ["fields": "id, first_name, last_name, middle_name, name"]
        let graphPath = "/me/friends"
        FBSDKGraphRequest(graphPath: graphPath, parameters: parameters).start(completionHandler: { connection, result, error in
            if error != nil {
                taskSource.set(error: error!)
            } else if result != nil {
                taskSource.set(result: result as AnyObject)
            }
        })
        return taskSource.task
    }
    private func signupUserWithFacebook(_ user : ParseUser) {
        let token = FBSDKAccessToken.current()
        LSUserDefaults.sharedInstance.objectId = user.objectId
        let authData = ["id": token?.userID, "access_token": token?.tokenString, "expiration_date": pffb_preciseDateFormatter().string(from: (token?.expirationDate)!)]
        user.linkWithAuthType(inBackground: FACEBOOK_AUTH_TYPE, authData: authData as! [String : String]).continueWith(block: { t -> Any? in
            if t.error != nil {
                return nil
            } else {
                return self.getFacebookUserDetails()?.continueWith(block: {  (task) -> Any? in
                    let dictionary = task.result as! NSDictionary
                    let fullName = dictionary.value(forKey: "name")
                    let facebookId = dictionary.value(forKey: "id")
                    if facebookId != nil {
                        LSUserDefaults.sharedInstance.facebookId = (facebookId as! String)
                    }
                    if fullName != nil {
                        LSUserDefaults.sharedInstance.fullName = (fullName as! String)
                    }
                    user.fullName = fullName as? String
                    user.saveInBackground()
                    print("Result Dictionary \(dictionary)")
                    self.facebookDelegate?.facebookLogin(withSuccess: true)
                    return nil
                })
            }
        })
    }
    private func getFacebookUserDetails() -> BFTask<AnyObject>? {
        let graphPath = "/me?fields=birthday,name,email,gender" //if you want to picture of 200, 200 add parameter picture.type(large)
        let taskSource = BFTaskCompletionSource<AnyObject>()
        FBSDKGraphRequest(graphPath: graphPath, parameters: nil).start(completionHandler: { connection, result, error in
            if error == nil && result != nil {
                taskSource.set(result: result as AnyObject)
            } else {
                taskSource.set(error: error!)
            }
        })
        return taskSource.task
    }
    private func pffb_preciseDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return formatter
    }
    
//    // For linked user without signup
//    func linkCurrentUserWithFacebook() {
//        let user = ParseUser().currentUser as! ParseUser
//        PFFacebookUtils.linkUser(inBackground: user, withReadPermissions: facebookPermissions() as? [String]) { (success, error) in
//            if error != nil {
//                self.facebookDelegate?.facebookLogin(withError: error!)
//            } else if success == true {
//                let token = FBSDKAccessToken.current()
//                LSUserDefaults.sharedInstance.facebookId = token?.userID
//                LSUserDefaults.sharedInstance.facebookAuthToken = token?.tokenString
//                LSUserDefaults.sharedInstance.isFacebookLinked = true
//                user.fbId = token?.userID
//                user.isFacebookLinked = true
//                user.saveInBackground(block: { (success, error) in
//                    if error != nil {
//                        self.facebookDelegate?.facebookLogin(withError: error!)
//                    } else if success == true {
//                        self.facebookDelegate?.facebookLogin(withSuccess: true)
//                    } else {
//                        self.facebookDelegate?.facebookLogin(withSuccess: false)
//                    }
//                })
//            } else {
//                self.facebookDelegate?.facebookLogin(withSuccess: false)
//            }
//        }
//    }
}
