//
//  PushNotificationExtension.swift
//  Reel
//
//  Created by Afnan on 22/02/2018.
//  Copyright © 2018 isystematic. All rights reserved.
//

import Foundation
import OneSignal

extension CaineAppDelegate    {
    
    func initialzePushNotification(withLaunchOption launchOptions: [UIApplicationLaunchOptionsKey: Any]?){
        
        OneSignal.setLogLevel(.LL_NONE, visualLevel: .LL_NONE)
        
        let onesignalInitSettings : NSMutableDictionary  =   NSMutableDictionary.init()
        onesignalInitSettings[kOSSettingsKeyAutoPrompt] =  false
        onesignalInitSettings[kOSSettingsKeyInAppAlerts] =  false
        //        onesignalInitSettings[kOSSettingsKeyInFocusDisplayOption] = OSNotificationDisplayType!.none//String(describing: OSNotificationDisplayType!.none)
        onesignalInitSettings[kOSSettingsKeyInFocusDisplayOption] =  String(describing: OSNotificationDisplayType!.none)
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
            if (notification?.wasAppInFocus)! {
                
                let payload = notification?.payload
                print("Payload where application was crashing \(payload?.additionalData!)")
                var messageTitle = "OneSignal Example"
                // let fullMessgae = payload?.body
                if ((payload?.attachments) != nil) {
                    print("Attachments")
                }
                if ((payload?.additionalData) != nil) {
                    if ((payload?.title) != nil) {
                        messageTitle = (payload?.title)!
                    }
                    print(payload?.additionalData[FNOTIFICATION_TYPE])
                    let notificationType = payload?.additionalData[FNOTIFICATION_TYPE] as! NSString
                 }
               
 
            }
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            guard let payload: OSNotificationPayload = result?.notification.payload else { return }
            print(payload)
            
            
            guard let additionalData = payload.additionalData else { return }
            
            print(additionalData)
            
            if payload.title != nil {
                let messageTitle = payload.title
                print("Message Title = \(messageTitle!)")
            }
            
//            let additionalData = payload.additionalData
            if ((payload.attachments) != nil) {
                print("Attachments")
            }
//            if additionalData["actionSelected"] != nil {
//                fullMessage = fullMessage! + "\nPressed ButtonID: \(String(describing: additionalData["actionSelected"]))"
//            }
            
            
            if let notificationType = payload.additionalData[FNOTIFICATION_TYPE] as? NSString {
                print("The Notification Type Is \(notificationType)")
            }
            
//            if payload.additionalData != nil {
//
//
//                             }
            
        }
        
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: ONE_SIGNAL_KEY,
                                        handleNotificationReceived: notificationReceivedBlock,
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings as! [AnyHashable : Any])
        
        
        
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
    }
    
    
    
    
    
    
    
    func resizeImage(withImage image: UIImage) -> UIImage {
        let destinationSize = CGSize(width: 50, height: 50)
        UIGraphicsBeginImageContext(destinationSize)
        image.draw(in: CGRect(x: 0, y: 0, width: destinationSize.width, height: destinationSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    // Open Reel detail controller
    
    
    //------------------ initiazation -----------
    //MARK: initialzeation
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("*** Access permisission granted")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("*** Access permisission Denied")
            }
        } else if stateChanges.to.status == OSNotificationPermission.denied { // DENIED = NOT SUBSCRIBED
            print("*** Access permisission Now Denied")
        }
    }
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        
    }
    
    /// Get  latest ids and check if user should updat eon server as well
    func getIds(_ shouldUpdateOnServer  : Bool){
        //getPermissionSubscriptionState
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        let hasPrompted = status.permissionStatus.hasPrompted
        print("hasPrompted = \(hasPrompted)")
        let userStatus = status.permissionStatus.status
        print("userStatus = \(userStatus)")
        let isSubscribed = status.subscriptionStatus.subscribed
        print("isSubscribed = \(isSubscribed)")
        let userSubscriptionSetting = status.subscriptionStatus.userSubscriptionSetting
        print("userSubscriptionSetting = \(userSubscriptionSetting)")
        let userID = status.subscriptionStatus.userId
        print("userID = \(userID ?? "None")")
        let pushToken = status.subscriptionStatus.pushToken
        print("pushToken = \(pushToken ?? "None")")
        
        if userID != nil  {
            if shouldUpdateOnServer == true {
                API.sharedInstance.updateOneSignal(id: userID!)
            }
            
            if   LSUserDefaults.sharedInstance.oneSignalId ==  nil ||
                LSUserDefaults.sharedInstance.oneSignalId  != userID{
                LSUserDefaults.sharedInstance.oneSignalId = userID!
                API.sharedInstance.updateOneSignal(id: userID!)
            }
            
        }
    }
    func askForPushNotification() {
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        let hasPrompted = status.permissionStatus.hasPrompted
        if hasPrompted == false {
            // Call when you want to prompt the user to accept push notifications.
            // Only call once and only if you set kOSSettingsKeyAutoPrompt in AppDelegate to false.
            OneSignal.promptForPushNotifications(userResponse: { accepted in
                if accepted == true {
                    print("User accepted notifications: \(accepted)")
                } else {
                    print("User accepted notifications: \(accepted)")
                }
            })
        }
    }
    
    
 
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Did Receive Remote Notification Called")
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
    }
 
    
    
    
    
}
