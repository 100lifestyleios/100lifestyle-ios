//
//  ArticleInstance.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 31/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation

class ArticleInstance {
    
    public  var articles : [Article] = []
    
    class var sharedInstance :ArticleInstance {
        struct Singleton {
            static let instance = ArticleInstance()
        }
        return Singleton.instance
    }
    
    
}
