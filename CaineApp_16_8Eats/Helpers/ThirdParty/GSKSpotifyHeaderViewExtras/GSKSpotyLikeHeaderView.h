#import <GSKStretchyHeaderView/GSKStretchyHeaderView.h>

@interface GSKSpotyLikeHeaderView : GSKStretchyHeaderView

@property (strong, nonatomic) UIImage * image;
@property (strong, nonatomic) NSString * titleText;
@end
