#import "GSKSpotyLikeHeaderView.h"
#import "UIView+GSKLayoutHelper.h"
#import <GSKStretchyHeaderView/GSKGeometry.h>
#import <Masonry/Masonry.h>

static const CGSize kUserImageSize = {.width = 64, .height = 64};

@interface GSKSpotyLikeHeaderView ()

@property (nonatomic) UIView *bgOverlayView;
@property (nonatomic) UIImageView *backgroundImageView;
@property (nonatomic) UIImageView *blurredBackgroundImageView;
@property (nonatomic) UIImageView *userImageView; // redondear y fondo blanco
@property (nonatomic) UIButton *followButton;
@property (nonatomic, assign) BOOL needsSetup;
@property (nonatomic) UILabel *title;

@end

@implementation GSKSpotyLikeHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.minimumContentHeight = 72;
        self.maximumContentHeight = [UIScreen mainScreen].bounds.size.height / 2; // image half size of screen
        self.backgroundColor = [UIColor lightGrayColor];
      self.needsSetup = YES;
      
    }
    return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  if (self.needsSetup) {
    [self setupViews];
    [self setupViewConstraints];
    self.needsSetup = NO;
  }
}
- (void)setupViews {
//    self.backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"landscape"]];
  self.backgroundImageView = [[UIImageView alloc] initWithImage:self.image];
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.backgroundImageView.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.backgroundImageView];
    
//    self.blurredBackgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"landscape_blur"]];
  self.blurredBackgroundImageView = [[UIImageView alloc] initWithImage:self.image];
    self.blurredBackgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.blurredBackgroundImageView.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:self.blurredBackgroundImageView];

    self.userImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"artist"]];
    self.userImageView.clipsToBounds = YES;
    self.userImageView.layer.cornerRadius = kUserImageSize.width / 2;
    self.userImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userImageView.layer.borderWidth = 4;
    [self.contentView addSubview:self.userImageView];
  self.userImageView.hidden = YES;
    
    self.bgOverlayView = [ [UIView alloc]  initWithFrame:self.contentView.frame];
    self.bgOverlayView.backgroundColor = [UIColor blackColor];
    self.bgOverlayView.alpha = 0.15f;
    [self.contentView addSubview:self.bgOverlayView];

    self.title = [[UILabel alloc] init];
  self.title.textAlignment = NSTextAlignmentCenter;
    self.title.text = self.titleText;
    self.title.textColor = [UIColor whiteColor];
  self.title.numberOfLines = 2;
    
    self.title.font = [UIFont fontWithName:@"Freshman" size:[UIScreen mainScreen].bounds.size.height / 26.285 ];
    self.title.minimumScaleFactor = 0.5;
    self.title.alpha = 1.0;
    [self.contentView addSubview:self.title];
    [UIView animateWithDuration:0.5 animations:^{
      self.title.alpha = 1.0;
    }];

    self.followButton = [[UIButton alloc] init];
  [self.followButton setImage:[UIImage imageNamed:@"down-arrow"] forState:UIControlStateNormal];
    self.followButton.backgroundColor = [UIColor clearColor];
    [ self.followButton setHidden:true];
//    self.followButton.titleLabel.font = [UIFont boldSystemFontOfSize:10];
//    self.followButton.layer.cornerRadius = 4;
//    self.followButton.layer.borderWidth = 1;
//    self.followButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [self.followButton addTarget:self
                          action:@selector(didTapFollowButton:)
                forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.followButton];
}

- (void)setupViewConstraints {
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.equalTo(@0);
        make.width.equalTo(self.contentView.mas_width);
        make.height.equalTo(self.contentView.mas_height);
    }];
    
    [self.bgOverlayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.equalTo(@0);
        make.width.equalTo(self.contentView.mas_width);
        make.height.equalTo(self.contentView.mas_height);
    }];

    [self.blurredBackgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.backgroundImageView);
    }];

    [self.userImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.centerY.equalTo(self.contentView.mas_centerY).offset(-10);
        make.width.equalTo(@(kUserImageSize.width));
        make.height.equalTo(@(kUserImageSize.height));
    }];

    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.top.equalTo(self.userImageView.mas_bottom).offset(10);
      make.left.equalTo(self.backgroundImageView.mas_left).offset(10);
      make.right.equalTo(self.backgroundImageView.mas_right).offset(-10);
    }];

    [self.followButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
    }];

}

- (void)didChangeStretchFactor:(CGFloat)stretchFactor {
    CGFloat alpha = 1;
    CGFloat blurAlpha = 1;
    NSLog(@"factor: %f",stretchFactor);
    if (stretchFactor >= 1) {
//        alpha = CGFloatTranslateRange(stretchFactor, 1, 1.12, 1, 0);
//        blurAlpha = alpha;
        alpha = 1;
        blurAlpha = alpha;
    } else //if (stretchFactor < 0.8)
    {
        alpha = CGFloatTranslateRange(stretchFactor, 0.2, 0.8, 0, 1);
        blurAlpha = 1-stretchFactor;
      if (stretchFactor==0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showNavigationTitle" object:self];
      } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideNavigationTitle" object:self];
      }
    }

    alpha = MAX(0, alpha);
    self.blurredBackgroundImageView.alpha = blurAlpha;
    self.userImageView.alpha = alpha;
    self.title.alpha = alpha;
    self.followButton.alpha = alpha;
}

- (void)didTapFollowButton:(id)sender {
//    [self.followButton setTitle:@"  FOLLOWING  " forState:UIControlStateNormal];
}

@end
