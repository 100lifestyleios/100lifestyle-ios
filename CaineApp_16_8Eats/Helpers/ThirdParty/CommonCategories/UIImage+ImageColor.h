//
//  UIImage+ImageColor.h
//  Recall
//
//  Created by clines192 on 2/5/16.
//  Copyright © 2016 MacbookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageColor)

- (UIImage *)imageWithColor:(UIColor *)color1;
+ (UIImage *)imageFromColor:(UIColor *)color;

- (UIImage *)toGrayscale;

@end
