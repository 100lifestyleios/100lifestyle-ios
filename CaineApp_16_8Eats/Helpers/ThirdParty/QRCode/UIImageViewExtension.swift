//
//  UIImageViewExtension.swift
//  QRCode
//
//  Created by Alexander Schuch on 27/01/15.
//  Copyright (c) 2015 Alexander Schuch. All rights reserved.
//

import UIKit
import UIKit
import Cloudinary
import Parse
import SDWebImage

public extension UIImageView {
    
    /// Creates a new image view with the given QRCode
    ///
    /// - parameter qrCode:      The QRCode to display in the image view
    public convenience init(qrCode: QRCode) {
        self.init(image: qrCode.image)
    }
    
}


extension UIImageView {
    
    
    private struct AssociatedImagesKeys {
        static var kTemporaryImageKey = "kTemporaryImageKey"
    }
    
    var temporaryImageURL: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedImagesKeys.kTemporaryImageKey) as? String
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedImagesKeys.kTemporaryImageKey,
                    newValue as NSString?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
    
    // upload imahge
 
    
    func setGifImageView(url urlString : String?  , completion: @escaping (_  :  UIImage? , _ : String) -> Void ){
        if (urlString == nil || (urlString?.isStringEmpty)!) {return }
        self.temporaryImageURL = urlString
        
        let cleanImageURL = urlString?.cleanImageUrlForSDWebImage()
        
        let imageURL =  URL(string : cleanImageURL!)
        
        if ImageHelper.sharedInstance.ifExistInDocument(name: (imageURL?.lastPathComponent)!) == true  {
            let tempImage = try? UIImage(gifData :  ImageHelper.sharedInstance.getFromDocuments(name:  (imageURL?.lastPathComponent)!)!)
            self.setGifImage(tempImage!)
            //self.image = image
            completion(tempImage! , (self.temporaryImageURL!))
            return
            
        } else {
            
             DispatchQueue.global(qos: .background).async { [weak self] () -> Void in
                var data : Data?  = nil
                data = try?  Data(contentsOf: imageURL!)
                if (data == nil)  {return}
                ImageHelper.sharedInstance.saveInDocuments(data: data!, name: (imageURL?.lastPathComponent)!)
                DispatchQueue.main.async { [weak self] in
                    let newImage = UIImage(gifData :data!)
                    self?.setGifImage(newImage)
                    //self.image = image
                    completion(newImage , (self?.temporaryImageURL!)!)
                }
                
            }
        }
        
    }
 
 
}
