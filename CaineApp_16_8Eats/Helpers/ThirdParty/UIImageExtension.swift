//
//  UIImageExtension.swift
//  Reel
//
//  Created by Mursaleen Moosa on 20/11/2017.
//  Copyright © 2017 isystematic. All rights reserved.
//

import UIKit

extension UIImage {
    
    public func makeImageSquare(_ image:UIImage, _ size:CGFloat)  -> UIImage {
        
        let squareImage:UIImage?
        if image.size.height > image.size.width {
            let ypos = (image.size.height - image.size.width) / 2
            squareImage = cropImage(image, 0, ypos, image.size.width, image.size.width)
        } else {
            let xpos = (image.size.width - image.size.height) / 2
            squareImage = cropImage(image, xpos, 0, image.size.height, image.size.height)
        }
//        return resizeImage(squareImage!, size, size, 1)
        return squareImage!
//        return whatsappCompressedImage(usingWidth: size, usingHeight: size)
    }
    public func resizeImage(_ image:UIImage, _ width:CGFloat, _ height:CGFloat, _ scale:CGFloat) -> UIImage {
        let size = CGSize(width: width, height: height)
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        image.draw(in: rect)
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
    private func cropImage(_ image:UIImage, _ x:CGFloat, _ y:CGFloat, _ width:CGFloat, _ height:CGFloat) -> UIImage {
        
        let rect = CGRect(x: x, y: y, width: width, height: height)
//        let imageRef = image.cgImage?.cropping(to: rect)
//        let croppedImage = UIImage(cgImage: imageRef!)
//        return croppedImage;
        UIGraphicsBeginImageContextWithOptions(rect.size, false, image.scale)
        let origin = CGPoint(x: rect.origin.x * CGFloat(-1), y: rect.origin.y * CGFloat(-1))
        image.draw(at: origin)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return result!
    }
 
    func whatsappCompressedImage(usingWidth width: CGFloat, usingHeight height: CGFloat) -> UIImage {
        var actualHeight = self.size.height
        var actualWidth = self.size.width
        var imageRatio = actualWidth/actualHeight
        let maxRatio = width/height
        if actualHeight > height || actualWidth > width {
            if imageRatio < maxRatio  {
                // Adjust width according to height
                imageRatio = height / actualHeight
                actualWidth = imageRatio * actualWidth
                actualHeight = height
            } else if imageRatio > maxRatio {
                // Adjust height according to width
                imageRatio = width / actualWidth;
                actualHeight = imageRatio * actualHeight
                actualWidth = width
            } else {
                actualHeight = height
                actualWidth = width
            }
        }
        
        let rect = CGRect(x: 0, y: 0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 1.0)
        self.draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

}


