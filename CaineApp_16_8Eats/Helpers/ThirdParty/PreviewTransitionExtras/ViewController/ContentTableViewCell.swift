//
//  ContentTableViewCell.swift
//  CaineApp_16_8Eats
//
//  Created by clines192 on 20/06/2017.
//  Copyright © 2017 iSystematic LLC. All rights reserved.
//

import UIKit
import WebKit
import JTMaterialSpinner
class ContentTableViewCell: UITableViewCell, WKNavigationDelegate, WKUIDelegate {
    
    @IBOutlet weak var progressSpinner : JTMaterialSpinner!
    
    public var webView: WKWebView?
    
    var tableViewHeight : CGFloat = 100
    
    override func awakeFromNib() {
        
        self.creatAndAttachWebView()
        
        super.awakeFromNib()
        
//        contentView.alpha = 0.0
        
        // Customize the line width
        progressSpinner.circleLayer.lineWidth = 2.0
        
        // Change the color of the line
        progressSpinner.circleLayer.strokeColor = UIColor(red: 58/255.0, green: 182/255.0, blue: 227/255.0, alpha: 1.0).cgColor
        
        // Change the duration of the animation
        progressSpinner.animationDuration = 2.5
        progressSpinner.isHidden = false
        progressSpinner.beginRefreshing()
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func creatAndAttachWebView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: self.frame, configuration: webConfiguration)
        webView?.uiDelegate = self
        webView?.navigationDelegate = self
        self.addSubview (webView!)
    }

}
