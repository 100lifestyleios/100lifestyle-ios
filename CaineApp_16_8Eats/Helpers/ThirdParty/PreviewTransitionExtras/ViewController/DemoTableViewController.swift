//
//  DemoTableViewController.swift
//
// Copyright (c)  21/12/15. Ramotion Inc. (http://ramotion.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
//import PreviewTransition

public class DemoTableViewController: PTTableViewController {
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.bounces = false
    }
  
  fileprivate let items = [("1. Caine's Story (Cover)", "Caine’s Story"),
                           ("2. 16:8 Intermittent Fasting (Cover)", "16:8 Intermittent Fasting - What is it?"),
                           ("3. How it Works (Cover)", "How it Works"),
                           ("4. Finding your Ideal Bodyweight (Cover)", "Finding Your Ideal Body Weight"),
                           ("5. Fasted training. The benfits. (Cover)", "The Benefits of Fasted Training"),
                           ("6. We Love Coffee! (Cover)", "We Love Coffee"),
                           ("7. Eating Plan (Cover)", "Eating Plan"),
                           ("8. Clean Eating Grocery List (Cover)", "Clean Eating Grocery List"),
                           ("9. Your on the right Track! (Cover)", "You are on right track"),
                           ("10. Alcohol Awereness (Cover)", "Alcohol Awereness"),
                           ("11. Eat Me! (Cover)", "Eat Me"),
                           ("12. Eating Plan 2.0 (Cover)", "Eating Plan 2.0"),
                           ("13. Step out of your Comfort Zone (Cover)", "Step out of your Comfort Zone"),
                           ("14. This will change your Life! (Cover)", "This will change your Life!"),
                           ("15. Our Journey (Cover)", "Our Journey"),
                           ("16. Fueling your body with Fats (Cover 1)", "Fueling your body with Fats"),
                           ("17. Keytones (Cover)", "Keytones"),
                           ("18. Healthy Fats (Cover)", "Healthy Fats"),
                           ("19. Eating Plan 3.0 (Cover)", "Eating Plan 3.0"),
                           ("20. Caine's Secrets to Cardio (Cover)", "Caine's Secrets to Cardio"),
                           ("21. Lunchtime Workouts (Cover)", "Lunchtime Workouts"),
                           ("22. Dan's Secret Lifting (Cover)", "Dan's Secret to Heavy Lifting")
    
                           ] // image names
}

// MARK: UITableViewDelegate
extension DemoTableViewController {
  
  public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  public override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard let cell = cell as? ParallaxCell else { return }
    
    let index = indexPath.row % items.count
    let imageName = items[index].0
    let title = items[index].1
    
    if var image = UIImage(named: imageName) {
      if index%3==0 {
        image = image.toGrayscale()
        cell.imageViewLock.image = UIImage(named: "lock")
      } else {
        cell.imageViewLock.image = UIImage(named: "unlock")
      }
      cell.setImage(image, title: title)
    }
  }
  
  public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: ParallaxCell = tableView.getReusableCellWithIdentifier(indexPath: indexPath)
    
    cell.lblTimeRemaining.isHidden = false
    cell.lblCaineName.isHidden = false
    cell.imageViewLock.isHidden = false
    cell.imageViewCaine.isHidden = false
    
    cell.imageViewCaine.hero.id = "image"
//    cell.imageViewBG.isHidden = false
    
    return cell
  }
  
  public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let storyboard = UIStoryboard.storyboard(storyboard: .Main)
    
    let detailViewController: DemoDetailViewController = storyboard.instantiateViewController()
    pushViewController(detailViewController)
    
  }

}
