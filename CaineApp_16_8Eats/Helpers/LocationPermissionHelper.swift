//
//  LocationPermissionHelper.swift
//  Quixly
//
//  Created by clines192 on 03/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import CoreLocation

// MARK: - LocationPermissionHelper
protocol LocationPermissionHelper {
    static var locationManager: CLLocationManager { get }
    func requestLocationPermissions(delegate: CLLocationManagerDelegate)
    
    func startMonitoringSignificantLocationChanges()
    func stopMonitoringSignificantLocationChanges()
    
    func startUpdatingLocation()
    func stopUpdatingLocation()
}
extension LocationPermissionHelper {
    func requestLocationPermissions(delegate: CLLocationManagerDelegate) {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            Self.locationManager.requestAlwaysAuthorization()
        }
        Self.locationManager.delegate = delegate
        Self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        Self.locationManager.distanceFilter = 100
    }
    func startMonitoringSignificantLocationChanges() {
        Self.locationManager.startMonitoringSignificantLocationChanges()
    }
    func stopMonitoringSignificantLocationChanges() {
        Self.locationManager.stopMonitoringSignificantLocationChanges()
    }
    func startUpdatingLocation() {
        Self.locationManager.startUpdatingLocation()
    }
    func stopUpdatingLocation() {
        Self.locationManager.stopUpdatingLocation()
    }
}
