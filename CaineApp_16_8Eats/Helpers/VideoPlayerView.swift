//
//  VideoPlayerView.swift
//  CaineApp_16_8Eats
//
//  Created by Salman Khan on 28/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerView: UIView {
    
    
    private var player: AVPlayer?
    private var playerLayer: AVPlayerLayer?
    var observer : Any?
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public func configureVideoPlayer(withUrl url : URL) {
        print("Video URL \(url.absoluteString)")
        var videoURL  : URL?
        if ImageHelper.sharedInstance.getVideoURL(name: url) ==  nil {
            videoURL = url
            print("configure Player IF")
            ImageHelper.sharedInstance.downloadNextGifImage(ofURL: url.absoluteString)
        } else {
            print("configure Player Else")
            videoURL = ImageHelper.sharedInstance.getVideoURL(name: url)
        }
        let playerItem = AVPlayerItem(url: videoURL!)
        if playerLayer != nil {
            playerLayer?.removeFromSuperlayer()
            playerLayer = nil
        }
        player = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = UIScreen.main.bounds//self.bounds
        playerLayer?.backgroundColor = UIColor.black.cgColor
        playerLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        layer.insertSublayer(playerLayer!, at: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(self.playVideoAgain), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
    }
    public func playVideo() {
        player?.play()
        print("Play Video Called")
//        observer = player?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 600), queue: DispatchQueue.main) {
//            [weak self] time in
//            if self?.player?.currentItem?.status == AVPlayerItemStatus.readyToPlay {
//                if let isPlaybackLikelyToKeepUp = self?.player?.currentItem?.isPlaybackLikelyToKeepUp {
//                    print(isPlaybackLikelyToKeepUp)
//                    try? self?.player?.removeTimeObserver(self?.observer as Any)
//                }
//            }
//        }
    }
    public func stopVideo() {
        print("Stop Video Called")
        player?.seek(to: kCMTimeZero)
        player?.cancelPendingPrerolls()
        player?.pause()
    }
    @objc func playVideoAgain() {
        print("Play Video Again Called")
        self.player?.seek(to: kCMTimeZero)
        self.player?.play()
    }
    public func removePlayer() {
        player = nil
        playerLayer?.removeFromSuperlayer()
        playerLayer = nil
        NotificationCenter.default.removeObserver(self)
        removeFromSuperview()
        print("Remove Player Called")
    }
    
}
