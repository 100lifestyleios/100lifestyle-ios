//
//  QXUserDefaults.swift
//  Quixly
//
//  Created by isystematic on 8/3/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation

class LSUserDefaults {
    
    class var sharedInstance : LSUserDefaults {
        struct Singleton {
            static let instance = LSUserDefaults()
        }
        return Singleton.instance
    }
    
    var objectId : String? {
        get {
            return UserDefaults.standard[#function]
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var username: String? {
        get {
            return UserDefaults.standard[#function]
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }

    var password: String? {
        get {
            return UserDefaults.standard[#function]
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var fullName: String? {
        get {
            return UserDefaults.standard[#function]
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var workoutReminderTime: Date? {
        get {
            return UserDefaults.standard[#function]
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var lastWorkOutDate: Date? {
        get {
            return UserDefaults.standard[#function] ?? Date().getBasicDate()
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    
    var purchaseStatus: NSNumber? {
        get {
            return UserDefaults.standard[#function]
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var facebookId : String? {
        get {
            return UserDefaults.standard[#function] ?? ""
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var isFacebookLogin : Bool? {
        get {
            return UserDefaults.standard[#function]
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var facebookAuthToken : String? {
        get {
            return UserDefaults.standard[#function] ?? ""
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    var workOutDay : Int? {
        get {
            return UserDefaults.standard[#function] ?? 1
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var oneSignalId: String? {
        get {
            return UserDefaults.standard[#function] ?? ""
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    func removeAllData() {
        objectId = nil
        username = nil
        password = nil
        fullName = nil
        workoutReminderTime = nil
        purchaseStatus = nil
        isFacebookLogin = nil
        facebookId = nil
        facebookAuthToken = nil
        workOutDay = nil
        oneSignalId = nil
        lastWorkOutDate = nil

    }
}




class WorkOutDefaults {
    
    class var sharedInstance : WorkOutDefaults {
        struct Singleton {
            static let instance = WorkOutDefaults()
        }
        return Singleton.instance
    }
    var workOutObjectId : String? {
        get {
            return UserDefaults.standard[#function]
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    
    var workoutName : String {
        get {
            return UserDefaults.standard[#function]!
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    var workoutImageUrl : String {
        get {
            return UserDefaults.standard[#function]!
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    var workoutVideoUrls : [String]? {
        get {
            return UserDefaults.standard[#function]!
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    var workoutVideoTitles : [String]? {
        get {
            return UserDefaults.standard[#function]!
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    var workoutDuration : NSNumber? {
        get {
            return UserDefaults.standard[#function] ?? 0
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var lastWorkoutPublishedOn : Date? {
        get {
            return UserDefaults.standard[#function] ?? Date(year: 2018, month: 12, day: 25, hour: 0, minute: 0, second: 0)
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
}

class FEUserDefaults {
    
    /***
     Facebook Events Log User Defaults
     */
    class var sharedInstance : FEUserDefaults {
        struct Singleton {
            static let instance = FEUserDefaults()
        }
        return Singleton.instance
    }
    
    var screenName : String? {
        get {
            return UserDefaults.standard[#function] ?? "Splash Screen"
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var startTime: Date? {
        get {
            return UserDefaults.standard[#function] ?? Date()
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    var endTime: Date? {
        get {
            return UserDefaults.standard[#function] ?? Date()
        }
        set {
            UserDefaults.standard[#function] = newValue
        }
    }
    
    func removeAllData() {
        screenName = nil
        startTime = nil
        endTime = nil
    }
}

