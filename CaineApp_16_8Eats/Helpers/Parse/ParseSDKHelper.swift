//
//  ParseSDKHelper.swift
//  Quixly
//
//  Created by clines192 on 04/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import Parse

//For Debuging, Use this
//let PARSE_APP_ID =                  "hZON33n2npzCjrMB6Z6gpgkWL8wDADa7CUnhVv62"
//let PARSE_CLIENT_KEY =              "Z1Sd0VdTbm6TcFzep1wHa7P8WyrZj39LHVbHjDO5"
//let PARSE_ADDRESS_URL =             "https://parseapi.back4app.com/"

//For Live, Use this
let PARSE_APP_ID =                  "lTNJspPaFRTD80aryqDAOY8S0xe4Ttt4ztPAcMSp"
let PARSE_CLIENT_KEY =              "C4dPuRaAx3tf7xcuFbI7iQwtb78MMRWYrgAu6Shg"
let PARSE_ADDRESS_URL =             "https://parseapi.back4app.com/"

// MARK: - RadarSDKHelper
protocol ParseSDKHelper {
    func setupParseSDK()
}
extension ParseSDKHelper {
    /// Call this method before intializing Parse SDK
    private func registerPFObjectSubclasses() {
        ParseUser.registerSubclass()
        WorkOutPFModel.registerSubclass()
//        ParseStore.registerSubclass()
//        ParseDeal.registerSubclass()
//        ParseUserDeal.registerSubclass()
    }
    
    /// Setup Parse SDK: Call this method in applicationDidFinishLaunching: method
    func setupParseSDK() {
        registerPFObjectSubclasses()
        let configuration = ParseClientConfiguration {
            $0.applicationId = PARSE_APP_ID
            $0.clientKey = PARSE_CLIENT_KEY
            $0.server = PARSE_ADDRESS_URL
            $0.networkRetryAttempts = 2
        }
        Parse.initialize(with: configuration)
    }
}
