//
//  AppDelegate+Branch.swift
//  ChallengeApp
//
//  Created by Mursaleen Moosa on 19/06/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//


import Foundation
import Bolts
import Branch

extension AppDelegate {
    public func initBranchWithLaunchOptions(_ launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            
            if error == nil {
                guard let data = params as? [String: AnyObject] else { return }
                guard let options = data["nav_to"] as? String else { return }
                switch options {
                case "challenge_share":
                    if let rootViewController = self.window?.rootViewController,
                        let challengeObjectId = data["$canonical_identifier"] as? String
                        /*let userFullName = data["name"] as? String */{
                            
                            /*if rootViewController is KNSplashController {
                                print("Appdelegate If called")
                                DispatchQueue.main.asyncAfter(deadline: .now()+3.0, execute: {
                                    let root = self.window?.rootViewController
                                    let controller = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "KNChallengeDetailController") as! KNChallengeDetailController
                                    controller.isFromPushNotification = true
                                    controller.sharedChallengeObjectId = challengeObjectId
                                    root?.present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
                                })
                            } else {
                                print("Appdelegate Else called")
                                DispatchQueue.main.asyncAfter(deadline: .now()+3.0, execute: {
                                    
                                    let controller = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "KNChallengeDetailController") as! KNChallengeDetailController
                                    
                                    controller.isFromPushNotification = true
                                    controller.sharedChallengeObjectId = challengeObjectId
                                    rootViewController.present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
                                })
                            }*/
                    } else {
                        print("Show Toast Called")
                        showToast(withMessage : "Sorry! Link has expired!")
                    }
                default:
                    print("BranchIO Default Called")
                    break
                }
            } else {
                print("BranchIO Error \(error?.localizedDescription ?? "No Error")")
            }
        })
    }
    // Respond to URI scheme links
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // pass the url to the handle deep link call
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        if (!branchHandled) {
            // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        }
        
        // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        return true
    }
    
    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        Branch.getInstance().continue(userActivity)
        return true
    }
}
