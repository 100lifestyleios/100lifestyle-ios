//
//  main.m
//  CaineApp_16_8Eats
//
//  Created by clines192 on 16/06/2017.
//  Copyright © 2017 iSystematic LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "CaineApp_16_8Eats-Swift.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CaineAppDelegate class]));
    }
}
