//
//  CaineAppDelegate.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 07/05/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import UserNotifications
import Firebase
import Crashlytics
import Parse
import Mixpanel
import GoogleMaps
import GooglePlaces

@objc class CaineAppDelegate : AppDelegate, ParseSDKHelper {
    
    static let TAG = "CaineAppDelegate"
    
    var appVisibleController : UIViewController?
    var remoteConfig: RemoteConfig!
    
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        super.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        FirebaseApp.configure()
//        setupParseSDK()
        //request authorization for user notifications
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        let options: UNAuthorizationOptions = [.alert, .sound]
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("Something went wrong")
                let userDefaults = UserDefaults.standard
                userDefaults.set(false, forKey: "notificationsGranted")
            }
            else {
                let userDefaults = UserDefaults.standard
                userDefaults.set(true, forKey: "notificationsGranted")
            }
        }
        
        Mixpanel.initialize(token: MIX_PANEL_TOKEN)
        // Setup Branch.io Deeplinking
        self.initBranchWithLaunchOptions(launchOptions)
        
        GMSServices.provideAPIKey(GOOGLE_MAPS_API_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_MAPS_API_KEY)

        setupParseSDK()
        PFFacebookUtils.initializeFacebook(applicationLaunchOptions: launchOptions)
        FBSDKAppEvents.activateApp()
        UINavigationBar.appearance().titleTextAttributes = [ NSForegroundColorAttributeName:UIColor.black , NSFontAttributeName : UIFont(name: CLANOT_BOLD, size: getSize(byScreen: 18))!]
        
        self.initialzePushNotification(withLaunchOption: launchOptions)
        askForPushNotification()
        return true
    }
    
    override func applicationDidBecomeActive(_ application: UIApplication) {
        super.applicationDidBecomeActive(application)
        
        DispatchQueue.main.async {[weak self] in
            self?.getIds(true)
         }
        
        
        //setup remote config
        setupRemoteConfig()
        API.sharedInstance.requestLocationPermissions(delegate: API.sharedInstance)
        ForceUpdateChecker(listener: self).check()
    }
    
    override func applicationWillTerminate(_ application: UIApplication) {
        super.applicationWillTerminate(application)
        API.sharedInstance.logApplicationTerminateEvent()
    }
    
    func scheduleNotification(date : Date, subTitle : String, body : String, imageName : String){
        UserNotificationUtils.scheduleNotification(date: date, title: "100 LifeStyle", subTitle: subTitle , body: body, imageName: imageName, delegate: self)
    }
    
    override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    @available(iOS 9.0, *)
    override func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: nil)
    }
}

extension CaineAppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("userNotificationCenter : didReceive")
        
        // Determine the user action
        switch response.actionIdentifier {
        case UNNotificationDefaultActionIdentifier:
            print("Open Notification")
            
            let userInfo = response.notification.request.content.userInfo
            
            if(appVisibleController is DemoDetailViewController){
                
                var notificationInfo : [ String:String ] = [:]
                notificationInfo["title"] = response.notification.request.content.subtitle
                notificationInfo["imageName"] = userInfo["imageName"] as? String
                
                NotificationCenter.default.post(name: Notification.Name(Globals.notificationArticleOpenedDetail), object: nil, userInfo: notificationInfo)
                
            }
            else if(appVisibleController is ArticlesListViewController){
                
                var notificationInfo : [ String:String ] = [:]
                notificationInfo["title"] = response.notification.request.content.subtitle
                notificationInfo["imageName"] = userInfo["imageName"] as? String
                
                NotificationCenter.default.post(name: Notification.Name(Globals.notificationArticleOpenedList), object: nil, userInfo: notificationInfo)
                
            } else {
                if visibleViewController is TabBarController {
                    let conroler = visibleViewController as! TabBarController
                    conroler.initialSelectedIndex = 0
                    conroler.selectedIndex = 0
                }
                
//                let controller =
//
//
//                openNotificationSplash(title: response.notification.request.content.subtitle, imageName: userInfo["imageName"] as! String)
            }
            
        default:
            print("Unknown action")
        }
        
        completionHandler()
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if(appVisibleController is ArticlesListViewController){
            NotificationCenter.default.post(name: Notification.Name(Globals.notificationArticleUnlockedList), object: nil)
        }
        else if(appVisibleController is DemoDetailViewController){
            NotificationCenter.default.post(name: Notification.Name(Globals.notificationArticleUnlockedDetail), object: nil)
        }
        
        completionHandler([.alert, .sound])
    }
    
}

//MARK :- Notification Helper Functions
extension CaineAppDelegate {
    
    func openNotificationSplash(title : String, imageName : String){
        
        print("CaineAppDelegate : openNotificationSplash()")
        
        let controller = UIViewController.SplashMainViewController() as! SplashMainViewController
        controller.isNotificationClicked = true
        controller.notificationArticleTitle = title
        controller.notificationArticleImageName = imageName
        
        self.visibleViewController?.present(controller, animated: true)
        
    }
    
    
    func openNotificationListController(title : String, imageName : String){
        
        let controller = UIStoryboard(name: "HeroApproach", bundle: nil).instantiateViewController(withIdentifier: "ArticlesListViewController") as! ArticlesListViewController
        
        controller.isNotificationClicked = true
        controller.notificationTitleText = title
        controller.notificationImageName = imageName
        
        let navigationController = UINavigationController(rootViewController: controller)
        
        let image = UIImage(named: "menu")
        let leftButton = UIBarButtonItem(image: image  , style: .plain, target: self , action: nil)
        navigationController.navigationItem.leftBarButtonItem = leftButton
        
        let rightImage = UIImage(named: "menu")
        let rightButton = UIBarButtonItem(image: rightImage  , style: .plain, target: self , action: nil)
        navigationController.navigationItem.rightBarButtonItem = rightButton
        
        self.visibleViewController?.present(navigationController, animated: true)
        
    }
}

//MARK:- Visible View Controller
extension CaineAppDelegate {
    var visibleViewController: UIViewController? {
        return getVisibleViewController(nil)
    }
    
    var topViewController : UIViewController? {
        return getTopViewController(UIApplication.shared.keyWindow?.rootViewController)
    }
    
    func getTopViewController(_ rootViewController : UIViewController?) -> UIViewController? {
        
        //        var rootVC = rootViewController
        //        if rootVC == nil {
        //            rootVC = UIApplication.shared.keyWindow?.rootViewController
        //        }
        
        if(rootViewController is UINavigationController){
            let navigationController = rootViewController as! UINavigationController
            return getTopViewController(navigationController.viewControllers.last)
        }
        else if (rootViewController is UITabBarController){
            let tabController = rootViewController as! UITabBarController
            return getTopViewController(tabController.selectedViewController)
        }
        else if(rootViewController?.presentedViewController != nil){
            return getTopViewController(rootViewController?.presentedViewController)
        }
        else {
            return rootViewController
        }
    }
    
    func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.keyWindow?.rootViewController
        }
        
        if rootVC?.presentedViewController == nil {
            return rootVC
        }
        
        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            
            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            
            return getVisibleViewController(presented)
        }
        return nil
    }
}


//Mark:- RemoteConfig Setup
extension CaineAppDelegate {
    func setupRemoteConfig(){
        
        remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.configSettings = RemoteConfigSettings(developerModeEnabled: false)!
        
        //set in app defaults
        let defaults : [String : Any] = getDefaultsForRemoteConfig()
        
        remoteConfig.setDefaults(defaults as? [String : NSObject])
        
        var expirationDuration = 60
        // If your app is using developer mode, expirationDuration is set to 0, so each fetch will
        // retrieve values from the service.
        if remoteConfig.configSettings.isDeveloperModeEnabled {
            expirationDuration = 0
        }
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { [weak self] (status, error) in
            if status == .success {
//                print("\(CaineAppDelegate.TAG) : config fetch done")
                self?.remoteConfig.activateFetched()
            } else {
//                print("Config not fetched")
//                print("Error: \(error?.localizedDescription ?? "No error available.")")
            }
        }
    }
}

extension CaineAppDelegate : OnUpdateNeededListener {
    func onUpdateNeeded(updateUrl: String) {
        
        let controller = UIStoryboard(name: "HeroApproach", bundle: nil).instantiateViewController(withIdentifier: "UpdateAppViewController") as! UpdateAppViewController
        controller.url = updateUrl
        
        if(self.appVisibleController is SplashMainViewController){
            self.appVisibleController?.present(controller, animated: false, completion: nil)
        } else if ( !( self.appVisibleController is UpdateAppViewController ) ){
            self.appVisibleController?.present(controller, animated: false, completion: nil)
        }
        
    }
    
    func onNoUpdateNeeded() {
        print("\(CaineAppDelegate.TAG) : onNoUpdateNeeded()")
        if(self.appVisibleController is UpdateAppViewController){
            self.appVisibleController?.dismiss(animated: false, completion: nil)
        }
    }
}

//set defaults for articles remote configs
extension CaineAppDelegate {
    func getDefaultsForRemoteConfig() -> [String : Any] {
        let defaults : [String : Any] = [
            ForceUpdateChecker.FORCE_UPDATE_REQUIRED : false,
            ForceUpdateChecker.FORCE_UPDATE_CURRENT_VERSION : "1.1(11)",
            ForceUpdateChecker.FORCE_UPDATE_STORE_URL : "https://itunes.apple.com/us/app/100-lifestyle/id1365005115?ls=1&mt=8",
            ]
        
        return defaults
    }
}

