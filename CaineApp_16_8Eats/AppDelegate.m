//
//  AppDelegate.m
//  CaineApp_16_8Eats
//
//  Created by clines192 on 16/06/2017.
//  Copyright © 2017 iSystematic LLC. All rights reserved.
//

#import "AppDelegate.h"
//#import "CaineApp_16_8Eats-Swift.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self configureNavigationBar];
  
//  for (NSString* family in [UIFont familyNames])
//  {
//    NSLog(@"----------------\nFamily: %@", family);
//    
//    for (NSString* name in [UIFont fontNamesForFamilyName: family])
//    {
//      NSLog(@"  %@", name);
//    }
//    NSLog(@"----------------\n");
//  }
    
    //[self compressImages];

    // For one push
    
    
    return YES;
}

//- (void)compressImages
//{
//    //[NSBundle mainBundle]
//}

- (void)configureNavigationBar {
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [UINavigationBar appearance].shadowImage = [UIImage new];
    [UINavigationBar appearance].translucent = YES;
    
    
    [UINavigationBar appearance].titleTextAttributes = @{
                                                         NSForegroundColorAttributeName : [UIColor whiteColor],
                                                         NSFontAttributeName : [UIFont fontWithName:@"Freshman" size:[UIScreen mainScreen].bounds.size.width / 27.6 ] //16
                                                         };
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
