//
//  TabBarControllerExtensions.swift
//  Quixly
//
//  Created by clines192 on 07/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

extension UIStoryboard {
    fileprivate class func tabBarStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "TabBarController", bundle: nil)
    }
}

extension UIViewController {
    static var sharedTabBarController:TabBarController? = nil;
    
    internal class func tabbarController() -> TabBarController? {
        if sharedTabBarController == nil {
            sharedTabBarController = UIStoryboard.tabBarStoryboard().instantiateViewController(withIdentifier: "TabBarController") as? TabBarController
        }
        return sharedTabBarController
    }
    
    internal class func tabBarChildViewController() -> TabBarChildViewController {
        return UIStoryboard.tabBarStoryboard().instantiateViewController(withIdentifier: "TabBarChildViewController") as! TabBarChildViewController
    }
    
    internal class func clearTabbarController() {
        sharedTabBarController = nil
    }
    
    
    //This function is for resetting tab bar for login/logout
    internal class func resetTabBarController(selectedIndex index : Int) {
        clearTabbarController()
        let controller = UIViewController.tabbarController()
        controller?.initialSelectedIndex = 0
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
}
