//
//  TabBarController.swift
//  Quixly
//
//  Created by clines192 on 07/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
//import Parse

class TabBarController: UITabBarController , UITabBarControllerDelegate {

    
    /// Initial selected tab index, default is 0
    var initialSelectedIndex:Int = 0
    var isNotificationClicked : Bool =  false
    var notificationTitleText : String?
    var notificationImageName : String?
    
    
    /// Tabbar Child View Controllers
    lazy var controllers:[UIViewController] = { [unowned self] in
        
        let currentUser = ParseUser.current()
        
        var DailyWorkoutController : UIViewController!
        var BootCampController : UIViewController!
        var NearestBootcampController : UIViewController!
                
        //Daily Work Out Tab 1
        DailyWorkoutController = UIViewController.DailyWorkOutViewController()
        
        //Bootcamp Tab 2
        BootCampController = UIViewController.MainBootCampViewController()
        
        //Map View for Nearest BootCamps Tab 4
        NearestBootcampController = UIViewController.NearestBootCampViewController()
        let NearestBootNavcampController = UINavigationController(rootViewController: NearestBootcampController)
        //        }
        
        //Articles List Tab 3
        let articleListController = UIStoryboard(name: "HeroApproach", bundle: nil).instantiateViewController(withIdentifier: "ArticlesListViewController") as! ArticlesListViewController
        if self.isNotificationClicked == true {
            articleListController.isNotificationClicked = self.isNotificationClicked
            articleListController.notificationTitleText = self.notificationTitleText
            articleListController.notificationImageName = self.notificationImageName
        }

        let articleListNavController = UINavigationController(rootViewController: articleListController)
        
        //Profile Controller Tab 5
        let ProfileController = UIViewController.ProfileViewController()
        
        return [
            DailyWorkoutController,
            BootCampController,
            articleListNavController,
            NearestBootNavcampController,
            ProfileController
        ]
    }()
}

// MARK: View Life Cycle
extension TabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.viewControllers = self.controllers
        
        setupTabBarItems()
        self.selectedIndex = initialSelectedIndex;
        updateTabBarUIForIndex(initialSelectedIndex)
        delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("notificationisClicked : ")
    }

}

// MARK: Private methods
extension TabBarController {
    func updateTabBarUIForIndex(_ index:Int) {
        let tabBar:UITabBar = self.tabBar
        tabBar.shadowImage = UIImage();
        tabBar.isTranslucent = false;
    }
    
    func setupTabBarItems() {
        let tabBar:UITabBar = self.tabBar
        tabBar.shadowImage = UIImage();
        tabBar.isTranslucent = false;
        let images = [
            "todayTab",
            "ticketTab",
            "exploreTab",
            "mapTab",
            "profileTab"
        ]
        let selectedImages = [
            "todayTabActive",
            "ticketTabActive",
            "exploreTabActive",
            "mapTabActive",
            "profileTabActive"
        ]
        
        let tabbarItems = tabBar.items as! [UITabBarItem]
        for index in (0..<tabbarItems.count) {
            let item = tabBar.items![index]
            item.image = UIImage(named: images[index])
            item.selectedImage = UIImage(named: selectedImages[index])
            item.tag = index
            item.title = ""
            item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)    
        }
    }
    
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let tabbarItem = item
    }
}
