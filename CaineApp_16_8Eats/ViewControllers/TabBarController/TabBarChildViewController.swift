//
//  TabBarChildViewController.swift
//  Quixly
//
//  Created by clines192 on 07/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

class TabBarChildViewController: UIViewController {

    var index:Int = 0
    
    @IBOutlet weak var lblTabIndex: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        lblTabIndex.text = "\(index)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
