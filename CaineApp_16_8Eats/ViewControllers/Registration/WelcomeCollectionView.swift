//
//  WelcomeCollectionView.swift
//  CaineApp_16_8Eats
//
//  Created by Mursaleen Moosa on 07/11/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

class WelcomeCollectionView: UICollectionView , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout ,UICollectionViewDelegate {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        dataSource = self
        delegate = self
    }
    
    
    // Mark : Collection View DataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WelcomeCollectionViewCell", for: indexPath) as! WelcomeCollectionViewCell
        if IS_IPHONE_X {
            cell.bgImageView.image = UIImage(named: "onboard-\(indexPath.row)X")
        } else {
            cell.bgImageView.image = UIImage(named: "onboard-\(indexPath.row)")
        }
        return cell
    }
    
    // Mark : Collection View Delegate Flow Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let controller = self.parentViewController as! WelcomeViewController
        controller.selectedIndexPath = (self.indexPath(for: self.visibleCells.first!)?.row)!
    }
    

}


