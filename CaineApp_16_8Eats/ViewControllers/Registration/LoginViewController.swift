//
//  LoginViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 10/30/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Parse
import Mixpanel
import ParseUI
import SDWebImage


class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emptyImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        Mixpanel.mainInstance().track(event: "Login View Screen", properties: ["Screen name" : "Login View Screen"])
        FEUserDefaults.sharedInstance.screenName = "Login Controller"
        FEUserDefaults.sharedInstance.startTime = Date()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object:nil)
        userNameTextField.becomeFirstResponder()
        isDataInserted()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        self.view.endEditing(true)
    }
    
    private func configureTextFields() {
        userNameTextField.setFontByScreenSize(fontName: CLANOT_NEWS, fontSize: 18)
        userNameTextField.delegate = self
        userNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.setFontByScreenSize(fontName: CLANOT_NEWS, fontSize: 18)
        passwordTextField.delegate = self
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        loginButton.isEnabled = false
        loginButton.setFont(withName: CLANOT_NEWS, withSize: 18)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(userNameTextField) {
            passwordTextField.becomeFirstResponder()
        } else if textField.isEqual(passwordTextField) {
            login()
        }
        return false
    }
    
    func isDataInserted(){
        if userNameTextField.text?.isStringEmpty == true || passwordTextField.text?.isStringEmpty == true {
            loginButton.isEnabled = false
        }
        else {
            loginButton.isEnabled = true
            
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        isDataInserted()
    }
    
    @IBAction func loginButtonClicked(_ sender: UIButton) {
        login()
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Welcome Screen")
        dismiss(animated: true, completion: nil)
    }
    
    //Mark: Keyboard Delegate
    @objc func keyboardWillShow(_ notification :NSNotification) {
//        let keyboardFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        btnLogin.bottomConstraint?.constant = keyboardFrame.size.height + 10
//        UIView.animate(withDuration: 0.25) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    private func login() {
//        if AppDelegate.sharedInstance.isNetConnected() == false {
//            showLongToast(INTERNET_CONNECTION_ERROR)
//            return
//        }
        view.showSpinner()
        PFUser.logInWithUsername(inBackground: (userNameTextField.text)!, password: (passwordTextField.text)!) { [weak self] (user, error) in
            if error == nil && user != nil {
                let loggedIn = user as? ParseUser
                API.sharedInstance.logLoginSuccesfulEvent(username: (loggedIn?.username)!, fullName: (loggedIn?.fullName)!, loginType: "Login With Username")
//                Branch.getInstance().setIdentity(loggedIn?.objectId)
                self?.updateUserData(user: loggedIn!)
            } else {
                self?.view.hideSpinner()
                self?.userNameTextField.becomeFirstResponder()
                showToast(withMessage: "Login Failed: Invalid Username/Password.")
            }
        }
        
    }
    
    
    //User Logged In Successfully and now update data
    
    func updateUserData(user: ParseUser){
        LSUserDefaults.sharedInstance.objectId = user.objectId
        LSUserDefaults.sharedInstance.fullName = user.fullName
        LSUserDefaults.sharedInstance.username = user.username
        LSUserDefaults.sharedInstance.workoutReminderTime = user.workoutTime
        LSUserDefaults.sharedInstance.isFacebookLogin = user.isFacebookLogin
        LSUserDefaults.sharedInstance.purchaseStatus = user.purchaseStatus
        LSUserDefaults.sharedInstance.workOutDay = user.workOutDay?.intValue
        LSUserDefaults.sharedInstance.lastWorkOutDate = getDate(Date())
        
        //When user login its last workout would remain same and Last Workout Date is changed to current date
        user.lastWorkOutDate = LSUserDefaults.sharedInstance.lastWorkOutDate
        user.saveInBackground()
        
        API.sharedInstance.saveAllWorkoutsInRealm().continueWith { [weak self] (task) -> Any? in
            if task.error != nil {
                print((task.error?.localizedDescription)!)
            } else {
                self?.updateUserPhoto(user: user)
            }
            return nil
        }
    }
    
    deinit {
        print("\(self) deinit called")
    }

    
    func updateUserPhoto(user : ParseUser) {
        view.endEditing(true)
        view.hideSpinner()
        
        if user.profilePhoto != nil {
            emptyImage.sd_setImage(with: URL(string: (user.profilePhoto?.url)!), placeholderImage: nil, options: SDWebImageOptions.refreshCached, completed: { [weak self] (image, error, type , url) in
                self?.changeController()
            })
        }
        else {
            changeController()
        }
    }
    
    func changeController() {
        UIViewController.clearTabbarController()
        
        //For testflight
        let user = ParseUser.current()!
        if  user.purchaseStatus == nil || (user.purchaseStatus?.int32Value)! <= NO_ACCOUNT {
            API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "In App Purchases Controller")
            let controller = UIViewController.InAppPurchaseController()
            UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: controller)
        }
        else {
            //Check if Current Workout Day is Synced otherwise open Workout Sync Screen and than move to First Controller
            let workout = API.sharedInstance.getCurrentWorkout(isSynced: false)
            if workout == nil {
                API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Daily Workout Controller")
                API.sharedInstance.syncDataInDisk()
                let controller = UIViewController.tabbarController()!
                controller.initialSelectedIndex = 0
                UIApplication.shared.keyWindow?.rootViewController = controller
            }
            else {
                API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Loading Workout Controller")
                let controller = UIViewController.LoadingDataViewController() as! LoadingDataViewController
                controller.workout = workout
                UIApplication.shared.keyWindow?.rootViewController = controller
            }
        }
    }
}
