//
//  SignupViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 10/30/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

import UIKit
import Parse
import Mixpanel

class SignupViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: "Signup Screen", properties: ["Screen name" : "Signup Screen"])
        FEUserDefaults.sharedInstance.screenName = "Signup Controller"
        FEUserDefaults.sharedInstance.startTime = Date()

//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        configureTextField()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    
    
    // MARK: - IBActions
    @IBAction func signupButtonClicked(_ sender: UIButton) {
        signupUser()
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Welcome Screen")
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Custom Functions
    private func configureTextField() {
        
        fullNameTextField.setFontByScreenSize(fontName: CLANOT_NEWS, fontSize: 20)
        fullNameTextField.delegate = self
        fullNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        usernameTextField.setFontByScreenSize(fontName: CLANOT_NEWS, fontSize: 20)
        usernameTextField.delegate = self
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.setFontByScreenSize(fontName: CLANOT_NEWS, fontSize: 20)
        passwordTextField.delegate = self
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        signupButton.isEnabled = false
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(fullNameTextField) {
            usernameTextField.becomeFirstResponder()
        } else if textField.isEqual(usernameTextField) {
            passwordTextField.becomeFirstResponder()
        } else if textField.isEqual(passwordTextField){
            signupUser()
        }
        return false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        isDataInserted()
    }
    
    func isDataInserted(){
        if fullNameTextField.text?.isStringEmpty == true || usernameTextField.text?.isStringEmpty == true || passwordTextField.text?.isStringEmpty == true {
            signupButton.isEnabled = false
        }
        else {
            signupButton.isEnabled = true
        }
    }
    
    
    private func signupUser() {
        
        view.showSpinner()
        let user = ParseUser()
        user.fullName = fullNameTextField.text
        user.username = usernameTextField.text
        user.password = passwordTextField.text
        user.purchaseStatus = NSNumber(value: NO_ACCOUNT)
        user.isFacebookLogin = false
        updateDataInUserDefaults(newUser: user)
        user.geoLocation = PFGeoPoint(location: API.sharedInstance.currentLocation)
        
        user.signUpInBackground { [weak self](success, error) in
            if success == true && error == nil {
                API.sharedInstance.logSignupSuccesfulEvent(username: user.username!, fullName: user.fullName!, signupType: "Signup With Username")
                self?.view.hideSpinner()
                self?.signupSuccessful()
            }
            else {
                self?.view.hideSpinner()
                showToast(withMessage: (error?.localizedDescription)!)
            }
        }
    }
    
    func updateDataInUserDefaults(newUser user : ParseUser) {
        LSUserDefaults.sharedInstance.fullName = user.fullName
        LSUserDefaults.sharedInstance.username = user.username
        LSUserDefaults.sharedInstance.purchaseStatus = user.purchaseStatus
        LSUserDefaults.sharedInstance.isFacebookLogin = user.isFacebookLogin
    }

    func showAlert(withTitle title: String, withMessage message: String) {
        
        let alert = AOAlertController.init(title: title, message: message, style: .alert)
        alert.configureFontAndColor()
        
        let okAction = AOAlertAction.init(title: "OK", style: .default, handler: nil)
        
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

    
    func signupSuccessful() {
        view.endEditing(true)
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Add Workout Reminder Controller")
        let controller = UIViewController.AddWorkoutTimerViewController()
        present(controller, animated: false, completion: nil)
    }
    
}
