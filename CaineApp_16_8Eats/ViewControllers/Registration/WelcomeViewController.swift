//
//  WelcomeViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 10/30/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Mixpanel
import Parse
import SDWebImage


class WelcomeViewController: UIViewController {

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // Replace _name to proper name of the field
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var facebookSignupButton: UIButton!
    
    @IBOutlet weak var collectionView : WelcomeCollectionView!
    @IBOutlet weak var emptyImage : UIImageView!
    
    @IBOutlet weak var integratorImageView0 : UIImageView!
    @IBOutlet weak var integratorImageView1 : UIImageView!
    @IBOutlet weak var integratorImageView2 : UIImageView!
    
    @IBOutlet weak var integratorViewBottomConstraint : NSLayoutConstraint!
    
    var selectedIndexPath : Int = 0 {
        didSet {
            if selectedIndexPath == 0 {
                setFirstIntegatorImage()
            } else if selectedIndexPath == 1 {
                setSecondIntegatorImage()
            } else {
                setThirdIntegatorImage()
            }
        }
    }

    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if IS_IPHONE_X {
            integratorViewBottomConstraint.constant = 60
        }
        Mixpanel.mainInstance().track(event: "Welcome Controller", properties: ["Screen name" : "Welcome Controller"])
        FEUserDefaults.sharedInstance.screenName = "Welcome Controller"
        FEUserDefaults.sharedInstance.startTime = Date()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    deinit {
        print("\(self) deinit called")
    }
    
    
    // MARK: - IBActions
    @IBAction func loginButtonClicked(_ sender: UIButton) {
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Login Controller")
        let controller = UIViewController.LoginViewController() as! LoginViewController
        present(controller, animated: true, completion: nil)
        
    }
    @IBAction func signupButtonClicked(_ sender: UIButton) {
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Signup Controller")
        let controller = UIViewController.SignupViewController() as! SignupViewController
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func facebookSignupButtonClicked(_ sender : UIButton) {
        let fbManager = LoginWithFacebook()
        fbManager.controller = self
        fbManager.facebookDelegate = self
        fbManager.facebookLogin()
        view.showSpinner()
    }
    
    private func setFirstIntegatorImage() {
        integratorImageView0.image = #imageLiteral(resourceName: "integratorSelected")
        integratorImageView1.image = #imageLiteral(resourceName: "integrator")
        integratorImageView2.image = #imageLiteral(resourceName: "integrator")
    }
    private func setSecondIntegatorImage() {
        integratorImageView0.image = #imageLiteral(resourceName: "integrator")
        integratorImageView1.image = #imageLiteral(resourceName: "integratorSelected")
        integratorImageView2.image = #imageLiteral(resourceName: "integrator")
    }
    private func setThirdIntegatorImage() {
        integratorImageView0.image = #imageLiteral(resourceName: "integrator")
        integratorImageView1.image = #imageLiteral(resourceName: "integrator")
        integratorImageView2.image = #imageLiteral(resourceName: "integratorSelected")
    }
    
    
}
extension WelcomeViewController : LoginWithFacebookDelegate {
    // MARK: Facebook login delegate
    func facebookLogin(withSuccess success: Bool) {
        if success == true  {
            if ParseUser().currentUser?.isNew == true {
                //New User Signup now fetch Image and go to next Controller
                let user = ParseUser.current()!
                API.sharedInstance.logSignupSuccesfulEvent(username: user.username!, fullName: user.fullName!, signupType: "Signup with Facebook")
                requestForGetFacebookImage(withUrl: FACEBOOK_IMAGE_URL)
            } else {
                view.hideSpinner()
                //User already created account now go to First Controller
                print("User previously logged In")
                let user = ParseUser.current()!
                API.sharedInstance.logLoginSuccesfulEvent(username: user.username!, fullName: user.fullName!, loginType: "Login With Facebook")
                updateUserData(user: user)
//                if user.profilePhoto != nil {
//                    emptyImage.sd_setImage(with: URL(string: (user.profilePhoto?.url)!), placeholderImage: nil, options: SDWebImageOptions.refreshCached, completed: { [weak self] (image, error, type , url) in
//                        self?.fetchUsersWorkout(user: user)
//                    })
//                }
//                else {
//                    fetchUsersWorkout(user: user)
//                }
                
            }
        } else {
            view.hideSpinner()
            //createAccountButton.isEnabled = true
            showToast(withMessage: "Facebook login failed!")
        }
    }
    
    func facebookLogin(withError error: Error) {
        view.hideSpinner()
        //createAccountButton.isEnabled = true
        showToast(withMessage: error.localizedDescription)
        print("Facebook login failed : \(error.localizedDescription)")
    }
    // For get facebook image
    func requestForGetFacebookImage(withUrl url : String) {
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let fetchProfileImage = session.dataTask(with: request as URLRequest) { [weak self] (data, response, error) -> Void in
            if (response as? HTTPURLResponse) != nil {
                if (PFUser.current()?.objectId) != nil {
                    _ = UIImage(data: data!)
                    ImageHelper.sharedInstance.saveInDocuments(data: data!, name: "\((PFUser.current()!.objectId)!).png")
                    //API.sharedInstance.updateEmailDPImage(UIImage(data: data!)!)
                    DispatchQueue.main.async {
                        self?.view.hideSpinner()
                        LSUserDefaults.sharedInstance.isFacebookLogin = true
                        print("Image Fatch Successfully")
                        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Add Workout Reminder Controller")
                        let controller = UIViewController.AddWorkoutTimerViewController() as! AddWorkoutTimerViewController
                        controller.isFromFacebook = true
                        self?.present(controller, animated: false, completion: nil)
                    }
                }
            }
        }
        fetchProfileImage.resume()
    }
    
    func updateUserData(user: ParseUser){
        LSUserDefaults.sharedInstance.objectId = user.objectId
        LSUserDefaults.sharedInstance.fullName = user.fullName
        LSUserDefaults.sharedInstance.username = user.username
        LSUserDefaults.sharedInstance.workoutReminderTime = user.workoutTime
        LSUserDefaults.sharedInstance.isFacebookLogin = user.isFacebookLogin
        LSUserDefaults.sharedInstance.purchaseStatus = user.purchaseStatus
        LSUserDefaults.sharedInstance.workOutDay = user.workOutDay?.intValue
        LSUserDefaults.sharedInstance.lastWorkOutDate = getDate(Date())
        //When user login its last workout would remain same and Last Workout Date is changed to current date
        user.lastWorkOutDate = LSUserDefaults.sharedInstance.lastWorkOutDate
        user.saveInBackground()
        
        API.sharedInstance.saveAllWorkoutsInRealm().continueWith { [weak self] (task) -> Any? in
            if task.error != nil {
                print((task.error?.localizedDescription)!)
            } else {
                self?.updateUserPhoto(user: user)
            }
            return nil
        }
    }
    
    func updateUserPhoto(user : ParseUser) {
        view.endEditing(true)
        view.hideSpinner()
        
        if user.profilePhoto != nil {
            emptyImage.sd_setImage(with: URL(string: (user.profilePhoto?.url)!), placeholderImage: nil, options: SDWebImageOptions.refreshCached, completed: { [weak self] (image, error, type , url) in
                self?.changeController()
            })
        }
        else {
            changeController()
        }
    }
    
    func changeController() {
        UIViewController.clearTabbarController()
        
        //For testflight
        let user = ParseUser.current()!
        if  user.purchaseStatus == nil || (user.purchaseStatus?.int32Value)! <= NO_ACCOUNT {
            API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "In App Purchases Controller")
            let controller = UIViewController.InAppPurchaseController()
            UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: controller)
        }
        else {
            //Check if Current Workout Day is Synced otherwise open Workout Sync Screen and than move to First Controller
            let workout = API.sharedInstance.getCurrentWorkout(isSynced: false)
            if workout == nil {
                API.sharedInstance.syncDataInDisk()
                API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Daily Workout Controller")
                let controller = UIViewController.tabbarController()!
                controller.initialSelectedIndex = 0
                UIApplication.shared.keyWindow?.rootViewController = controller
            }
            else {
                API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Loading Workout Controller")
                let controller = UIViewController.LoadingDataViewController() as! LoadingDataViewController
                controller.workout = workout
                UIApplication.shared.keyWindow?.rootViewController = controller
            }
        }
    }
}

