//
//  LoadingDataViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 12/28/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import SDWebImage
import Bolts
import Cloudinary
import RealmSwift
import Mixpanel

class LoadingDataViewController: UIViewController {

    var workout : WorkoutRealmModal!
    let realm = try! Realm()
    @IBOutlet weak var loadingImageView: UIImageView!
    @IBOutlet weak var loadingInfoLabel: UILabel!
    @IBOutlet weak var emptyImageView: UIImageView!
    var videoPlayerView : VideoPlayerView!
    let animateImageArray = [#imageLiteral(resourceName: "01") , #imageLiteral(resourceName: "02") , #imageLiteral(resourceName: "03") , #imageLiteral(resourceName: "04") , #imageLiteral(resourceName: "05") , #imageLiteral(resourceName: "06") , #imageLiteral(resourceName: "07") , #imageLiteral(resourceName: "08")]
    var totalCount = 0
    var completeCount = 1
    
    
    var loadingLabelText : String? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                UIView.animate(withDuration: 0.5, animations: {
                    self?.loadingInfoLabel.alpha = 0
                }) { (success) in
                    self?.loadingInfoLabel.text = self?.loadingLabelText
                    UIView.animate(withDuration: 0.5, animations: {
                        self?.loadingInfoLabel.alpha = 1
                    })
                }
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: "DataLoading Screen", properties: ["Screen name" : "DataLoading Screen"])
        loadImage()
        FEUserDefaults.sharedInstance.screenName = "Loading Data ViewController"
        FEUserDefaults.sharedInstance.startTime = Date()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimation()
    }
    deinit {
        print("\(self) deinit called")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadImage() {
        loadingInfoLabel.text = "loading workout image"
        if (workout.isWorkoutImageSynced == false) {
            emptyImageView.sd_setImage(with: URL(string: (workout?.workoutImageUrl)!), placeholderImage: nil, options: SDWebImageOptions.refreshCached) { [weak self] (image, error, type, url) in
                if error == nil {
//                    self?.emptyImageView.image = image
                    try! self?.realm.write {
                        self?.workout.isWorkoutImageSynced = true
                        self?.realm.add((self?.workout)!, update: true)
                    }
                    print("Image is Synced")
                    self?.loadVideos()
                }
            }
        }
        else {
            loadVideos()
        }
    }
    
    private func startAnimation() {
        loadingImageView.animationImages = animateImageArray
        loadingImageView.animationDuration = 2.0
        loadingImageView.animationRepeatCount = 0
        loadingImageView.startAnimating()
    }
    
    func loadVideos() {
        var counter = 0
        totalCount =  workout.workoutVideoUrls.count
        if totalCount == 0 {
            completeCount = 0
        }
        loadingInfoLabel.text = "loading workout videos \((completeCount)) / \((totalCount))"
        for workoutVideo in (workout?.workoutVideoUrls)! {
            downloadVideo(atIndex: counter, withUrl: workoutVideo)
            counter = counter + 1
        }
    }
    
    func downloadVideo(atIndex index : Int, withUrl url : String) {
        let name = "\(workout.objectId!)\(index).mov"
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let fetchProfileImage = session.dataTask(with: request as URLRequest) { [weak self] (data, response, error) -> Void in
            if (response as? HTTPURLResponse) != nil {
                if data == nil {
                    self?.downloadVideo(atIndex: index, withUrl: url)
                    return
                }
                if (self?.completeCount)! < (self?.totalCount)! {
                    self?.completeCount = (self?.completeCount)! + 1
                }
                ImageHelper.sharedInstance.saveInDocuments(data: data!, name: name)
                DispatchQueue.main.async { [weak self] in
                    self?.loadingInfoLabel.text = "loading workout videos \((self?.completeCount)!) / \((self?.totalCount)!)"
                    try! self?.realm.write {
                        self?.workout.isWorkoutVideosSynced[index] = true
                        if index == ((self?.workout.isWorkoutVideosSynced.count)! - 1) {
                            self?.workout.isWorkoutSynced = true
                        }
                        self?.realm.add((self?.workout)!, update: true)
                        self?.callNextController()
                    }
                }
            }
        }
        fetchProfileImage.resume()
    }
    
    func callNextController() {
        if workout.isWorkoutSynced == true {
            loadingImageView.stopAnimating()
            API.sharedInstance.syncDataInDisk()
            API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Daily Workout Controller")
            let controller = UIViewController.tabbarController()!
            controller.initialSelectedIndex = 0
            UIApplication.shared.keyWindow?.rootViewController = controller
        }
    }

}
