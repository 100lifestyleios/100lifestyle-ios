//
//  AddPhotoViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 10/1/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Parse
import SDWebImage
import Mixpanel


class AddPhotoViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    var isImageUpdated : Bool = false
    var userImage : UIImage?
    var user : ParseUser?
    var file : PFFile?
    
    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: "Add Photo Controller", properties: ["Screen name" : "Add Photo Controller"])
        user = ParseUser.current()
        userNameLabel.text = user?.fullName
        userImageView.makeRounded(cornerWithRadius: 176)
        if userImage != nil {
            userImageView.image = userImage
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    // MARK: - IBActions
    @IBAction func addProfileImageButtonClicked(_ sender: UIButton) {
        callCameraController()
    }

    
    @IBAction func doneButtonClicked(_ sender: UIButton) {
        sender.isEnabled = false
        if isImageUpdated == true {
            user?.profilePhoto = file
            updateUser()
        }
        else {
            dismiss(animated: false, completion: nil)
        }
    }
    
    func updateUser() {
        
        view.showSpinner()
        user?.saveInBackground(block: { [weak self] (success, error) in
            if success == true {
                self?.callNextController()
            }
            else {
                self?.doneButton.isEnabled = true
                showToast(withMessage: (error?.localizedDescription)!)
            }
        })
    }
    
    
    func callNextController() {
        
        
        if user?.profilePhoto != nil {
            userImageView.sd_setImage(with: URL(string: (user?.profilePhoto?.url)!), placeholderImage: userImage, options: SDWebImageOptions.refreshCached, completed: { [weak self] (image, error, type , url) in
                self?.view.hideSpinner()
                self?.dismiss(animated: false, completion: nil)
            })
        }
        else {
            view.hideSpinner()
            dismiss(animated: false, completion: nil)
        }
        API.sharedInstance.logImageCaptureEvent(username: (user?.username)!, fullName: (user?.fullName)!, ImageCapture: "Image Capture Succesful")
    }
    
    // MARK: - Custom Functions

}

extension AddPhotoViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    func callCameraController() {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        showActionSheet(picker)
    }
    private func showActionSheet(_ picker : UIImagePickerController) {
        let alertController = AOAlertController(title: "Photo Source", message: "", style: .actionSheet)
        alertController.configureFontAndColor()
        
        let cameraAction = AOAlertAction(title: "Camera", style: .default) { [weak self] in
            picker.sourceType = .camera
            self?.presentedViewController?.dismiss(animated: false, completion: {
                self?.present(picker, animated: true, completion: nil)
            })
        }
        cameraAction.configureDefaultAction()
        
        let libraryAction = AOAlertAction(title: "Library", style: .default) { [weak self] in
            picker.sourceType = .photoLibrary
            self?.presentedViewController?.dismiss(animated: false, completion: {
                self?.present(picker, animated: true, completion: nil)
            })
        }
        libraryAction.configureDefaultAction()
        
        let cancelAction = AOAlertAction(title: "Cancel", style: .cancel, handler: nil)
        cancelAction.configureCancelAction()
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) == true {
            alertController.addAction(cameraAction)
        }
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    // MARK : UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        API.sharedInstance.logImageCaptureEvent(username: (user?.username)!, fullName: (user?.fullName)!, ImageCapture: "Image Capture Cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let possibleImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            userImageView.image = possibleImage
        } else if let possibleImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            userImageView.image = possibleImage
        } else {
            return
        }
        let data = UIImagePNGRepresentation(userImageView.image!)
        file = PFFile(data: data!)!
        doneButton.isEnabled = true
        isImageUpdated = true
        userImage = userImageView.image
        dismiss(animated: true, completion: nil)
    }
}
