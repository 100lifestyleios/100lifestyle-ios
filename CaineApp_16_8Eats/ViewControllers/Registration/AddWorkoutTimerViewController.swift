//
//  AddWorkoutTimerViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 11/8/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Parse
import SDWebImage

class AddWorkoutTimerViewController: UIViewController {

    @IBOutlet weak var workoutTimeDatePicker: UIDatePicker!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var emptyImage: UIImageView!
    
    var isFromFacebook : Bool = false
    var isFromProfile : Bool = false
    let user = ParseUser.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if LSUserDefaults.sharedInstance.workoutReminderTime != nil {
            workoutTimeDatePicker.date = LSUserDefaults.sharedInstance.workoutReminderTime!
        }
        FEUserDefaults.sharedInstance.screenName = "Workout Reminder Time Controller"
        FEUserDefaults.sharedInstance.startTime = Date()
        
        doneButton.isEnabled = (isFromProfile == true) ? true : false
        workoutTimeDatePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)

        // Do any additional setup after loading the view.
    }
    deinit {
        print("\(self) deinit called")
    }
    
    
    func dateChanged(sender: UIDatePicker) {
        doneButton.isEnabled = true
        let date = setWorkoutTime(sender.date)
        LSUserDefaults.sharedInstance.workoutReminderTime = date
    }
    @IBAction func doneButtonClicked(_ sender: UIButton) {
        
        view.showSpinner()
        sender.isEnabled = false
        if isFromFacebook == true {
            user?.fullName = LSUserDefaults.sharedInstance.fullName
            user?.isFacebookLogin = LSUserDefaults.sharedInstance.isFacebookLogin!
            user?.geoLocation = PFGeoPoint(location: API.sharedInstance.currentLocation)
            let imageData = ImageHelper.sharedInstance.getFromDocuments(name: "\((user?.objectId)!).png")
            let file = PFFile(data: imageData!)
            user?.profilePhoto = file
        }
        
        //Set Last Work out time and work out day
        if isFromProfile == false {
            user?.lastWorkOutDate = getDate(Date())
            LSUserDefaults.sharedInstance.lastWorkOutDate = user?.lastWorkOutDate
            
            user?.workOutDay = 1 // its always one on signup
            LSUserDefaults.sharedInstance.workOutDay = 1
        }
        user?.workoutTime = LSUserDefaults.sharedInstance.workoutReminderTime
        LSUserDefaults.sharedInstance.objectId = user?.objectId
        user?.purchaseSuccessful = false
        user?.saveInBackground(block: { [weak self] (success, error) in
            if error == nil && success == true {
                self?.updateSuccessful()
            }
            else {
                self?.view.hideSpinner()
                showToast(withMessage: (error?.localizedDescription)!)
                sender.isEnabled = true
            }
        })
    }
    
    
    func updateSuccessful() {
        
        if isFromProfile == true {
            let workoutTime = user?.workoutTime?.getStringFromDate()
            print("Workout Time is \(workoutTime!)")
            API.sharedInstance.logWorkoutTimeAddedEvent(username: (user?.username)!, fullName: (user?.fullName)!, workoutTime: workoutTime!, actionType: "Update")
            dismiss(animated: false, completion: nil)
        }
        else {
            let workoutTime = user?.workoutTime?.getStringFromDate()
            print("Workout Time is \(workoutTime!)")
            API.sharedInstance.logWorkoutTimeAddedEvent(username: (user?.username)!, fullName: (user?.fullName)!, workoutTime: workoutTime!, actionType: "Add")
            if user?.profilePhoto != nil {
                emptyImage.sd_setImage(with: URL(string: (user?.profilePhoto?.url)!), placeholderImage: nil, options: SDWebImageOptions.refreshCached, completed: { [weak self] (image, error, type , url) in
                    self?.fetchWorkoutData()
                })
            }
            else {
                fetchWorkoutData()
            }
        }
    }
    
    func fetchWorkoutData() {
        API.sharedInstance.saveAllWorkoutsInRealm().continueWith { [weak self] (task) -> Any? in
            if task.error != nil {
                print((task.error?.localizedDescription)!)
            } else {
                self?.callNextController()
            }
            return nil
        }
    }
    
    func callNextController() {
        //After signup we will always open In App Purchases screen but for debugging purpose we can skip in by executing debugging without In App Purchases
        
        //For Testflight
        UIViewController.clearTabbarController()
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "In App Purchases Controller")
        let controller = UIViewController.InAppPurchaseController()
        UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: controller)
        
        //For debugging without In App Purchases
        //        let controller = UIViewController.tabbarController()!
        //        controller.initialSelectedIndex = 0
        //        UIApplication.shared.keyWindow?.rootViewController = controller
        
    }
}
