//
//  NearestBootCampViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 11/1/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class NearestBootCampViewController: UIViewController, UISearchBarDelegate, LocateOnTheMap, GMSAutocompleteFetcherDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var setMapCameraToCurrentLocation: UIButton!
    var searchResultController: SearchResultsController!
    var resultsArray = [String]()
    var mapFetcher: GMSAutocompleteFetcher!
    
    var googleMapsView: GMSMapView!
    let location = API.sharedInstance.currentLocation
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
    }

    private func configureNavigationBar() {
        navigationItem.title = "Nearest FREE BOOTCAMPS"
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchWithAddress))
        searchButton.tintColor = UIColor().UIColorFromRGB(0x4d4d4d)
        navigationItem.rightBarButtonItem = searchButton
        FEUserDefaults.sharedInstance.screenName = "Nearest Bootcamp ViewController"
        FEUserDefaults.sharedInstance.startTime = Date()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Tabbar")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setMap()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        googleMapsView.removeFromSuperview()
    }

    func setMap() {
        
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 14.0)
        googleMapsView = GMSMapView.map(withFrame: containerView.frame, camera: camera)
        view.addSubview(googleMapsView)
        view.bringSubview(toFront: setMapCameraToCurrentLocation)
        
        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        mapFetcher = GMSAutocompleteFetcher()
        mapFetcher.delegate = self
        
//        let marker = GMSMarker()
        //        marker.position = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!+1, longitude: (location?.coordinate.longitude)!+1)
//        marker.position = CLLocationCoordinate2D(latitude: 24.863194, longitude: 67.074657)
//        marker.title = "Berlitz"
//        marker.snippet = "School"
//        marker.map = googleMapsView
//
//        let marker2 = GMSMarker()
//        marker2.position = CLLocationCoordinate2D(latitude: 24.910123, longitude: 67.125148)
//        marker2.title = "Jauhar Square"
//        marker2.snippet = "Home"
//        marker2.map = googleMapsView
        
    }
    @objc func searchWithAddress() {
        
        let searchController = UISearchController(searchResultsController: searchResultController)
        searchController.searchBar.delegate = self
        self.present(searchController, animated:true, completion: nil)
        
//        searchBarView.frame = frame
//        navigationItem.rightBarButtonItem = nil
//        navigationItem.titleView = searchBarView
//        searchBar.becomeFirstResponder()
        
    }
    
    @IBAction func setMapCameraToCurrentLocationClicked(_ sender: UIButton) {
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 14.0)
        googleMapsView.camera = camera
    }
    
    
    //MARK : Search Bar

    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.resultsArray.removeAll()
        mapFetcher?.sourceTextHasChanged(searchText)
    }
    
    public func didFailAutocompleteWithError(_ error: Error) {
        //        resultText?.text = error.localizedDescription
    }
    
    /**
     * Called when autocomplete predictions are available.
     * @param predictions an array of GMSAutocompletePrediction objects.
     */
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        //self.resultsArray.count + 1
        
        for prediction in predictions {
            
            if let prediction = prediction as GMSAutocompletePrediction?{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        self.searchResultController.reloadDataWithArray(self.resultsArray)
        //   self.searchResultsTable.reloadDataWithArray(self.resultsArray)
        print(resultsArray)
    }

    /**
     Locate map with longitude and longitude after search location on UISearchBar
     
     - parameter lon:   longitude location
     - parameter lat:   latitude location
     - parameter title: title of address location
     */
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        
        
        
        DispatchQueue.main.async { () -> Void in
            
            let position = CLLocationCoordinate2DMake(lat, lon)
            let marker = GMSMarker(position: position)
            
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 14)
            self.googleMapsView.camera = camera
            
//            marker.title = "Address : \(title)"
//            marker.map = self.googleMapsView
            
        }
        
    }
}

//          Additional Information If you want to add on map
//          marker.title = "Sydney"
//          marker.snippet = "Australia"
