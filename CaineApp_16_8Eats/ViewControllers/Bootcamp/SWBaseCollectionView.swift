//
//  SWBaseCollectionView.swift
//  SurfWind
//
//  Created by Afnan on 23/04/2018.
//  Copyright © 2018 iSystematic. All rights reserved.
//

import UIKit

protocol PageChangeCollectionViewDelegate {
    func pageChanged(_ number : Int)
}


class SWBaseCollectionView: UICollectionView {

    public var presentedIndex : Int = 0
     var pageChangeDelegate:PageChangeCollectionViewDelegate?

     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("ScrollView did end editng")
        var visibleRect = CGRect()
        visibleRect.origin = self.contentOffset
        visibleRect.size = self.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        var visibleIndexPath : IndexPath?
        visibleIndexPath = (self.indexPathForItem(at: visiblePoint))
        if  visibleIndexPath ==  nil ||  self.cellForItem(at: visibleIndexPath!) == nil { return}
        presentedIndex = (visibleIndexPath?.row)!
        pageChangeDelegate?.pageChanged(presentedIndex)
    }
    
    
    
}
