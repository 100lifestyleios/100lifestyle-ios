//
//  DailyWorkOutViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 11/1/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Mixpanel
import Cloudinary
import SDWebImage

let START = 1
let STOP = 2

class DailyWorkOutViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , PageChangeCollectionViewDelegate{
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBOutlet weak var collectionView: SWBaseCollectionView!
    @IBOutlet weak var indicatorView: SWIndicatorView!
    @IBOutlet weak var timerLabel : UILabel!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var titleLabelView: UIView!
    @IBOutlet weak var titleLabelViewWidthConstraint: NSLayoutConstraint!
    var timer : Timer?
    var index = 0
    var secondsRemaining = 0
    var lastDisplay = false
    var isStopWatch = false
    var workout : WorkoutRealmModal!
    var oldCell : WorkoutCollectionViewCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Mixpanel.mainInstance().track(event: "Daily WorkOut Screen", properties: ["Screen name" : "Daily WorkOut Screen"])
        collectionView.pageChangeDelegate = self
        FEUserDefaults.sharedInstance.screenName = "Daily Workout ViewController"
        FEUserDefaults.sharedInstance.startTime = Date()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if oldCell != nil {
            collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: false)
            pageChanged(0)
        }
        workout = API.sharedInstance.getCurrentWorkout(isSynced: true)
        indicatorView.maxiumNumberOfImages = (workout.workoutVideoUrls.count) + 1
        indicatorView.selectedImage = 0
        secondsRemaining = (workout.workoutDuration?.intValue)!
        if secondsRemaining == 0 { isStopWatch = true}
        collectionView.reloadData()
        lastDisplay = false
        hideTitleLabelViewWithoutAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideTimerView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        oldCell?.videoPlayerView.stopVideo()
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Tabbar")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopTimerView()
    }
    deinit {
        print("\(self) deinit called")
    }
    // MARK: - CollectionView Delegates
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if workout == nil { return 0 }
        if workout.workoutVideoUrls == nil { return 0 }
        return (workout.workoutVideoUrls.count) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WORKOUTCELL, for: indexPath) as! WorkoutCollectionViewCell
        if indexPath.row == 0 {
            cell.workoutImageView.isHidden = false
            
            if workout.workoutNumericalName == 1 {
                cell.workoutImageView.image = UIImage(named: (workout.workoutImageUrl)!)
            }
            else {
                let imageName = workout.objectId! + ".png"
                if let data = ImageHelper.sharedInstance.getFromDocuments(name: imageName) {
                    cell.workoutImageView.image = UIImage(data: data)
                }
                else {
                    cell.workoutImageView.sd_setImage(with: URL(string: workout.workoutImageUrl!), placeholderImage: nil, options: SDWebImageOptions.refreshCached, completed: nil)
                }
            }
        } else{
            cell.workoutImageView.isHidden = true
            if workout.workoutNumericalName == 1 {
                cell.videoPlayerView.configureVideoPlayer(withUrl: getVideoForLocalPath(withIndex: indexPath))
            }
            else {
                cell.videoPlayerView.configureVideoPlayer(withUrl: getVideoForPath(withIndex: indexPath))
            }
        }
        return cell
    }
    
    private func getVideoForPath(withIndex index : IndexPath) -> URL{
        let name = "\(workout.objectId!)\(index.item-1).mov"
        return URL(string: name)!
    }
    
    private func getVideoForLocalPath(withIndex indexPath : IndexPath) -> URL{
        let videoName = "\(indexPath.row)"
        let videoURL = Bundle.main.path(forResource: videoName, ofType: "mov", inDirectory: "Day1")
        return URL(fileURLWithPath: videoURL!)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        index = indexPath.item
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = scrollView.contentOffset
        visibleRect.size = scrollView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        var visibleIndexPath : IndexPath?
        visibleIndexPath = (collectionView.indexPathForItem(at: visiblePoint))
        if  visibleIndexPath ==  nil ||  collectionView.cellForItem(at: visibleIndexPath!) == nil { return }
        pageChanged((visibleIndexPath?.row)!)
        if oldCell != nil { oldCell?.videoPlayerView.stopVideo() }
        if visibleIndexPath?.row == 0 {
            hideTitleLabelViewWithoutAnimation()
            return
        }
        oldCell = collectionView.cellForItem(at: visibleIndexPath!) as? WorkoutCollectionViewCell
        oldCell?.videoPlayerView.playVideo()
       
        
        if (workout.workoutVideoUrls.count == index) {
            if (index == (visibleIndexPath?.item)! && lastDisplay == false) {
                hideTitleLabelViewWithoutAnimation()
                index = (visibleIndexPath?.item)!
                let title = workout.workoutVideoTitles[(index-1)]
                titleLabel.text = "  \(title)  ".uppercased()
                view.layoutSubviews()
                showTitleView()
            }
            lastDisplay = true
        }
        else {
            if (index == (visibleIndexPath?.item)!) {
                hideTitleLabelViewWithoutAnimation()
                index = (visibleIndexPath?.item)!
                let title = workout.workoutVideoTitles[(index-1)]
                titleLabel.text = "  \(title)  ".uppercased()
                view.layoutSubviews()
                showTitleView()
            }
        }
        
    }
    
    func pageChanged(_ number: Int) {
        print("page changed")
        indicatorView.selectedImage = number
    }
    
    @IBAction func startButtonClicked(_ sender: UIButton) {
        if sender.tag == START {
            secondsRemaining = (workout.workoutDuration?.intValue)!
            showTimerView()
            sender.setImage(#imageLiteral(resourceName: "btnStop"), for: .normal)
            sender.tag = STOP
            isStopWatch == true ? startStopWatch() : startTimer()
        } else if sender.tag == STOP {
            secondsRemaining = 0
            isStopWatch == true ? stopStopWatch() : stopTimerView()
        }        
    }
    
    func hideTimerView() {
//        UIView.animate(withDuration: 0.3, animations: { [weak self] in
//            let height : CGFloat = -(self?.timerView.frame.size.height)!
//            self?.timerView.transform = CGAffineTransform(translationX: 0, y: height)
//        }) { [weak self] (success) in
//            self?.timerView.isHidden = true
//        }
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            
            self?.timerLabel.alpha = 0.0
            //            self?.timerView.transform = .identity
            //            self?.timerView.isHidden = false
        }
    }
    
    func showTimerView() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            
            self?.timerLabel.alpha = 1.0
//            self?.timerView.transform = .identity
//            self?.timerView.isHidden = false
        }
    }
    
    
    func showTitleView() {
        let width = titleLabel.frame.width
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.titleBarView.alpha = 1.0
            self?.titleLabelViewWidthConstraint.constant = width
            self?.view.layoutSubviews()
        }) { [weak self] (success) in
            self?.titleLabel.alpha = 1.0
        }
    }
    
    func hideTitleLabelViewWithoutAnimation() {
        lastDisplay = false
        titleLabel.alpha = 0.0
        titleBarView.alpha = 0.0
        titleLabelViewWidthConstraint.constant = 0
        view.layoutSubviews()
        
    }
    
    func hideTitleView() {
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.titleLabel.alpha = 0.0
            
        }) { [weak self] (success) in
            if success == true {
                self?.hideTitleLabelView()
            }
        }
    }
    
    func hideTitleLabelView() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.titleBarView.alpha = 0.0
            self?.titleLabelViewWidthConstraint.constant = 0
            self?.view.layoutIfNeeded()
        }
    }
    
    
    
    func startTimer() {
        if index == 0 {
            moveCell()
        }
        timerLabel.text = String(format: "%02d:%02d", (secondsRemaining / 60), (secondsRemaining % 60))
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] (timers) in
            self?.secondsRemaining = (self?.secondsRemaining)! - 1
            self?.timerLabel.text = String(format: "%02d:%02d", ((self?.secondsRemaining)! / 60), ((self?.secondsRemaining)! % 60))
            if (self?.secondsRemaining)! <= 0 {
                self?.timerLabel.text = "00:00"
                self?.stopTimerView()
            }
            
        })
    }
    
    func stopTimerView() {
        timer?.invalidate()
        timer = nil
        stopButton.setImage(#imageLiteral(resourceName: "btnStart"), for: .normal)
        stopButton.tag = START
        hideTimerView()
    }
    
    func startStopWatch() {
        if index == 0 {
            moveCell()
        }
        timerLabel.text = String(format: "%02d:%02d", 0, 0)
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] (timers) in
            self?.secondsRemaining = (self?.secondsRemaining)! + 1
            self?.timerLabel.text = String(format: "%02d:%02d", ((self?.secondsRemaining)! / 60), ((self?.secondsRemaining)! % 60))
            //            if (self?.secondsRemaining)! <= 0 {
            //                self?.timerLabel.text = "00:00"
            //                self?.stopTimerView()
            //            }
        })
    }
    
    func stopStopWatch() {
        timer?.invalidate()
        timer = nil
        stopButton.setImage(#imageLiteral(resourceName: "btnStart"), for: .normal)
        stopButton.tag = START
        hideTimerView()
    }

    
    func moveCell() {
        snapToCell(at: 1, withAnimation: true) //paas index here to move to.
    }

    func snapToCell(at index: Int, withAnimation animated: Bool) {
        print("move")
        let indexPath = IndexPath(row: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .left, animated: animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.scrollViewDidEndDecelerating(self.collectionView)
        }
    }
}
