//
//  InAppFactory.h
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 30/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InAppFactory : NSObject

+ (instancetype)sharedInstance;

/**
 Request for list of product and save **/
-(void)requestProducts:(void (^)(NSString * ))error ;

-(void)monthlySubscription:(void (^)(NSError * ))error ;

-(void)yearlySubscription:(void (^)(NSError * ))error ;

    
-(void)checkExpirationDateIfUserHasPurchased;
/**
 in Splash check if receipt has been expired
 **/
-(void)refreshInAppReceipt;

-(void)purchaseSuccessfully:(int )purchaseType withExpiryDate:(NSString *)expirydate;

@end
