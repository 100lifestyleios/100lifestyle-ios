//
//  InAppTestController.m
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 30/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import "InAppTestController.h"


@interface InAppTestController ()

@end

@implementation InAppTestController



- (void)viewDidLoad {
    [super viewDidLoad];
    [[InAppFactory sharedInstance] checkExpirationDateIfUserHasPurchased];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)inAppTestButtonClicked:(UIButton *)sender {
    NSLog(@"Inapp Test ");
    // if([UserData getAccountType] > NO_ACCOUNT) return;
    
    [sender setEnabled:false];
    // Ayaz: Test Code for Auto Renew-able subscriptions
    __weak typeof(self) weakSelf = self;
    [[InAppFactory sharedInstance] monthlySubscription:^(NSError * error) {
        [sender setEnabled:true];
        
    }];
    
}





@end
