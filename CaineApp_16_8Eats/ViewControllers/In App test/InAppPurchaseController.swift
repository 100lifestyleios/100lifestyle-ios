//
//  InAppPurchaseController.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 03/11/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import SafariServices

class InAppPurchaseController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    
    // MARK : view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        navigationItem.title = "Special Offer"
        FEUserDefaults.sharedInstance.screenName = "In App Purchases Controller"
        FEUserDefaults.sharedInstance.startTime = Date()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    @objc func subscribeForMonthly(_ sender : UIButton) {
        sender.isEnabled = false
        self.view.showSpinner()
        InAppFactory.sharedInstance().monthlySubscription {[weak self] (error) in
            self?.view.hideSpinner()
            if error != nil {
                sender.isEnabled = true
                showToast(withMessage: (error?.localizedDescription)!)
            } else {
                let user = ParseUser.current()
                API.sharedInstance.logInAppPurchaseMonthlyPurchaseEvent(username: (user?.username)!, fullName: (user?.fullName)!, purchaseAmount: 19.99)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    self?.changeController()
                })
            }
        }
    }
    
    @IBAction func termsOfServiceClicked(_ sender :UIButton) {
        //Call Terms
        let url = URL(string: "https://100clubapp.com/terms/")
        openSafariViewControllerWithURL(url: url!)
    }
    
    @IBAction func privacyPolicyClicked(_ sender :UIButton) {
        //Call Privacy
        let url = URL(string: "https://100clubapp.com/privacy/")
        openSafariViewControllerWithURL(url: url!)
    }
    
    @objc func subscribeForYearLy(_ sender : UIButton) {
        sender.isEnabled = false
        self.view.showSpinner()
        InAppFactory.sharedInstance().yearlySubscription {[weak self] (error) in
            self?.view.hideSpinner()
            if error != nil {
                sender.isEnabled = true
                showToast(withMessage: (error?.localizedDescription)!)
            } else {
                showToast(withMessage: "Subscribed for Free trial successfully")
                let user = ParseUser.current()
                API.sharedInstance.logInAppPurchaseAnnualPurchaseEvent(username: (user?.username)!, fullName: (user?.fullName)!, purchaseAmount: 119.99)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    self?.changeController()
                })
            }
        }
    }
    
    @objc func callAnnualSubscriptionScreen(_ sender : UIButton) {
        sender.isEnabled = false
        print("Call Annual Subscription Screen")
    }
    
    func openSafariViewControllerWithURL(url : URL) {
        
        let sfvc = SFSafariViewController(url: url)
        present(sfvc, animated: true)
        //        } else {
        //            UIApplication.shared.openURL(url)
        //        }
    }
    
    func changeController() {
        UIViewController.clearTabbarController()
        let user = ParseUser.current()
        user?.purchaseSuccessful = true
        user?.saveInBackground()
        InAppFactory.sharedInstance().checkExpirationDateIfUserHasPurchased()
        navigationController?.viewControllers.removeAll()
        //Check if Current Workout Day is Synced otherwise open Workout Sync Screen and than move to First Controller
        let workout = API.sharedInstance.getCurrentWorkout(isSynced: false)
        if workout == nil {
            API.sharedInstance.syncDataInDisk()
            API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Daily Workout Controller")
            let controller = UIViewController.tabbarController()!
            controller.initialSelectedIndex = 0
            UIApplication.shared.keyWindow?.rootViewController = controller
        }
        else {
            API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Loading Workout Controller")
            let controller = UIViewController.LoadingDataViewController() as! LoadingDataViewController
            controller.workout = workout
            UIApplication.shared.keyWindow?.rootViewController = controller
        }
    }
    
    
    
}

extension InAppPurchaseController : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InAppPurchaseTableCell") as! InAppPurchaseTableCell
        if indexPath.row != 3 {
           cell.bgImageView.image = UIImage(named: "inApp\(indexPath.row)")
        }
        if indexPath.row == 1 {
            cell.trySevenDayFreeButton.isHidden = false
            cell.trySevenDayFreeButton.setImage(#imageLiteral(resourceName: "annualPlanBtn"), for: .normal)
            cell.trySevenDayFreeButton.setImage(#imageLiteral(resourceName: "annualPlanBtnFill"), for: .highlighted)
            cell.trySevenDayFreeButton.addTarget(self, action: #selector(subscribeForYearLy(_:)), for: .touchUpInside) // Annual Subscription

        }  else if indexPath.row == 2 {
            cell.trySevenDayFreeButton.isHidden = false
            cell.trySevenDayFreeButton.setImage(#imageLiteral(resourceName: "monthlyPlanBtn"), for: .normal)
            cell.trySevenDayFreeButton.setImage(#imageLiteral(resourceName: "monthlyPlanBtnFill"), for: .highlighted)
            cell.trySevenDayFreeButton.addTarget(self, action: #selector(subscribeForMonthly(_:)), for: .touchUpInside) // Monthly Subscription
        } else {
            cell.trySevenDayFreeButton.isHidden = true
        }
        
        if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InAppRestorePurchaseTableCell") as! InAppRestorePurchaseTableCell
            cell.restorePurchaseButton.addTarget(self, action: #selector(restorePurchaseButtonClicked(_:)), for: .touchUpInside)
            return cell
        }
        return cell
    }
    
    @objc func restorePurchaseButtonClicked(_ sender : UIButton) {
        // Restore Purchase work here
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getCellHeight(withIndexPath: indexPath.row)
    }
    private func getCellHeight(withIndexPath index : Int) -> CGFloat {
        var cellHeight : CGFloat = 0.0
        if index == 0      { cellHeight = 266 * screenWidthRatio }
        else if index == 1 { cellHeight = 292 * screenWidthRatio }
        else if index == 2 { cellHeight = 224 * screenWidthRatio }
        else if index == 3 { cellHeight = 360 * screenWidthRatio }
        else if index == 4 { cellHeight = 600 * screenWidthRatio }
        else if index == 5 { cellHeight = 948 * screenWidthRatio }
        return cellHeight
    }
}
