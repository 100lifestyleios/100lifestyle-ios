//
//  InAppAPI.h
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 30/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IAPHelper.h"


@interface InAppAPI : NSObject


+ (instancetype)sharedInstance;


@property (strong, nonatomic) IAPHelper * iapHelper;


@end
