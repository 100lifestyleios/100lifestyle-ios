//
//  InAppFactory.m
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 30/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import "InAppFactory.h"
#import "NSDate+Escort.h"


static InAppFactory *sharedInstance;

@implementation InAppFactory

+ (instancetype)sharedInstance {
    static dispatch_once_t DDASLLoggerOnceToken;
    dispatch_once(&DDASLLoggerOnceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

-(void)requestProducts:(void (^)(NSString * ))error {
    
    [[InAppAPI sharedInstance].iapHelper requestProductsWithCompletion:^(SKProductsRequest *request, SKProductsResponse *response) {
        NSLog(@"product downloaded");
        error(@"Testing");
    }];
}

-(void)monthlySubscription:(void (^)(NSError * ))error {
    
    [[InAppAPI sharedInstance].iapHelper buyMonthlyPackageWithCompletion:^(SKPaymentTransaction *transcation, NSString *productIdentifier, NSDate *expiryDate) {
         if(transcation.error && transcation.transactionState == SKPaymentTransactionStateFailed)
        {
            error(transcation.error);
            // [weakSelf.view makeToast:transcation.error.localizedDescription];
            return;
        }
        //   [self.view makeToast:@"Purchase item successfully"];
        error(nil);

        [[InAppFactory sharedInstance] purchaseSuccessfully:[[InAppUtils sharedInstance] verifyProductIdentify:productIdentifier] withExpiryDate:[InAppUtils getStringFromDate:expiryDate]];
    }];
}

-(void)yearlySubscription:(void (^)(NSError * ))error {
    
    [[InAppAPI sharedInstance].iapHelper buyYearlylyPackageWithCompletion:^(SKPaymentTransaction *transcation, NSString *productIdentifier, NSDate *expiryDate) {
        if(transcation.error && transcation.transactionState == SKPaymentTransactionStateFailed)
        {
            error(transcation.error);
            // [weakSelf.view makeToast:transcation.error.localizedDescription];
            return;
        }
        //   [self.view makeToast:@"Purchase item successfully"];
        error(nil);
        
        [[InAppFactory sharedInstance] purchaseSuccessfully:[[InAppUtils sharedInstance] verifyProductIdentify:productIdentifier] withExpiryDate:[InAppUtils getStringFromDate:expiryDate]];
    }];
}


-(void)checkExpirationDateIfUserHasPurchased {
    
    if([UserData getAccountType]<=NO_ACCOUNT) return;
    int runningTime = [InAppUtils getapplicationRunningtime];
    // If user has purchased some package then must check after some time
 //   if(runningTime %3 == 0)
        [self refreshInAppReceipt];
    
    
}



#pragma mark - Refresh in Splash

-(void)refreshInAppReceipt{
    __weak typeof(self) weakSelf = self;
    // Ayaz: Test Code for Auto Renew-able subscriptions
    [[InAppAPI sharedInstance].iapHelper downloadReceiptAndValidateTransaction:nil onCompletion:^(SKPaymentTransaction* transcation, NSString *productIdentifier, NSDate *expiryDate) {
        
        // If expiryDate > currentDate: Show productIdentifier as selected package
        // Else If expiryDate < currentDate: Package expired
        
        if(expiryDate == nil) return;
        NSDate * currentDate = [InAppUtils getDateFromString:[InAppUtils getCurrentDateTime] ];
    //  currentDate =   [currentDate dateByAddingTimeInterval:3600];
        // Package has expired
        if ([expiryDate isEarlierThanDate:currentDate])
     //   if([currentDate timeIntervalSinceDate:expiryDate] > 0 )
        {
            [[InAppUtils sharedInstance] setAccountType:NO_ACCOUNT];
 
        }
        else  {
            [[InAppUtils sharedInstance] setAccountType:[[InAppUtils sharedInstance] verifyProductIdentify:productIdentifier]];
            [ weakSelf purchaseSuccessfully:[[InAppUtils sharedInstance] verifyProductIdentify:productIdentifier] withExpiryDate:[InAppUtils getStringFromDate:expiryDate ]];
        }
    }];
}



-(void)purchaseSuccessfully:(int )purchaseType withExpiryDate:(NSString *)expirydate
{
    [[InAppUtils sharedInstance] setAccountType:purchaseType ];
    if(purchaseType == YEAR_ACCOUNT) {
        // Notify then somethings has been purchased
    }
    if(purchaseType == MONTH_ACCOUNT) {
        // Notify then somethings has been purchased
    }
    
    
}


@end
