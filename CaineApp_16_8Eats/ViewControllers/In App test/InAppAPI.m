//
//  InAppAPI.m
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 30/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import "InAppAPI.h"
#import "IAPShare.h"
#import "Constant.h"

static InAppAPI *sharedInstance;


@implementation InAppAPI

+ (instancetype)sharedInstance {
    static dispatch_once_t DDASLLoggerOnceToken;
    dispatch_once(&DDASLLoggerOnceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

#pragma mark - InAppPurchase Helper API
- (IAPHelper *)iapHelper {
    if (!_iapHelper) {
        if(![IAPShare sharedHelper].iap) {
            NSSet* dataSet = [[NSSet alloc] initWithObjects:
                              kInAppPurchaseYearPackage,
                              kInAppPurchaseMonthlyPackage,
                              kInAppPurchaseYearPackageReduced,
                              kInAppPurchaseMonthlyPackageReduced,
                              nil];
            [IAPShare sharedHelper].iap = [[IAPHelper alloc] initWithProductIdentifiers:dataSet];
 
            [IAPShare sharedHelper].iap.production =IN_APP_IS_PRODUCTION;
        }
        _iapHelper = [IAPShare sharedHelper].iap;
    }
    return _iapHelper;
}



@end
