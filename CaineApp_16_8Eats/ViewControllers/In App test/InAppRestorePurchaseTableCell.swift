//
//  InAppRestorePurchaseTableCell.swift
//  CaineApp_16_8Eats
//
//  Created by Mursaleen Moosa on 12/11/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import SafariServices

let TERMS_OF_SERVICES = "https://100clubapp.com/terms/"
let PRIVACY_POLICY = "https://100clubapp.com/privacy-policy/"


class InAppRestorePurchaseTableCell: UITableViewCell , UITextViewDelegate {

    @IBOutlet weak var textView : UITextView!
    @IBOutlet weak var restorePurchaseButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        restorePurchaseButton.setFont(withName: CLANOT_NEWS, withSize: 18)
        configureTextView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:])
        return false
    }
    
    private func configureTextView() {
        textView.delegate = self
        textView.setFontByScreenSize(fontName: CLANOT_NEWS, fontSize: 13)
        let string = "The total amount for the subscription period will be charged to your iTunes Account. Unless you turn off auto-renewal at least 24 hours before the end of the subscription period, the subscription will renew automatically for the same price, and your iTunes Account will be charged accordingly. You can manage the subscription and turn off automatic renewal in the Account Settings for your Apple ID at any time. Any unused portion of a free trial period will be forfeited when subscribing to a non-trial plan."
        textView.text = string
        textView.textAlignment = .justified
    }
    
   
    
}
