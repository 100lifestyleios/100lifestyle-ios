//
//  InAppPurchaseTableCell.swift
//  CaineApp_16_8Eats
//
//  Created by Mursaleen Moosa on 09/11/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

class InAppPurchaseTableCell: UITableViewCell {

    @IBOutlet weak var bgImageView : UIImageView!
    @IBOutlet weak var trySevenDayFreeButton : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
