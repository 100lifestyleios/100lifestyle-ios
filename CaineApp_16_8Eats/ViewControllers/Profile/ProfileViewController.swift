//
//  ProfileViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 10/31/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Mixpanel
import SDWebImage
import SafariServices
import Branch

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var updatePhotoButton: UIButton!
    
    var user : ParseUser?
    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: "Profile Controller", properties: ["Screen name" : "Profile Controller"])
        userImageView.makeRounded(cornerWithRadius: 196)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        user = ParseUser.current()
        FEUserDefaults.sharedInstance.screenName = "Profile ViewController"
        FEUserDefaults.sharedInstance.startTime = Date()
        updatePhotoButton.isEnabled = true
        setUsersView()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    
    func setUsersView() {
    
        headerView.frame = CGRect(x: UIScreen.main.bounds.origin.x, y: UIScreen.main.bounds.origin.y, width: UIScreen.main.bounds.size.width, height: 396 / 812 * UIScreen.main.bounds.size.height)
        userFullNameLabel.text = user?.fullName
        usernameLabel.text = (user?.isFacebookLogin == false) ? "@\((user?.username)!)" : ""
       
        if user?.profilePhoto != nil {
            userImageView.sd_setImage(with: URL(string: (user?.profilePhoto?.url)!), placeholderImage: #imageLiteral(resourceName: "PlaceHolderImage"), options: SDWebImageOptions.refreshCached, completed:nil)
        }
        else {
            userImageView.image = #imageLiteral(resourceName: "PlaceHolderImage")
        }
        tableView.reloadData()
    }
    
    deinit {
        print("\(self) deinit called")
    }
    
    
    // MARK: - IBActions

    @IBAction func updateUserPhotoClicked(_ sender: UIButton) {
        sender.isEnabled = false
        let controller = UIViewController.AddPhotoViewController() as! AddPhotoViewController
        if user?.profilePhoto != nil {
            controller.userImage = userImageView.image
        }
        present(controller, animated: false, completion: nil)
    }
    
    
    //MARK - UITableViewDelegate and Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 / 812 * UIScreen.main.bounds.size.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = ProfileCell()
        cell = tableView.dequeueReusableCell(withIdentifier: PROFILECELL) as! ProfileCell
        if indexPath.row == 0 {
            cell.configureWorkoutCell(withTitle: "Workout Reminder", withIcon: #imageLiteral(resourceName: "workoutIcon"), withWorkoutTime: user?.workoutTime!)
        }
        else if indexPath.row == 1 {
            cell.configureCell(withTitle: "Invite Friends", withIcon:#imageLiteral(resourceName: "Share"))
        }
        else if indexPath.row == 2 {
            cell.configureCell(withTitle: "Contact Us", withIcon:#imageLiteral(resourceName: "CustomerSupportIcon"))
        }
            
        else if indexPath.row == 3 {
            cell.configureCell(withTitle: "About 100 Lifestyle", withIcon:#imageLiteral(resourceName: "AboutIcon"))
        }
            
        else if indexPath.row == 4 {
            cell.configureCell(withTitle: "Sign out", withIcon:#imageLiteral(resourceName: "LogoutIcon"))
        }
        
        return cell; //change it accordingly
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
            //Change work out time controller
            let controller = UIViewController.AddWorkoutTimerViewController() as! AddWorkoutTimerViewController
            controller.isFromProfile = true
            present(controller, animated: false, completion: nil)
        }
        
        if indexPath.row == 1 {
            //Invite Friends
            showShareSheet()
        }
        
        if indexPath.row == 2 {
            //Contact Us
            let url = URL(string: CONTACT_US)
            openSafariViewControllerWithURL(url: url!)
        }
        
        if indexPath.row == 3 {
            let url = URL(string: ABOUT_100LIFESTYLE)
            openSafariViewControllerWithURL(url: url!)
        }
       
        if indexPath.row == 4 {
            showLogoutAlert(withTitle: "\nLog Out", withMessage: "\nAre you sure you want to log out?\n")
        }
    }
    func logout(){
        ParseUser.logOutInBackground { (error) in
            if error != nil {
                print(error?.localizedDescription ?? "Logout Failed!")
            }
            else {
                LSUserDefaults.sharedInstance.removeAllData()
                let controller = UIViewController.WelcomeViewController() as! WelcomeViewController
                UIApplication.shared.keyWindow?.rootViewController = controller
            }
        }
    }
    
    func showLogoutAlert(withTitle title: String, withMessage message: String) {
        
        let alert = AOAlertController.init(title: title, message: message, style: .actionSheet)
        alert.configureFontAndColor()
        
        let okAction = AOAlertAction.init(title: "Logout", style: .default, handler: { [weak self] in
            self?.logout()
        })
        
        okAction.configureDefaultAction()
        
        let cancelAction = AOAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        cancelAction.configureCancelAction()
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Tabbar")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("\(self) viewDidDisappear")
    }
    
    private func showShareSheet() {
        
        let userFullName = ParseUser.current()?.fullName?.capitalized
        let message = userFullName! + INVITE_MESSAGE
        let branchLinkProperty = createBranchLinkProperties()
        
        let buo = BranchUniversalObject(canonicalIdentifier: message)
        buo.locallyIndex = true
        buo.publiclyIndex = true
        
        buo.showShareSheet(with: branchLinkProperty, andShareText: message, from: self) { (activityType, completed) in
            if completed {
                print("\(activityType ?? "No ActivityType") : \(completed)")
            }
        }
    }
    
    private func createBranchLinkProperties() -> BranchLinkProperties {
        let lp: BranchLinkProperties = BranchLinkProperties()
        lp.channel = "facebook"
        lp.feature = "sharing"
        lp.campaign = "content 123 launch"
        lp.stage = "new user"
        lp.tags = ["one", "two", "three"]
        lp.addControlParam("nav_to", withValue: "referral_code_share")
        return lp
    }
    
    func openSafariViewControllerWithURL(url : URL) {
        
        let sfvc = SFSafariViewController(url: url)
        present(sfvc, animated: true)
        //        } else {
        //            UIApplication.shared.openURL(url)
        //        }
    }
}
