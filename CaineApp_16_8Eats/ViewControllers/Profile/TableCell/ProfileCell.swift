//
//  ProfileCell.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 10/31/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var workoutTitleLabel: UILabel!
    @IBOutlet weak var profileCellIcon: UIImageView!
    @IBOutlet weak var profileCellButton: UIButton!
    @IBOutlet weak var workoutLabel: UILabel!
    @IBOutlet weak var workoutStackView: UIStackView!
    @IBOutlet weak var descriptionStackView: UIStackView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.setFontSize(withName:CLANOT_NEWS, fontSize: 16)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(withTitle title: String, withIcon icon: UIImage) {
        titleLabel.text = title
        profileCellIcon.image = icon
        
    }
    
    func configureWorkoutCell(withTitle title: String, withIcon icon: UIImage, withWorkoutTime time : Date? = nil) {
        workoutTitleLabel.text = title
        profileCellIcon.image = icon
        if time != nil {
            workoutLabel.text = getTime(byDate: time!)
        }
        
    }
}
