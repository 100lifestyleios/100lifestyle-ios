//
//  WorkoutCollectionViewCell.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 11/3/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

class WorkoutCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var workoutImageView: UIImageView!
    @IBOutlet weak var videoPlayerView : VideoPlayerView!
    
    func setWorkOutGif(gifURL videoURL : String) {
        let userImageURL  = videoURL.videoToGIFURL(withWidth: Int(workoutImageView.frame.size.width), andHeight: Int(workoutImageView.frame.size.height))
        
        workoutImageView.image =  nil
        workoutImageView.setGifImageView(url: userImageURL) { (image, url) in
            print("URL is \(url)")
        }
    }
}
