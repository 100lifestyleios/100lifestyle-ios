//
//  ViewController.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 11/04/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import WebKit
class MyWebViewController: UIViewController, WKUIDelegate {
    
    var webView: WKWebView!
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
        
        super.viewDidLoad()
        
        let fileURL = URL(fileURLWithPath: Bundle.main.path(forResource:"Caine’s Story", ofType: "html")!)
        webView?.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
        webView?.scrollView.isScrollEnabled = false
        
    }
    
//    func creatAndAttachWebView() {
//        let webConfiguration = WKWebViewConfiguration()
//        webView = WKWebView(frame: self.view.frame, configuration: webConfiguration)
//    }

}
