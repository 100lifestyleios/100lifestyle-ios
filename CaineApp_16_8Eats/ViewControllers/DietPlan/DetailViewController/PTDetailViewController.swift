//
//  PTDetailViewController.swift
//
// Copyright (c)  21/12/15. Ramotion Inc. (http://ramotion.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import WebKit
import Hero


/// Base UIViewController for preview transition
open class PTDetailViewController: UITableViewController {

    var tableViewHeight : CGFloat = 100
    var cellAlpha : CGFloat = 0.0
    var isLoaded = false
    let screenHeight = UIScreen.main.bounds.height
    var shouldAnimateFirstRow = false
    var isHtmlLoading = true
  var bgImage: UIImage?
  var titleText: String?
  var stretchyHeaderView:GSKSpotyLikeHeaderView?
  
  fileprivate var backgroundImageView: UIImageView?
    
    var tableRow = 0
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}

// MARK: life cicle

extension  PTDetailViewController {
  
  open override func viewDidLoad() {
    super.viewDidLoad()
    
//    backgroundImageView = createBackgroundImage(bgImage)
    view.backgroundColor = .white
    tableView.estimatedRowHeight = 100.0
    
    tableView.tableFooterView = UIView();
    
    self.automaticallyAdjustsScrollViewInsets = false;
    var contentInset = self.tableView.contentInset;
    if ((self.navigationController) != nil) {
      contentInset.top = 64;
    }
    if ((self.tabBarController) != nil) {
      contentInset.bottom = 44;
    }
    self.tableView.contentInset = contentInset;
    self.tableView.separatorStyle = .none
    
    stretchyHeaderView = GSKSpotyLikeHeaderView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 100))
    stretchyHeaderView?.titleText = self.titleText
    
    self.navigationController?.hero.isEnabled = true
    
//    stretchyHeaderView?.hero.id = "image"
    
    if(screenHeight >= 812){ //iPhoneX
        stretchyHeaderView?.minimumContentHeight = 100
    }
    
//    self.view.hero.modifiers = [.fade, .translate(y:20)]
//    self.stretchyHeaderView?.hero.modifiers = [.scale(0.6), .fade]
    stretchyHeaderView?.image = self.bgImage
    stretchyHeaderView!.hero.id = "image_\(tableRow)"
    self.tableView.addSubview(stretchyHeaderView!)
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "showNavigationTitle"), object: nil, queue: OperationQueue.main) {[weak self] (notification:Notification) in
      
      // Animate navigation title
      if let titleText = self?.titleText {
        let fadeTextAnimation = CATransition()
        fadeTextAnimation.duration = 0.5
        fadeTextAnimation.type = kCATransitionFade
        self?.navigationController?.navigationBar.layer.add(fadeTextAnimation, forKey: "fadeText")
        self?.navigationItem.title = titleText
      }
      
        if(self?.stretchyHeaderView != nil){
            // Add blur effect
            let bounds = self?.stretchyHeaderView?.bounds as CGRect!
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
            visualEffectView.frame = bounds!
            visualEffectView.tag = 10010
            visualEffectView.alpha = 0.8
            visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self?.stretchyHeaderView?.addSubview(visualEffectView)
        }
        
    }
    NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "hideNavigationTitle"), object: nil, queue: OperationQueue.main) {[weak self] (notification:Notification) in
      
      // Animate navigation title
      let fadeTextAnimation = CATransition()
      fadeTextAnimation.duration = 0.5
      fadeTextAnimation.type = kCATransitionFade
      self?.navigationController?.navigationBar.layer.add(fadeTextAnimation, forKey: "fadeText")
      self?.navigationItem.title = ""
      
      // Remove blur effect
      self?.stretchyHeaderView?.viewWithTag(10010)?.removeFromSuperview()
    }
    
    // hack 
    if let navigationController = self.navigationController {
      for case let label as UILabel in navigationController.view.subviews {
        label.isHidden = true
      }
    }
    
    let _ = createNavBar(UIColor(red: 0, green: 0, blue: 0, alpha: 0.0))
  }
  
  open override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.navigationController?.gsk_setNavigationBarTransparent(true, animated: false)
    self.navigationController?.navigationBar.barStyle = .black
    self.navigationController?.navigationBar.tintColor = UIColor.white
  }
    
    open override func viewWillDisappear(_ animated: Bool) {
        
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ContentTableViewCell
        if(cell != nil){
            cell?.webView?.navigationDelegate = nil
            cell?.webView?.uiDelegate = nil
            tableView.reloadData()
        }
        
        super.viewWillDisappear(animated)
        
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
}

// MARK: public

extension PTDetailViewController {
  
  /**
   Pops the top view controller from the navigation stack and updates the display with custom animation.
   */
  public func popViewController() {
    
//    if let navigationController = self.navigationController {
//      for case let label as UILabel in navigationController.view.subviews {
//        label.isHidden = false
//      }
//    }

//    self.navigationController?.hero.isEnabled = true
//    self.navigationController?.hero.navigationAnimationType = .none
//    self.navigationController?.popViewController(animated: true)
    
    self.navigationController?.hero.isEnabled = true
    self.navigationController?.view.hero.modifiers = [.fade]
    self.navigationController?.dismiss(animated: true)
    
  }
}

// MARK: create

extension PTDetailViewController {
  
  fileprivate func createBackgroundImage(_ image: UIImage?) -> UIImageView {
    // add constraint closures
    let addConstraint: (_ imageView: UIImageView, _ toView: UIView, _ attribute: NSLayoutAttribute) -> () = {
      (imageView, toView, attribute) in
      let constraint = NSLayoutConstraint(item: imageView,
        attribute: attribute,
        relatedBy: .equal,
        toItem: toView,
        attribute: attribute,
        multiplier: 1,
        constant: 0)
      toView.addConstraint(constraint)
    }
    
    // crate imageView
    let imageView = UIImageView(frame: CGRect.zero)
    imageView.image = image
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .center
    view.insertSubview(imageView, at: 0)
    
    // added constraints
    for attribute in [NSLayoutAttribute.leading, NSLayoutAttribute.trailing, NSLayoutAttribute.top, NSLayoutAttribute.bottom] {
      addConstraint(imageView, view, attribute)
    }
    
    return imageView
  }
  
  fileprivate func createNavBar(_ color: UIColor) -> UIView {
    let navBar = UIView(frame: CGRect.zero)
    navBar.backgroundColor = color
    navBar.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(navBar)
    
    for attributes: NSLayoutAttribute in [.left, .right, .top] {
      (view, navBar) >>>- {
        $0.attribute = attributes
        return
      }
    }
    navBar >>>- {
      $0.attribute = .height
      $0.constant = 64
      return
    }
    
    return navBar
  }

}

// MARK: UITableViewDataSource / UITableViewDelegate
extension PTDetailViewController : WKNavigationDelegate {
  open override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
    
    open override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let contentCell = cell as! ContentTableViewCell
        
        if shouldAnimateFirstRow {
            UIView.animate(withDuration: 0.1, animations: {
                contentCell.webView?.alpha = 1.0
            }, completion: { [weak self] (result) in
                self?.cellAlpha = 1.0
                self?.shouldAnimateFirstRow = false
            })
        }
        
        if isHtmlLoading {
            contentCell.progressSpinner.isHidden = false
            contentCell.progressSpinner.beginRefreshing()
        }
        else {
            contentCell.progressSpinner.isHidden = true
            contentCell.progressSpinner.endRefreshing()
        }
        
    }
    
  open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ContentTableViewCell
    cell.tableViewHeight = tableViewHeight
    cell.webView?.alpha = 0.0
//    cell.lblTitle.text = self.titleText
//    cell.lblTitle.frame.size = .zero
//    cell.lblTitle.isHidden = true
   
    print("titleText: \(String(describing: self.titleText))")
    
    var fileName = self.titleText!
    
    if(self.titleText!.contains(":")){
        let index = self.titleText?.index(of: ":")?.encodedOffset
        print("index: \(String(describing: index))")
        fileName  = replace(myString: self.titleText!, index!, "_")
        print("fileName: \(String(describing: fileName))")
    }
    
//    cell.webView.alpha = cellAlpha
    
    print("tableViewHeight: \(tableViewHeight), cellAlpha: \(cellAlpha)")
    
    
//    if(tableViewHeight > 100 && cellAlpha <= 0.0){
//        cell.contentView.alpha = 0.0
//        UIView.animate(withDuration: 10, animations: { [weak self] in
//            cell.contentView.alpha = 1.0
//            self?.cellAlpha = 1.0
//            //self?.tableView.reloadData()
//        })
//
//        UIView.animate(withDuration: 10, animations: {
//            cell.contentView.alpha = 1.0
//        }, completion: { (result) in
////            tableView.reloadData()
//        })
//    }
    
    
    
    //--------THE HTML Way
//    let html = Bundle.main.url(forResource: "Test", withExtension: "html")
//
//    //print("html: \(html)")
//
//    let data = try? Data(contentsOf: html!)
//    let attributedStr = try? NSAttributedString(data: data!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
    
    //------------------------
    
    //print("attributedStr: \(attributedStr)")
    
    
    //--------THE Markdown Way
    
//    let md = SwiftyMarkdown(string: "# Heading\nMy *Markdown* string")
//    let attributedStr = md.attributedString()
    
//    if let url = Bundle.main.url(forResource: "Test", withExtension: "md"), let md = SwiftyMarkdown(url: url ) {
//        let attributedStr = md.attributedString()
//
//
//    }
    
    
    //--------------------
    
    
    //let data =
    
    let fileURL = URL(fileURLWithPath: Bundle.main.path(forResource:fileName, ofType: "html")!)
    cell.webView?.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
    cell.webView?.navigationDelegate = self
//    cell.webView?.scrollView.isScrollEnabled = false
    
//    var attributedStr = NSAttributedString(data: html, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
//    cell.lblDescription.text =
    
    
//    cell.lblDescription.attributedText = attributedStr
//    cell.lblDescription.sizeToFit()
//    cell.lblDescription.numberOfLines = 0
    
    
            //    cell.lblDescription.numberOfLines = 2
    //        //    cell.lblDescription.sizeToFit()
    
    
    return cell
  }
    
//    func scrollViewDidScroll(scrollView: UIScrollView) {
//        if let tableView = scrollView as? UITableView {
//            for cell in tableView.visibleCells {
//                guard let cell = cell as? ContentTableViewCell else { continue }
//                cell.webView?.setNeedsLayout()
//            }
//        }
//    }
    
    open override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let tableView = scrollView as? UITableView {
            for cell in tableView.visibleCells {
                guard let cell = cell as? ContentTableViewCell else { continue }
                cell.webView?.setNeedsLayout()
            }
        }
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let url = navigationAction.request.url,
                let host = url.host, !host.hasPrefix("www.google.com"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    // Fallback on earlier versions
                }
                print(url)
                print("Redirected to browser. No need to open it locally")
                decisionHandler(.cancel)
            } else {
                print("Open it locally")
                decisionHandler(.allow)
            }
        } else {
            print("not a user click")
            decisionHandler(.allow)
        }
    }
    
  open override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//    return UITableViewAutomaticDimension
    return tableViewHeight
  }
    
    func replace(myString: String, _ index: Int, _ newChar: Character) -> String {
        var chars = Array(myString.characters)     // gets an array of characters
        chars[index] = newChar
        let modifiedString = String(chars)
        return modifiedString
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        let userDefaults = UserDefaults.standard
        let articleSavedHeight = userDefaults.double(forKey: self.titleText!)
        
        if(articleSavedHeight <= 100){
            
            if webView.isLoading == false && !isLoaded {
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { [weak self] (result, error) in
                    if let height = result as? CGFloat {
                        print("height: \(height)")
                        //                    webView.frame.size.height += height
                        webView.frame.size.height = height
                        webView.scrollView.isScrollEnabled = false
                        self?.tableViewHeight =  height
                        self?.isLoaded = true
                        self?.shouldAnimateFirstRow = true
                        self?.isHtmlLoading = false
                        
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {[weak self] in
                            self?.tableView.reloadData()
                        })
                        
                        userDefaults.set(Double(height), forKey: (self?.titleText)!)
                        
                    }
                })
            }
            
        }
        else {
            
            if webView.isLoading == false && !isLoaded {
                webView.frame.size.height = CGFloat(articleSavedHeight)
                webView.scrollView.isScrollEnabled = false
                self.tableViewHeight =  CGFloat(articleSavedHeight)
                self.isLoaded = true
                self.shouldAnimateFirstRow = true
                self.isHtmlLoading = false
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {[weak self] in
                    self?.tableView.reloadData()
                })
            }
            
            
        }
        
    }
    
    /*
    if let htmlURL = NSBundle.mainBundle().URLForResource("page", withExtension: "html"),
    data = NSData(contentsOfURL: htmlURL) {
        do {
            let attrStr = try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType], documentAttributes: nil)
            print(attrStr)
        }
        catch {
            print("error creating attributed string")
        }
    }
    */
    
//    func convertHtml() -> NSAttributedString{
//        guard let data = data(using: .utf8) else { return NSAttributedString() }
//        do{
//            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
//        }catch{
//            return NSAttributedString()
//        }
//    }
}
