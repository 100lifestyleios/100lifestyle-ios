//
//  MainBootCampViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 11/1/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit

class MainBootCampViewController: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    let inviteFriendController = UIViewController.InviteFriendsViewController()
    let bootcampController = UIViewController.EntryPassViewController()
    let isBootCampAvailable = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        removeController(inviteFriendController)
        removeController(bootcampController)
        
        if  isBootCampAvailable == false {
            addChildController(withViewController: inviteFriendController)
        }
        else if ParseUser.current() != nil {
            fetchBootCampData()
        }
    }
    
    func fetchBootCampData() {
        addChildController(withViewController: bootcampController)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func addChildController(withViewController controller: UIViewController) {
        addChildViewController(controller)
        controller.view.frame = view.frame
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFromBottom
        view.layer.add(transition, forKey: nil)
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    
    func removeController(_ controller : UIViewController) {
        if controller.view != nil {
            controller.willMove(toParentViewController: nil)
            controller.view.removeFromSuperview()
            controller.removeFromParentViewController()
        }
    }

}
