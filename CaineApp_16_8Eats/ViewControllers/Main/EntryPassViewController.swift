//
//  EntryPassViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 10/25/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Mixpanel


class EntryPassViewController: UIViewController {
    
    @IBOutlet weak var EntryPassQRCode: UIImageView!
    
    var backButton: UIButton?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: "Entry Pass View", properties: ["Screen name" : "Entry Pass View"])

        createQRCode()
        self.navigationController?.gsk_setNavigationBarTransparent(true, animated: false)
        hideLockElement()
        FEUserDefaults.sharedInstance.screenName = "EntryPass ViewController"
        FEUserDefaults.sharedInstance.startTime = Date()
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkInAppStatus()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Tabbar")
    }
    
    func createQRCode() {
        
        EntryPassQRCode.image = {
            var qrCode = QRCode("Link Here")!
            qrCode.size = self.EntryPassQRCode.bounds.size
            qrCode.color = CIColor(rgba: "4F4F4F")
            qrCode.errorCorrection = .High
            return qrCode.image
        }()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func checkInAppStatus(){
        
        guard let currentUser = ParseUser.current() else { return }
        
        if  currentUser.purchaseStatus == nil || (currentUser.purchaseStatus?.int32Value)! <= NO_ACCOUNT {
            hideLockElement()
            self.presentAlert(title: "In App Purchase", message: "Do you want to subscribe for Boot Camp!", yesHandler: { [weak self ]_ in
                self?.navigationController?.pushViewController(InAppPurchaseController(), animated: true)

            }) { _ in
                print("No print")
            }
            
        } else {
        showLockElement()
        }
    }
    
    func subscribeForBootCamp(){
        self.view.showSpinner()
        InAppFactory.sharedInstance().monthlySubscription {[weak self] (error) in
            self?.view.hideSpinner()
            if error != nil {
                showToast(withMessage: (error?.localizedDescription)!)
            } else {
                self?.showLockElement()
            }
        }
    }
    
    
    //MARK: Show Hide UIView on App
    func showLockElement(){
       EntryPassQRCode.isHidden =  false
    }
    
    func hideLockElement(){
        EntryPassQRCode.isHidden =  true

    }
    
    func showPurchaseAlert() {
        
        let alert = AOAlertController.init(title: "InApp", message: "Do you want to unlock bottcamp", style: .actionSheet)
        alert.configureFontAndColor()
        
        let okAction = AOAlertAction.init(title: "Yes", style: .default, handler: { [weak self] in
            self?.navigationController?.pushViewController(UIViewController.InAppPurchaseController(), animated: true)
         })
        
        okAction.configureDefaultAction()
        
        let cancelAction = AOAlertAction.init(title: "No", style: .cancel, handler: nil)
        cancelAction.configureCancelAction()
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
}
