//
//  ViewController.m
//  Hush
//
//  Created by isystematic on 5/3/17.
//  Copyright © 2017 isystematic. All rights reserved.
//

#import "SplashViewController.h"
#import "UIViewController+Additions.h"

#import "HyTransitions.h"
#import "HyLoginButton.h"

@interface SplashViewController ()<UIViewControllerTransitioningDelegate>

@property (weak, nonatomic) IBOutlet HyLoginButton *btnAnimation;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    self.btnAnimation.layer.cornerRadius = CGRectGetHeight(self.btnAnimation.bounds)/2;
    self.btnAnimation.clipsToBounds = YES;
    [self.btnAnimation startLoading];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf.btnAnimation succeedAnimationWithCompletion:^{
            [weakSelf didPresentControllerButtonTouch];
        }];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIViewControllerTransitioningDelegate
- (void)didPresentControllerButtonTouch {
    UIViewController *controller  = [UIViewController articlesListNavController];
    controller.transitioningDelegate = self;
    [self presentViewController:controller animated:YES completion:nil];
    
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    return [[HyTransitions alloc]initWithTransitionDuration:0.4f StartingAlpha:0.5f isPush:true];
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return [[HyTransitions alloc]initWithTransitionDuration:0.4f StartingAlpha:0.8f isPush:false];
}

@end
