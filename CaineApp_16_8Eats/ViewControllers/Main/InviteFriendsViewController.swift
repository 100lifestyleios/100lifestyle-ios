//
//  InviteFriendsViewController.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 11/1/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import Mixpanel
import Branch

class InviteFriendsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: "Invite Friend Screen", properties: ["Screen name" : "Invite Friend Screen"])
        FEUserDefaults.sharedInstance.screenName = "Invite Friends ViewController"
        FEUserDefaults.sharedInstance.startTime = Date()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func invtiteFriends(_ sender: UIButton) {
        showShareSheet()
        //Branch IO code here
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        API.sharedInstance.logApplicationScreenTimeEvent(movedToScreen: "Tabbar")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        checkInAppStatus()
        
//        let videoURL  = "http://res.cloudinary.com/isystematic/video/upload/v1541232824/workout_video/day_7/video_1.mov"
//        let userImageURL  = videoURL.videoToGIFURL(withWidth: Int(maoImageView.frame.size.width), andHeight: Int(maoImageView.frame.size.height))
//        maoImageView.image =  nil
//        
//        maoImageView.setGifImageView(url: userImageURL) { (image, url) in
//            print("URL is \(url)")
//        }
    }
    

    func checkInAppStatus(){
        
        guard let currentUser = ParseUser.current() else { return }
        
        if  currentUser.purchaseStatus == nil || (currentUser.purchaseStatus?.int32Value)! <= NO_ACCOUNT {
            hideLockElement()
            self.presentAlert(title: "In App Purchase", message: "Do you want to subscribe for Boot Camp!", yesHandler: { [weak self ]_ in
                self?.present(UIViewController.InAppPurchaseController(), animated: true, completion: nil)
               // self?.navigationController?.pushViewController(UIViewController.InAppPurchaseController(), animated: true)
                
            }) { _ in
                print("No print")
            }
            
        } else {
            showLockElement()
        }
    }
    
    private func showShareSheet() {
        
        let userFullName = ParseUser.current()?.fullName?.capitalized
        let message = userFullName! + INVITE_MESSAGE
        let branchLinkProperty = createBranchLinkProperties()
        
        let buo = BranchUniversalObject(canonicalIdentifier: message)
        buo.locallyIndex = true
        buo.publiclyIndex = true
        
        buo.showShareSheet(with: branchLinkProperty, andShareText: message, from: self) { (activityType, completed) in
            if completed {
                print("\(activityType ?? "No ActivityType") : \(completed)")
            }
        }
    }
    
    private func createBranchLinkProperties() -> BranchLinkProperties {
        let lp: BranchLinkProperties = BranchLinkProperties()
        lp.channel = "facebook"
        lp.feature = "sharing"
        lp.campaign = "content 123 launch"
        lp.stage = "new user"
        lp.tags = ["one", "two", "three"]
        lp.addControlParam("nav_to", withValue: "referral_code_share")
        return lp
    }
    
    //MARK: Show Hide UIView on App
    func showLockElement(){
      //  EntryPassQRCode.isHidden =  false
    }
    func hideLockElement(){
       // EntryPassQRCode.isHidden =  true
        
    }
    
}
