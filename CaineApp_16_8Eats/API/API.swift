//
//  API.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 02/11/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class API: NSObject {
    
    let  manager  : SDWebImageManager =  SDWebImageManager.shared()
    
    
    class var sharedInstance :API {
        struct Singleton {
            static let instance = API()
        }
        
        return Singleton.instance
    }
    
}
