//
//  AppDelegate+Extensions.swift
//  Quixly
//
//  Created by clines192 on 03/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import CoreLocation

extension Notification.Name {
    static let CurrentUserLocationUpdated = NSNotification.Name("Notification_CurrentUserLocationUpdated")
}
class Notification_ParameterNames {
    static let CurrentLocation = NSNotification.Name("location")
}

// MARK: - Implementaion - LocationPermissionHelper
extension API: LocationPermissionHelper, CLLocationManagerDelegate {
    static var locationManager: CLLocationManager = CLLocationManager()
    
    // MARK: Implementaion - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            stopUpdatingLocation()
            startMonitoringSignificantLocationChanges()
        } else if status == .authorizedWhenInUse {
            stopMonitoringSignificantLocationChanges()
            startUpdatingLocation()
        } else {
            stopMonitoringSignificantLocationChanges()
            stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation: CLLocation = locations.last!
        API.sharedInstance.currentLocation = currentLocation
        NotificationCenter.default.post(Notification(name: Notification.Name.CurrentUserLocationUpdated, object: nil, userInfo: [Notification_ParameterNames.CurrentLocation : currentLocation]))
    }
}
