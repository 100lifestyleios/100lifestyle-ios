//
//  API+ParseModels.swift
//  Quixly
//
//  Created by clines192 on 13/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Parse
import CoreLocation

/// Quixly - Global variables
extension API {
    /// User's most recent location
    var currentLocation: CLLocation? {
        get {
            if let locationDictionary = UserDefaults.standard.object(forKey: "API_Current_Location") as? Dictionary<String,NSNumber> {
                let locationLat = locationDictionary["lat"]!.doubleValue
                let locationLon = locationDictionary["lon"]!.doubleValue
                return CLLocation(latitude: locationLat, longitude: locationLon)
            }
            return nil
        }
        set {
            // Update user's location in UserDefaults
            let defaults = UserDefaults.standard
            if let location = newValue {
                let locationLat = NSNumber(value:location.coordinate.latitude)
                let locationLon = NSNumber(value:location.coordinate.longitude)
                defaults.set(["lat": locationLat, "lon": locationLon], forKey: "API_Current_Location")
                defaults.synchronize()
            } else {
                defaults.removeObject(forKey: "API_Current_Location")
                defaults.synchronize()
            }
            
            // Update user's location in Parse
            let user = ParseUser.current()
            if let location = newValue {
                user?.geoLocation = PFGeoPoint(location: location)
                user?.saveInBackground()
            }
        }
    }
    
     
}
