//
//  CloudinaryHelper.swift
//  ChallengeApp
//
//  Created by Afnan on 08/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import Cloudinary
import Parse
import SDWebImage



class CloudinaryHelper {
    
    var cloudinary : CLDCloudinary?
    let  manager  : SDWebImageManager =  SDWebImageManager.shared()
    
    
    class var sharedInstance : CloudinaryHelper {
        struct Singleton {
            static let instance = CloudinaryHelper()
            
            
            
        }
        return Singleton.instance
    }
    
    private func getClodinaryObject () -> CLDCloudinary{
        
        if cloudinary == nil  {
            let config = CLDConfiguration(cloudName: CLOUDINARY_NAME, secure: true)
            cloudinary = CLDCloudinary(configuration: config)
        }
        return cloudinary!
    }
    
    lazy var cld : CLDCloudinary?  = self.getClodinaryObject()
    
    
    
  
    
    //MARK: Upload Cloudinary
    func uploadImageOnCloudinary(withImage image : UIImage , andImageName imageName : String ) -> BFTask<AnyObject>{
        
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        
        let data = UIImagePNGRepresentation(image)
        
        CloudinaryHelper.sharedInstance.cld?.createUploader().upload(data: data!, uploadPreset: CLOUDINARY_PRESET, params: getImageParams(fromName: imageName), progress: { (progress)  in
            
        }, completionHandler: { (response, error) in
            if error != nil {
                taskSource.set(error: error!)
            } else {
                taskSource.set(result: response?.url as AnyObject)
            }
        })
        return taskSource.task
        
    }
    
    func uploadImageOnCloudinary(withImage image : UIImage , andImageName imageName : String  , onFolder folderName : String?  ,  block: @escaping ( _ : String?) -> Void) -> BFTask<AnyObject>{
        
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        let data = UIImagePNGRepresentation(image)
        
        let imageCompleteName =  ( folderName == nil ||  folderName?.isStringEmpty ==  true) ? imageName :  "\(folderName!)/\(imageName)"
        
        let url = CloudinaryHelper.sharedInstance.cld?.createUrl() .setTransformation(CLDTransformation()).generate(imageCompleteName)
        print("Final Image Name : \(url!)")
        
        let sdImage  = "\(url!.cleanImageUrlForSDWebImage()).png"
        manager.imageCache?.store(UIImage(data: data!), forKey: manager.cacheKey(for: URL(string: sdImage)), toDisk: true, completion: {
            print("String is \(sdImage)")
            if block != nil{
                block(sdImage)
            }
            
        })
        
        
        CloudinaryHelper.sharedInstance.cld?.createUploader().upload(data: data!, uploadPreset: CLOUDINARY_PRESET, params: self.getImageParams(fromName: imageCompleteName), progress: { (progress)  in
            
        }, completionHandler: {[weak self] (response, error) in
            print("after upload Image Name : \(url!)")
            
            if error != nil {
                self?.manager.imageCache?.removeImage(forKey: self?.manager.cacheKey(for: URL(string: sdImage)), withCompletion: {
                    taskSource.set( error: error! )
                })
            } else {
                taskSource.set(result: response?.url?.cleanImageUrlForSDWebImage() as AnyObject)
            }
        })
        
        return taskSource.task
    }
    
    
    func uploadVideoeOnCloudinary(withVideo videoData : Data , andVideoName videoName : String ) -> BFTask<AnyObject>{
        
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        
        CloudinaryHelper.sharedInstance.cld?.createUploader().upload(data: videoData, uploadPreset: CLOUDINARY_PRESET, params: getVideoParams(fromName: videoName), progress: { (progress)  in
            
        }, completionHandler: { (response, error) in
            if error != nil {
                taskSource.set(error: error!)
            } else {
                
                taskSource.set(result: response?.url as AnyObject)
            }
        })
        return taskSource.task
        
    }
    
    func uploadVideoeOnCloudinary(withVideo videoData : Data , andVideoName videoName : String , onFolder folderName : String?) -> BFTask<AnyObject>{
        
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        
        let videoCompleteName =  ( folderName == nil ||  folderName?.isStringEmpty ==  true) ? videoName :  "\(folderName)/\(videoName)"
        
        
        CloudinaryHelper.sharedInstance.cld?.createUploader().upload(data: videoData, uploadPreset: CLOUDINARY_PRESET, params: getVideoParams(fromName: videoCompleteName), progress: { (progress)  in
            
        }, completionHandler: { (response, error) in
            if error != nil {
                taskSource.set(error: error!)
            } else {
                taskSource.set(result: response?.url as AnyObject)
            }
        })
        return taskSource.task
        
    }
    
    
    //MARK: Get Params
    
    func getImageParams(fromName fileName : String ) -> CLDUploadRequestParams{
        let params = CLDUploadRequestParams()
        params.setPublicId(fileName)
        params.setResourceType(.image)
        let transformation = CLDTransformation().setWidth(90).setHeight(90)
        
        // params.setTransformation(transformation)
        return params
        
    }
    
    func getVideoParams(fromName fileName : String ) -> CLDUploadRequestParams{
        let params = CLDUploadRequestParams()
        params.setPublicId(fileName)
        params.setResourceType(.video)
        return params
        
    }
    
}

