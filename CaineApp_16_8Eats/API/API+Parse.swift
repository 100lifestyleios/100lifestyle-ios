//
//  API+Parse.swift
//  100 LifeStyle
//
//  Created by isystematic on 19/05/2018.
//  Copyright © 2018 isystematic. All rights reserved.
//

import UIKit
import Parse

extension API {
    
    func workoutQuery() -> PFQuery<PFObject> {
        return PFQuery(className: KWorkOutTableName)
    }
    
    func getAllWorkoutsFromParse() -> BFTask<AnyObject> {
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        let query = workoutQuery()
        query.whereKey(WORKOUT_NAME, notEqualTo: "Day1")
        query.whereKey(IS_PUBLISHED, equalTo: true)
        query.whereKey(IS_DELETED, equalTo: false)
        query.order(byAscending: PUBLISH_DATE)
        query.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                taskSource.set(error: error!)
            } else {
                taskSource.set(result: objects as AnyObject)
            }
        })
        return taskSource.task
    }
    
    func getNewWorkoutsFromParse() -> BFTask<AnyObject> {
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        let query = workoutQuery()
        query.whereKey(WORKOUT_NAME, notEqualTo: "Day1")
        query.whereKey(IS_PUBLISHED, equalTo: true)
        query.whereKey(IS_DELETED, equalTo: false)
        query.whereKey(PUBLISH_DATE, greaterThan: WorkOutDefaults.sharedInstance.lastWorkoutPublishedOn!)
        query.order(byAscending: PUBLISH_DATE)
        query.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                taskSource.set(error: error!)
            } else {
                taskSource.set(result: objects as AnyObject)
            }
        })
        return taskSource.task
    }

    
    func getNumberOfWorkouts() -> BFTask<AnyObject> {
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        let query = workoutQuery()
        query.whereKey(IS_PUBLISHED, equalTo: true)
        query.whereKey(IS_DELETED, equalTo: false)
        query.countObjectsInBackground { (count, error) in
            if error != nil {
                taskSource.set(error: error!)
            } else {
                taskSource.set(result: count as AnyObject)
            }
        }
        return taskSource.task
    }
}
