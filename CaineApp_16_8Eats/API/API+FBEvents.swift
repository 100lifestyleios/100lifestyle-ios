//
//  API+FBEvents.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 1/14/19.
//  Copyright © 2019 iSystematic LLC. All rights reserved.
//

import Foundation
import FBSDKCoreKit

extension API {
    
    func logApplicationTerminateEvent() {
        let screenName = FEUserDefaults.sharedInstance.screenName
        let timeOnScreen = calculateScreenTime()
        let params = [
            "screenName" : screenName!,
            "timeOnScreen in Seconds" : timeOnScreen
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Application Terminate", parameters: params)
    }
    
    func logApplicationScreenTimeEvent(movedToScreen : String) {
        let screenName = FEUserDefaults.sharedInstance.screenName
        let timeOnScreen = calculateScreenTime()
        let params = [
            "screenName" : screenName!,
            "movedToScreen" : movedToScreen,
            "timeOnScreen in Seconds" : timeOnScreen
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Screen Time Event", valueToSum: Double(timeOnScreen), parameters: params)
    }
    
    func logApplicationScreenTimeOnArticlesDetailsEvent(articleName : String) {
        let screenName = FEUserDefaults.sharedInstance.screenName
        let timeOnScreen = calculateScreenTime()
        let params = [
            "screenName" : screenName!,
            "articleName" : articleName,
            "timeOnScreen in Seconds" : timeOnScreen
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Screen Time Event", valueToSum: Double(timeOnScreen), parameters: params)
    }
    
    func logLoginSuccesfulEvent(username : String, fullName : String, loginType : String) {
        let params = [
            "username" : username,
            "fullname" : fullName,
            "login_type" : loginType
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Login Succesful Event", parameters: params)
    }
    
    func logImageCaptureEvent(username : String, fullName : String, ImageCapture : String) {
        let params = [
            "username" : username,
            "fullname" : fullName,
            "ImageCapture" : ImageCapture
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Image Capture Event", parameters: params)
    }
    
    func logSignupSuccesfulEvent(username : String, fullName : String, signupType : String) {
        let params = [
            "username" : username,
            "fullname" : fullName,
            "signup_type" : signupType
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Signup Succesful Event", parameters: params)
    }
    
    func logWorkoutTimeAddedEvent(username : String, fullName : String, workoutTime : String, actionType : String) {
        let params = [
            "username" : username,
            "fullname" : fullName,
            "workoutTime" : workoutTime,
            "Action Type" : actionType //Add or Update
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Workout Time Event", parameters: params)
    }
    
    func logInAppPurchaseMonthlyPurchaseEvent(username : String, fullName : String, purchaseAmount : Double) {
        let params = [
            "username" : username,
            "fullname" : fullName,
            "purchaseType" : "Monthly",
            "purchaseAmount" : purchaseAmount
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Monthly Purchase Event", valueToSum: purchaseAmount, parameters: params)
    }
    
    func logInAppPurchaseAnnualPurchaseEvent(username : String, fullName : String, purchaseAmount : Double) {
        let params = [
            "username" : username,
            "fullname" : fullName,
            "purchaseType" : "Annual",
            "purchaseAmount" : purchaseAmount
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Annual Purchase Event", valueToSum: purchaseAmount, parameters: params)
    }

    func logLogoutEvent(username : String, fullName : String) {
        let params = [
            "username" : username,
            "fullname" : fullName
            ] as [String : Any]
        FBSDKAppEvents.logEvent("Logout Event", parameters: params)
    }
    
    private func calculateScreenTime() -> Int {
        let startTime = FEUserDefaults.sharedInstance.startTime
        let interval = startTime?.timeIntervalSinceNow
        let time = Int(round(interval!)) * -1
        return time
    }
}
