//
//  API+WorkOut.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 02/11/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import Parse
import UIKit
import SDWebImage
import RealmSwift

extension API {
    
    func workOutQuery () -> PFQuery<PFObject> {
        return PFQuery(className: KWorkOutTableName)
    }
    
    func createWorkOutOnServer(data workOutData : Dictionary<String, Any>) -> BFTask<AnyObject> {
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        let workout:  WorkOutPFModel  =  WorkOutPFModel()
        
        workout.workoutName =           workOutData[WORKOUT_NAME] as? String
        workout.workoutImageUrl =       workOutData[WORKOUT_IMAGE_URL] as? String
        workout.workoutDuration =       workOutData[WORKOUT_DURATION] as? NSNumber
        workout.workoutVideoUrls =      workOutData[WORKOUT_VIDEO_URLS] as? [String]
        workout.workoutVideoTitles =    workOutData[WORKOUT_VIDEO_TITLES] as? [String]
        workout.publishDate =           workOutData[PUBLISH_DATE] as? Date
        workout.isPublished =           false
        workout.isDeleted =             false
        workout.saveInBackground { (Success, error) in
            if (Success == true && error == nil ){
                taskSource.set(result: workout as AnyObject)
            }else {
                taskSource.set(error: error!)
            }
            
        }
        
        return taskSource.task
     }
    
     func uploadWorkOutOnSerer() -> BFTask<AnyObject>{
        
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        
         guard let video1 = Bundle.main.path(forResource: "1", ofType:"mov") else {
            debugPrint("video.m4v not found")
            return taskSource.task
        }
 
        guard let video2 = Bundle.main.path(forResource: "2", ofType:"mov") else {
            debugPrint("video.m4v not found")
            return taskSource.task
        }
         
        guard let video3 = Bundle.main.path(forResource: "3", ofType:"mov") else {
            debugPrint("video.m4v not found")
            return taskSource.task
        }
 
        /*
        guard let video4 = Bundle.main.path(forResource: "Video_3", ofType:"mov") else {
            debugPrint("video.m4v not found")
            return taskSource.task
        }
 */

        let videoData1 =  try?  Data(contentsOf:   URL(fileURLWithPath: video1))
        let videoData2 =  try?  Data(contentsOf:   URL(fileURLWithPath: video2))
        let videoData3 =  try?  Data(contentsOf:   URL(fileURLWithPath: video3))
//        let videoData4 =  try?  Data(contentsOf:   URL(fileURLWithPath: video4))
        
        let workOutImage = CloudinaryHelper.sharedInstance.uploadImageOnCloudinary(withImage: #imageLiteral(resourceName: "day2Workout"), andImageName: "workout_video/day2/workOutImage" )
        let videoTask1 = CloudinaryHelper.sharedInstance.uploadVideoeOnCloudinary(withVideo: videoData1!, andVideoName: "workout_video/day2/video_1")
        let videoTask2 = CloudinaryHelper.sharedInstance.uploadVideoeOnCloudinary(withVideo: videoData2!, andVideoName: "workout_video/day2/video_2")
        let videoTask3 = CloudinaryHelper.sharedInstance.uploadVideoeOnCloudinary(withVideo: videoData3!, andVideoName: "workout_video/day2/video_3")
   //     let videoTask4 = CloudinaryHelper.sharedInstance.uploadVideoeOnCloudinary(withVideo: videoData4!, andVideoName: "workout_video/day2/video_4")

       
        BFTask<AnyObject>(forCompletionOfAllTasksWithResults : [workOutImage,videoTask1,videoTask2,videoTask3]).continueWith (block: {  (t) -> Any? in
            if((t.error) != nil) {
                print("Error is \(t.error?.localizedDescription)")
                 taskSource.set(error: t.error!)
            }
            else {
                var urls = t.result as! Array<String>
                print("Uploading 1 \(urls)")

                let workData : NSMutableDictionary = NSMutableDictionary.init()
                workData[WORKOUT_NAME] =  "Day2"
                workData[WORKOUT_IMAGE_URL] = urls[0]
                workData[WORKOUT_DURATION] = 0 // Set Duration of Workout
                workData[WORKOUT_VIDEO_TITLES] = ["title1", "title2","title3"]
                urls =  Array(urls.dropFirst(1))
                workData[WORKOUT_VIDEO_URLS] =     urls
                print("Uploading \(urls)")
                workData[PUBLISH_DATE] = Date()
                self.createWorkOutOnServer(data: workData as! Dictionary<String, Any>)
                
                taskSource.set(result: t.result as? String as AnyObject)
                
            }
            return t
        })
        return taskSource.task
    }
    
    func getTodayWorkOutFromParse() -> BFTask<AnyObject> {
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        let query = workOutQuery()
        query.whereKey(WORKOUT_NAME, equalTo: "Day\(LSUserDefaults.sharedInstance.workOutDay!)")
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                taskSource.set(error: error!)
                print("Today Workout Error is : \(error!)")
            } else if objects != nil {
                if let todayWorkOut = objects?.first as? WorkOutPFModel {
                    WorkOutDefaults.sharedInstance.workOutObjectId = todayWorkOut.objectId
                    WorkOutDefaults.sharedInstance.workoutName = todayWorkOut.workoutName!
                    WorkOutDefaults.sharedInstance.workoutImageUrl = todayWorkOut.workoutImageUrl!
                    WorkOutDefaults.sharedInstance.workoutVideoUrls = todayWorkOut.workoutVideoUrls!
                    WorkOutDefaults.sharedInstance.workoutVideoTitles = todayWorkOut.workoutVideoTitles
                    WorkOutDefaults.sharedInstance.workoutDuration = todayWorkOut.workoutDuration
                    print("Today Workout is : \(todayWorkOut)")
                } else {
                    print("The Object is nil")
                }
                taskSource.set(result: objects?.first)
            } else {
                print("Today Workout Not Fatch")
            }
        }
        return taskSource.task
    }
    
    func setTodayWorkOutFromLocal() -> BFTask<AnyObject> {
        let taskSource : BFTaskCompletionSource<AnyObject> =  BFTaskCompletionSource.init()
        let title = "Day1"
        guard let video1 = Bundle.main.path(forResource: "1", ofType: "mov", inDirectory: title) else {
            debugPrint("video.m4v not found")
            return taskSource.task
        }
        
        guard let video2 = Bundle.main.path(forResource: "2", ofType: "mov", inDirectory: title) else {
            debugPrint("video.m4v not found")
            return taskSource.task
        }
        
        guard let video3 = Bundle.main.path(forResource: "3", ofType: "mov", inDirectory: title) else {
            debugPrint("video.m4v not found")
            return taskSource.task
        }
        WorkOutDefaults.sharedInstance.workoutName = title
        WorkOutDefaults.sharedInstance.workoutImageUrl = title
        WorkOutDefaults.sharedInstance.workoutDuration = 300
        WorkOutDefaults.sharedInstance.workoutVideoTitles = ["thrusters single", "thrusters double", "rowing"]
        WorkOutDefaults.sharedInstance.workoutVideoUrls = [video1,video2, video3]
        taskSource.set(result: true as AnyObject)
        return taskSource.task
    }
    
    func saveAllWorkoutsInRealm() -> BFTask<AnyObject> {
        saveFirstWorkoutInRealm()
        return API.sharedInstance.getAllWorkoutsFromParse().continueWith {[weak self] (task) -> Any? in
            if task.error != nil {
                return task
            } else {
                print("Total Workouts are \(task.result?.count ?? 0)")
                for workout in task.result as! [WorkOutPFModel] {
                    self?.saveWorkoutInRealm(withWorkout: workout)
                    if (task.result?.lastObject as! WorkOutPFModel) == workout {
                        WorkOutDefaults.sharedInstance.lastWorkoutPublishedOn = workout.publishDate!
                        print("last workout \(workout)")
                    }
                }
            }
            return nil
        }
    }
    
    func saveNewWorkoutsInRealm() -> BFTask<AnyObject> {
        return API.sharedInstance.getNewWorkoutsFromParse().continueWith {[weak self] (task) -> Any? in
            if task.error != nil {
                return task
            } else {
                print("Total Workouts are \(task.result?.count ?? 0)")
                for workout in task.result as! [WorkOutPFModel] {
                    self?.saveWorkoutInRealm(withWorkout: workout)
                    if (task.result?.lastObject as! WorkOutPFModel) == workout {
                        WorkOutDefaults.sharedInstance.lastWorkoutPublishedOn = workout.publishDate!
                        print("last workout \(workout)")
                    }
                }
            }
            return nil
        }
    }
    
    func saveWorkoutInRealm(withWorkout workout: WorkOutPFModel) {
        let realm = try! Realm()
        let workoutRealm = WorkoutRealmModal()
        workoutRealm.objectId = workout.objectId
        workoutRealm.workoutName = workout.workoutName
        workoutRealm.workoutDuration = workout.workoutDuration
        workoutRealm.workoutImageUrl = workout.workoutImageUrl
        for i in 0..<(workout.workoutVideoUrls?.count)! {
            workoutRealm.workoutVideoUrls.append(workout.workoutVideoUrls![i])
            workoutRealm.workoutVideoTitles.append(workout.workoutVideoTitles![i])
            workoutRealm.isWorkoutVideosSynced.append(false)
        }
        workoutRealm.publishDate = workout.publishDate
        workoutRealm.isPublished = workout.isPublished
        workoutRealm.isDeleted = workout.isDeleted
        workoutRealm.workoutNumericalName = workout.workoutNumericalName
        workoutRealm.isWorkoutImageSynced = false
        workoutRealm.isWorkoutSynced = false
        
        try! realm.write {
            realm.add(workoutRealm, update: true)
        }
    }
    
    func saveFirstWorkoutInRealm() {
        
        let title = "Day1"
        guard let video1 = Bundle.main.path(forResource: "1", ofType: "mov", inDirectory: title) else {
            debugPrint("video.m4v not found")
            return
        }
        
        guard let video2 = Bundle.main.path(forResource: "2", ofType: "mov", inDirectory: title) else {
            debugPrint("video.m4v not found")
            return
        }
        
        guard let video3 = Bundle.main.path(forResource: "3", ofType: "mov", inDirectory: title) else {
            debugPrint("video.m4v not found")
            return
        }
        let videos = [video1,video2,video3]
        let titles = ["thrusters single", "thrusters double", "rowing"]
        
        let realm = try! Realm()
        let workoutRealm = WorkoutRealmModal()
        workoutRealm.objectId = title
        workoutRealm.workoutName = title
        workoutRealm.workoutImageUrl = title
        workoutRealm.workoutDuration = NSNumber(value: 300)
        for i in 0..<(videos.count) {
            workoutRealm.workoutVideoUrls.append(videos[i])
            workoutRealm.workoutVideoTitles.append(titles[i])
            workoutRealm.isWorkoutVideosSynced.append(true)
        }
        workoutRealm.publishDate = Date()
        workoutRealm.isPublished = true
        workoutRealm.isDeleted = false
        workoutRealm.workoutNumericalName = NSNumber(value: 1)
        workoutRealm.isWorkoutImageSynced = true
        workoutRealm.isWorkoutSynced = true
        try! realm.write {
            realm.add(workoutRealm, update: true)
        }
    }
    
    func getCurrentWorkout(isSynced: Bool) ->  WorkoutRealmModal? {
        if isSynced == true {
            return getCurrentSyncedWorkout()
        }
        else {
            return getCurrentUnSyncedWorkout()
        }
    }
    
    func getCurrentUnSyncedWorkout() -> WorkoutRealmModal? {
        let realm = try! Realm()
        let workoutday = NSNumber(value: LSUserDefaults.sharedInstance.workOutDay!)
        let idPredicate = NSPredicate(format:"workoutNumericalName = %@ AND isWorkoutSynced = false", workoutday)
        return { realm.objects(WorkoutRealmModal.self).filter(idPredicate) }().first
    }
    
    func getCurrentSyncedWorkout() -> WorkoutRealmModal? {
        let realm = try! Realm()
        let workoutday = NSNumber(value: LSUserDefaults.sharedInstance.workOutDay!)
        let idPredicate = NSPredicate(format:"workoutNumericalName = %@ AND isWorkoutSynced = true", workoutday)
        return { realm.objects(WorkoutRealmModal.self).filter(idPredicate) }().first
    }
    
    func updateRealmObject(withObject object : WorkoutRealmModal) {
        let realm = try! Realm()
        try! realm.write {
            object.isWorkoutImageSynced = true
            realm.add(object, update: true)
        }
        print("Updated Object \(object)")
    }
    
    func getUpcomingUnSyncedWorkouts() -> Results<WorkoutRealmModal> {
        let realm = try! Realm()
        let workoutday = NSNumber(value: LSUserDefaults.sharedInstance.workOutDay!)
        let idPredicate = NSPredicate(format:"workoutNumericalName > %@ AND isWorkoutSynced = false", workoutday)
        return { realm.objects(WorkoutRealmModal.self).filter(idPredicate) }()
    }
    
    func getPreviousUnSyncedWorkouts() -> Results<WorkoutRealmModal> {
        let realm = try! Realm()
        let workoutday = NSNumber(value: LSUserDefaults.sharedInstance.workOutDay!)
        let idPredicate = NSPredicate(format:"workoutNumericalName < %@ AND isWorkoutSynced = false", workoutday)
        return { realm.objects(WorkoutRealmModal.self).filter(idPredicate) }()
    }
    
    func syncDataInDisk() -> BFTask<AnyObject> {
        
        var workouts = getUpcomingUnSyncedWorkouts()
        if workouts.count > 0 {
            print("Upcoming Workouts Are \(workouts) \n Number of Workouts are \(workouts.count)")
        }
        else {
            workouts = getPreviousUnSyncedWorkouts()
            print("Previous Workouts Are \(workouts) \n Number of Workouts are \(workouts.count)")
        }
        
        var serialTasks = BFTask<AnyObject>(result: nil)
        for workout : WorkoutRealmModal in workouts {
            serialTasks = serialTasks.continueWith(block: {[weak self] (task) -> Any? in
                return self?.saveDataInLocal(withWorkout: workout)
            })
        }
        return serialTasks.continueWith(block: { (resultantTask) -> Any? in
            return nil
        })
        
    }
    
    private func saveDataInLocal(withWorkout workout: WorkoutRealmModal) -> BFTask<AnyObject>{
        var tasks:[BFTask<AnyObject>] = []
        var counter = 0
        print("In Workout serial task for saving videos and images \(workout)")
        let imageDownloadTask = self.saveDataInDisk(withName: workout.objectId!, withURL: workout.workoutImageUrl!, atIndex: nil, withWorkout: workout)
        tasks.append(imageDownloadTask)
        for workoutVideo in (workout.workoutVideoUrls) {
            let videoDownloadTask = self.saveDataInDisk(withName: workout.objectId!, withURL: workoutVideo, atIndex: counter, withWorkout: workout)
            counter = counter + 1
            tasks.append(videoDownloadTask)
        }
        return BFTask(forCompletionOfAllTasksWithResults:tasks)
    }
    
    func saveDataInDisk(withName name: String,withURL url: String, atIndex index : Int?, withWorkout workout : WorkoutRealmModal) -> BFTask<AnyObject> {
        let taskSource = BFTaskCompletionSource<AnyObject>()
        let completeName = (index == nil) ? "\(name).png"  : "\(name)\(index!).mov"
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        print("In Workout saving task for saving videos and images \(completeName)")
        let fetchProfileImage = session.dataTask(with: request as URLRequest) {  (data, response, error) -> Void in
            if (response as? HTTPURLResponse) != nil {
                ImageHelper.sharedInstance.saveInDocuments(data: data!, name: completeName)
                DispatchQueue.main.async {
                    let realm = try! Realm()
                    try! realm.write {
                        if index == nil {
                            print("In Workout Image Saved")
                            workout.isWorkoutImageSynced = true
                            print("In Workout Saving Task After Image Save\(workout)")
                        } else {
                            workout.isWorkoutVideosSynced[index!] = true
                            print("In Workout Saving Task After Video Save\(workout)")
                            if index == ((workout.isWorkoutVideosSynced.count) - 1) {
                                workout.isWorkoutSynced = true
                                print("In Workout Saving Task After Workout Saved Complete \(workout)")
                            }
                        }
                        realm.add((workout), update: true)
                        taskSource.set(result: true as AnyObject)
                    }
                }
                
            }
        }
        fetchProfileImage.resume()
        return taskSource.task
    }
}
