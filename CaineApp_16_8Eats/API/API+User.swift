//
//  API+User.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 09/11/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import OneSignal


extension API {

///////--------------------------  One Signal --------------------------------
//MARK:One Signal
func updateOneSignal(id oneSignalId : String ){
    
    guard let knUser =  ParseUser().currentUser else { return }
    knUser.oneSignalId = oneSignalId
    knUser.saveInBackground()
     CloudCode.sharedInstance.callOneSignalCloudFunction(onSignalID: oneSignalId)
    
    
}

/// If one signal id is change then user is login from other device
@objc  func checkIfUserIsAlreadyLoginPrevious(){
    
    guard let knUser =  ParseUser().currentUser else { return }
    print("RL User \(knUser.oneSignalId) and login  \(LSUserDefaults.sharedInstance.oneSignalId)")
    if knUser.oneSignalId != nil && knUser.oneSignalId!.isStringEmpty == false &&
        LSUserDefaults.sharedInstance.oneSignalId != knUser.oneSignalId {
        CloudCode.sharedInstance.VerifyMultipleLoginForUser()
    }
    
}

    
    //MARK: One Signal
    
    func  sendTemporaryMessage(toUser user : String){
        
        let notificationString = "Testing notification"
        let data : NSMutableDictionary  =   NSMutableDictionary.init()
        data["NotificationText"] = notificationString
        data[FNOTIFICATION_TYPE] =              FNOTIFICATION_WORK_OUT
        
        let message : NSMutableDictionary  =   NSMutableDictionary.init()
        message["notificationMessage"] =        notificationString
        message["notificationSender"] =         ParseUser().currentUser!.value(forKey: "fullName")
        
        
        let notificationContent = [
            "include_player_ids": ["26a7cd39-5833-427c-a616-e9c3bf16da53"]/*[user.oneSignalId]*/,
            "headings": ["en": "Notification Title"],
            "subtitle": ["en": "An English Subtitle"],
            "ios_badgeType": "Increase",
            "url": "https://imgur.com",
            "ios_attachments": ["image" : "https://s3-us-west-2.amazonaws.com/isystematic/caine4.gif"],
            "content_available": 1,
            "isIos": true,
            "ios_badgeCount": 1
            
            ] as [String : Any]
        
        OneSignal.postNotification(notificationContent)
        
    }
    

}
