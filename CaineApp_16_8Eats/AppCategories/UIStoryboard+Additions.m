//
//  UIStoryboard+Additions.m
//  Hush
//
//  Created by Salman Khan on 03/05/17.
//  Copyright © 2016 iSystematic. All rights reserved.
//

#import "UIStoryboard+Additions.h"

@implementation UIStoryboard (Additions)

+ (UIStoryboard *) mainStoryboard {
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

+ (UIStoryboard *) heroApproachStoryboard {
    return [UIStoryboard storyboardWithName:@"HeroApproach" bundle:nil];
}

@end
