//
//  UIColor+CustomColors.h
//  Popping
//
//  Created by André Schneider on 25.05.14.
//  Copyright (c) 2014 André Schneider. All rights reserved.
//

#import <UIKit/UIKit.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorFromRGBA(rgbValue, alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]

@interface UIColor (CustomColors)

+ (UIColor *)color_TabBarBG;

+ (UIColor *)color_NavigationBarBG;
+ (UIColor *)color_NavigationBarTitle;
+ (UIColor *)color_NavigationBarButtons;

+ (UIColor *)color_MessagesList_Name;
+ (UIColor *)color_MessagesList_Text;
+ (UIColor *)color_MessagesList_Date;

+ (UIColor *)color_SignUp_Button;

@end


//Edit This
