//
//  UIViewController+Additions.m
//  Hush
//
//  Created by Macbook Pro on 9/20/16.
//  Copyright © 2017 Salman Khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Additions)

+ (UIViewController *)homeController;
+ (UIViewController *)articlesListNavController;

@end
