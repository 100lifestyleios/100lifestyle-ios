//
//  UIFont+FontUtils.h
//  9Dollor99
//
//  Created by Afnan on 2/4/17.
//  Copyright © 2017 isystematic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (FontUtils)

#pragma mark - General
//+ (UIFont* )font_Regular:(CGFloat)size;
//+ (UIFont* )font_Normal:(CGFloat)size;
//+ (UIFont* )font_Light:(CGFloat)size;
//+ (UIFont* )font_Bold:(CGFloat)size;

#pragma mark - Used In App
+ (UIFont *)font_NavigationBarTitle;
+ (UIFont *)font_NavigationBarButtonTitle;
+ (UIFont *)font_MessagesList_Name;
+ (UIFont *)font_MessagesList_Text;
+ (UIFont *)font_MessagesList_Date;

+ (UIFont *)font_SignUp_Button;
@end


//Edit This
