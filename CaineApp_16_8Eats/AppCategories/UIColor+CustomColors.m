//
//  UIColor+CustomColors.m
//  Popping
//
//  Created by André Schneider on 25.05.14.
//  Copyright (c) 2014 André Schneider. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+ (UIColor *)color_TabBarBG                 { return UIColorFromRGB(0xF6F6F6); }

+ (UIColor *)color_NavigationBarBG          { return UIColorFromRGB(0xFFFFFF); }
+ (UIColor *)color_NavigationBarTitle       { return UIColorFromRGB(0x4A4A4A); }
+ (UIColor *)color_NavigationBarButtons     { return UIColorFromRGB(0xD83D79); }

+ (UIColor *)color_MessagesList_Name        { return UIColorFromRGB(0xA2A2A2); }
+ (UIColor *)color_MessagesList_Text        { return UIColorFromRGB(0xA2A2A2); }
+ (UIColor *)color_MessagesList_Date        { return UIColorFromRGB(0xA2A2A2); }

+ (UIColor *)color_SignUp_Button            { return UIColorFromRGB(0xDE295C);}

@end
