//
//  UIStoryboard+Additions.h
//  TowTruck
//
//  Created by clines192 on 9/19/16.
//  Copyright © 2016 Ahsan Misbah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (Additions)

+ (UIStoryboard *) mainStoryboard;
+ (UIStoryboard *) heroApproachStoryboard;

@end
