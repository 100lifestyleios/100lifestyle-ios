//
//  UIFont+FontUtils.m
//  9Dollor99
//
//  Created by Afnan on 2/4/17.
//  Copyright © 2017 isystematic. All rights reserved.
//

#import "UIFont+FontUtils.h"

@implementation UIFont (FontUtils)

+ (UIFont* )font_Regular:(CGFloat)size
{
    return [UIFont fontWithName:@"Montserrat-Regular" size:size];
}
+ (UIFont* )font_Normal:(CGFloat)size
{
    return [UIFont fontWithName:@"Montserrat" size:size];
}
+ (UIFont* )font_Light:(CGFloat)size
{
    return [UIFont fontWithName:@"Montserrat-Light" size:size];
}
+ (UIFont* )font_Bold:(CGFloat)size
{
    return [UIFont fontWithName:@"Montserrat-Bold" size:size];
}

+ (UIFont *)font_NavigationBarTitle         { return [self font_Bold:18.0]; }
+ (UIFont *)font_NavigationBarButtonTitle   { return [self font_Bold:14.0]; }
+ (UIFont *)font_MessagesList_Name          { return [self font_Bold:18.0]; }
+ (UIFont *)font_MessagesList_Text          { return [self font_Light:14.0]; }
+ (UIFont *)font_MessagesList_Date          { return [self font_Bold:12.0]; }


+ (UIFont *)font_SignUp_Button              { return [self font_Bold:14.0]; }
@end
