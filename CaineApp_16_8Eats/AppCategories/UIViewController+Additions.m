//
//  UIViewController+Additions.m
//  Hush
//
//  Created by Salman Khan on 04/27/17.
//  Copyright © 2017 iSystematic. All rights reserved.
//

#import "UIViewController+Additions.h"
#import "UIStoryboard+Additions.h"

@implementation UIViewController (Additions)

+ (UIViewController *)homeController {
    return [[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:@"HomeController"];
}

+ (UIViewController *) articlesListNavController {
    return [[UIStoryboard heroApproachStoryboard] instantiateViewControllerWithIdentifier:@"ArticlesListNavViewController"];
}

@end
