//
//  ParseUser.swift
//  Quixly
//
//  Created by clines192 on 04/08/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Parse

//enum Gender
//{
//    case male, female, not_specified
//    func stringValue() -> String {
//        switch self {
//        case .male:
//            return "male"
//        case .female:
//            return "female"
//        default:
//            return ""
//        }
//    }
//}

@objc class ParseUser: PFUser {
    @objc @NSManaged var fullName:String?
    @objc @NSManaged var geoLocation:PFGeoPoint?
    @objc @NSManaged var profilePhoto:PFFile?
    @objc @NSManaged var workoutTime:Date?
    @objc @NSManaged var isFacebookLogin:Bool
    @objc @NSManaged var lastWorkOutDate : Date?
    @objc @NSManaged var workOutDay : NSNumber?
    @objc @NSManaged var purchaseDate : Date?
    @objc @NSManaged var purchaseStatus : NSNumber?
    @objc @NSManaged var purchaseSuccessful:    Bool
    @objc @NSManaged var oneSignalId:    String? // like 3242-23-

    
    lazy var currentUser  = PFUser.current() as? ParseUser
}
