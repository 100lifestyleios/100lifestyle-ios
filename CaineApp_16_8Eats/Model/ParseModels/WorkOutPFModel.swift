//
//  WorkOutPFModel.swift
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 02/11/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import Foundation
import Parse


class WorkOutPFModel: PFObject , PFSubclassing {
    static func parseClassName() -> String {
        return KWorkOutTableName
    }
    
    
    @objc @NSManaged var workoutName:   String?
    @objc @NSManaged var workoutImageUrl:   String?
    @objc @NSManaged var workoutDuration:   NSNumber?
    @objc @NSManaged var workoutVideoUrls:   [String]?
    @objc @NSManaged var workoutVideoTitles:   [String]?
    @objc @NSManaged var workoutNumericalName:   NSNumber?
    @objc @NSManaged var publishDate:   Date?// Parent Challenge ID if itself is Parent then it ll nil
    @NSManaged var isPublished:          Bool // if private then check published
    @NSManaged var isDeleted:          Bool // if private then check published
}
