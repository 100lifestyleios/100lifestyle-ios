//
//  WorkoutRealmModal.swift
//  CaineApp_16_8Eats
//
//  Created by isystematic on 12/27/18.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

import UIKit
import RealmSwift

class WorkoutRealmModal :  Object {
    
    dynamic var objectId:                   String?
    dynamic var workoutName:                String?
    dynamic var workoutImageUrl:            String?
    dynamic var publishDate:                Date?
    dynamic var workoutDuration:            NSNumber?  = 0          //If Videos are synced from server set this to true.
    dynamic var workoutNumericalName:       NSNumber?  = 1          //Its workout days numerical part to make queries easy based on numerical value
    dynamic var isPublished:                Bool = true
    dynamic var isDeleted:                  Bool = false
    dynamic var isWorkoutImageSynced:       Bool = false         //If Image is synced from server set this to true.
    dynamic var isWorkoutSynced:            Bool = false           //If Image and Videos are synced from server set this to true.
    var isWorkoutVideosSynced =     List<Bool>()
    var workoutVideoUrls =          List<String>()
    var workoutVideoTitles =        List<String>()

    override class func primaryKey() -> String? {
        return "objectId"    }
}
