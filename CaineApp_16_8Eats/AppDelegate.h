//
//  AppDelegate.h
//  CaineApp_16_8Eats
//
//  Created by clines192 on 16/06/2017.
//  Copyright © 2017 iSystematic LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

