//
//  NSDateFormatter+DateFormatterUtils.h
//  TheMemoryApp
//
//  Created by Apple on 7/30/16.
//  Copyright © 2016 isystematic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (DateFormatterUtils)

-(NSDateFormatter *)getCurretTimeDate;
 
@end
