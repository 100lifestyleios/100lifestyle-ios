//
//  NSString+StringUtils.m
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 30/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import "NSString+StringUtils.h"

@implementation NSString (StringUtils)

- (bool )isStringEmpty
{
    if(self == nil ||  [self isEqual:[NSNull null] ] ) return YES;
    
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if([trimmedString length] == 0)
        return YES;
    return false;
}


-(NSString *) StringArrayToCommaSeparatedString:(NSMutableArray *)array
{
    NSString * tempArray = @"";
    if(array ==nil || [array count]==0) return tempArray;
    for(NSString * pathName in array)
    {
        tempArray  =[NSString stringWithFormat:@"%@%@,",tempArray,pathName];
    }
    //
    tempArray = [tempArray substringToIndex:tempArray.length-1];
    
    return tempArray;
    
}


-(bool)caseIncentiveComparison:(NSString *)otherString
{
    return ( [otherString caseInsensitiveCompare:self] == NSOrderedSame ) ;
}

@end
