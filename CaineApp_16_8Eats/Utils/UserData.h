//
//  UserData.h
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 30/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject

+(void)setAccountType:(int)accountType;
+(int)getAccountType;

@end
