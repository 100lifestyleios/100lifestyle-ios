//
//  UserData.m
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 30/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import "UserData.h"

@implementation UserData



+(void)setAccountType:(int)accountType
{
    [[NSUserDefaults standardUserDefaults ] setInteger:accountType forKey:@"user_account_type"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(int)getAccountType
{
    
    if ([[NSUserDefaults standardUserDefaults ] integerForKey:@"user_account_type"] ==  nil) return NO_ACCOUNT;
    return ((int) [[NSUserDefaults standardUserDefaults ] integerForKey:@"user_account_type"]);
}


@end
