//
//  Constant.h
//  CaineApp_16_8Eats
//
//  Created by Sheeraz Ahmed Memon on 29/10/2018.
//  Copyright © 2018 iSystematic LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

#define kInAppPurchaseYearPackage @"com.isystematic.CaineApp__6_8Eats.latest.content"
#define kInAppPurchaseMonthlyPackage @"com.isystematic.CaineApp__6_8Eats.Monthly_Subscription"

#define kInAppPurchaseYearPackageReduced @"com.isystematic.CaineApp__6_8Eats.latestreduced.content"
#define kInAppPurchaseMonthlyPackageReduced @"com.isystematic.CaineApp__6_8Eats.Monthly_Subscription_reduced"

#define kInAppPurchaseAutoRenewableSharedSecret @"f0e3f0eb2aef4142abdd2c796fe7ad4e"

#define IN_APP_IS_PRODUCTION  false


// Account type
#define NO_ACCOUNT 1 // No purchases yet
#define YEAR_ACCOUNT 2 // User has purchased
#define MONTH_ACCOUNT 3 // User has purchased


@end
