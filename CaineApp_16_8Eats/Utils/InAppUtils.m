//
//  InAppUtils.m
//  TheMemoryApp
//
//  Created by Afnan on 12/4/16.
//  Copyright © 2016 isystematic. All rights reserved.
//

#import "InAppUtils.h"

#import "CaineApp_16_8Eats-Swift.h"


static InAppUtils *sharedInstance;


@implementation InAppUtils



+ (instancetype)sharedInstance {
    static dispatch_once_t DDASLLoggerOnceToken;
    dispatch_once(&DDASLLoggerOnceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

-(int)verifyProductIdentify:(NSString *)productIdentify
{
    if(productIdentify==nil)     return NO_ACCOUNT;
    
    if([productIdentify caseIncentiveComparison:kInAppPurchaseYearPackage])
        return YEAR_ACCOUNT;
    
    if([productIdentify caseIncentiveComparison:kInAppPurchaseYearPackageReduced])
        return YEAR_ACCOUNT;
    
    if([productIdentify caseIncentiveComparison:kInAppPurchaseMonthlyPackage])
        return MONTH_ACCOUNT;
    
    if([productIdentify caseIncentiveComparison:kInAppPurchaseMonthlyPackageReduced])
        return MONTH_ACCOUNT;
    
    return NO_ACCOUNT;
    
}

//AfnanWork: set account type and other work here
-(void)setAccountType:(int)accountType {
    
    [UserData setAccountType:accountType];
    ParseUser *user = [ParseUser currentUser];
    if (user  != nil && user.purchaseSuccessful) {
        user.purchaseStatus = [NSNumber numberWithInt:accountType];
        [user saveInBackground];
    }
    
}
-(void)setAccountExpire {
    [UserData setAccountType:NO_ACCOUNT];
}


#pragma mark - Date work

+(NSString *)getCurrentDateTime {
      return [self getFormatDateInString:[NSDate date]];
}

+(NSString *)getFormatDateInString:(NSDate *)date {
    
    NSDateFormatter *formatter;
    NSString        *dateString;
    formatter = [[NSDateFormatter alloc] init];
    formatter = [formatter getCurretTimeDate];
    dateString = [formatter stringFromDate:date];
    return dateString;
    
}

+(NSDate *)getDateFromString:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    return date;;
    
}

+(NSString *)getStringFromDate:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    //Optionally for time zone conversions
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    NSString *stringFromDate = [dateFormat stringFromDate:date];
    return stringFromDate;
    
}


+(int) getapplicationRunningtime
{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    int previousNumber = (int)[userDefault integerForKey:@"APP_RUNNING_TIME"];
    if(!previousNumber)
    {
        previousNumber =0;
    }
    [userDefault setInteger:(previousNumber+1) forKey:@"APP_RUNNING_TIME"];
    return previousNumber;
}



@end
