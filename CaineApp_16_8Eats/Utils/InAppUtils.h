//
//  InAppUtils.h
//  TheMemoryApp
//
//  Created by Afnan on 12/4/16.
//  Copyright © 2016 isystematic. All rights reserved.
//

#import <Foundation/Foundation.h>
// This class is responsible to handle Common IN App function

@interface InAppUtils : NSObject


+ (instancetype)sharedInstance;

/**
 Save in user defauly
 then on call for firebase  with account type
 **/
-(int)verifyProductIdentify:(NSString *)productIdentify;
-(void)setAccountType:(int)accountType;
// Payment has been expired
-(void)setAccountExpire ;



+(NSString *)getCurrentDateTime ;
+(NSString *)getFormatDateInString:(NSDate *)date ;

+(NSDate *)getDateFromString:(NSString *)dateStr;
+(NSString *)getStringFromDate:(NSDate *)date ;

+(int) getapplicationRunningtime;


@end
